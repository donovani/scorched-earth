# Scorched Earth 2000 Restoration

## About This Repo

### Overview

This is a port of an old Java game called Scorched Earth 2000, which was a recreation of an older game called Scorched Earth.
Unfortunately, the game was built as a Java applet, and stopped working once browsers began deprecating the technology.
It is based on the source code for Scorched Earth 2000, which was released open-source under the GPL 3.0.

The source code of Scorched Earth 2000 is from another era of Java development.
It used CVS for version control, an old Java version, no specified build directory, and a nonstandard project structure.
The original repo is in read-only mode, and there is only the most recent commit available for download.
Some parts of it don't work. Some parts of it rely on there being an applet with embedded HTML.

The goal of this port is to modernize the codebase, simplify building, & have them running as standalone apps.

### Structure

The Gradle project is composed of 2 source sets - one for the game client, and one for the server.
```
build/
| ScorchAppletClient.jar
| ScorchDesktopClient.jar
| ScorchServer.jar
src/
| main/
| | scorch/
| | resources/
| | | html/
| | | sound/
| test/
| | scorch/
.editorconfig
build.gradle
```
Both `scorchclient` and `scorchserver` are source sets of the greater Gradle project.
The Gradle project also contains tasks to build each source set into an executable JAR.

### Requirements

This project requires a working Java installation of version 8 or higher,
as well as a JAVA_HOME environment variable defined on your system.

- Java SDK 8 or higher
- Gradle runtime, or one provided by your IDE.

## How To Build

Open the project as a Gradle project using the `build.gradle` file - that's it.

### Building the Server

Building the server application JAR is very straightforward. Simply run:
```sh
gradle scorchServerJar
```
The JAR will be generated within `build/ScorchServer.jar`.

### Building the Client

This repo features 2 ways of building the application - as an applet or a desktop application.

The applet version is generally not supported, but was what the original was based on, so can be helpful for development.

#### Building the applet client

Building the applet JAR is a simple Gradle task:
```sh
gradle scorchAppletClientJar
```
The JAR will be generated within `build/ScorchAppletClient.jar`

It's then on you to embed it into the product and provide the params.
An example of this can be found in the HTML template.

#### Building the desktop client

Building the client application JAR is a straightforward
```sh
gradle scorchDesktopClientJar
```
The JAR will be generated within `build/ScorchDesktopClient.jar`.

It should already be executable, but if you want you can wrap it in a native executable using something like Launch4J.

## How to Test

### JUnit
TBA - Testing is not currently implemented


## Attribution

In addition to this repo's contributors, it's important to note the authorship of the original projects.
This restoration stands on the shoulders of giants.

### Scorched Earth
The original DOS game Scorched Earth was created by one person:

- **Creator** - Wendell Hicken

### Scorched Earth 2000

The original Scorched Earth 2000 source code that this repo was created by the KAOS Software team:

Scorched Earth 2000 Source Code v1.060 - March 23, 2001

- **Project Lead** - Hei C. Ng (xixi)
- **Development Lead** - Alexander Rasin
- **Client Programming** - Mikhail Kruk
- **Weapons Programming** - Nathan Roslavker
- **Physics** - Ramya Ramesh
- **Documentation** - Kapil Mehra
- **Quality Assurance** - John Langton

## License

This code is a derivative of Scorched Earth 2000.
It was forked from the very final release:version 1.060 - released on March 23, 2001.

The original source was released open-source under the GPL 2.0.
As such, this repo comes with the same license.
