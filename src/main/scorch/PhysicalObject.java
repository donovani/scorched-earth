package scorch;

/**
 * A base class for any animated object that purports to have real world physical properties.
 * An instance of Physics class, passed as a parameter, is used to calculate the trajectory.
 * @author Nathan Roslavker
 */
abstract public class PhysicalObject extends Audible {
	protected int x, y, weight;
	protected Bitmap bitmap;
	protected Physics physics;

	public PhysicalObject(Bitmap bitmap, Physics physics) {
		this.bitmap = bitmap;
		this.physics = physics;

		x = y = weight = 0;

		if (physics != null)
			setPosition(physics.getStartX(), physics.getStartY());
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setPhysics(Physics physics) {
		this.physics = physics;
	}

	// These two methods might want to be synchronized, but there's a catch:
	// When mouse is moved, coordinates of players are used, & it may interfere
	// with animations synchronized on player.
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
