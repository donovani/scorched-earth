package scorch;

/**
 * Any class that can be used as scorch animation must implement this interface
 */
public interface FrameShow {
	/**
	 * Returns false after the last frame of animation has been drawn.
	 * Lets the animation container know when to stop calling this method.
	 * @param update specifies whether newPixels should be called.
 	 */
	boolean drawNextFrame(boolean update);
}

