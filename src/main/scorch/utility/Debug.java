package scorch.utility;

import scorch.Network;
import scorch.Protocol;

/**
 * Handles debug printouts, stack dumps, plus memory & time measurements
 * @author Mikhail Kruk
 * @author Alexander Rasin
 */
public class Debug {
	private static String log = "";

	public static final int debugLevel = 0;
	public static boolean desyncTest = false;
	public static boolean dev = false;

	private static long time;
	private static Console console;

	private static int frameCount;

	public static void startTimer() {
		time = System.currentTimeMillis();
	}

	public static void stopTimer(String message) {
		println("timer (" + message + "): " +
			(System.currentTimeMillis() - time) + " ms");
	}

	public static void printMemoryInfo() {
		println(getMemoryInfo());
	}

	public static String getMemoryInfo() {
		long mem = Runtime.getRuntime().totalMemory() -
			Runtime.getRuntime().freeMemory();
		return mem / 1000 + "Kb";
	}

	public static void println(String message) {
		println(message, 0);
	}

	public static void println(String message, int level) {
		if (debugLevel > level)
			System.out.println(message);
	}

	public static void printStack() {
		printStack(Thread.currentThread());
	}

	public static void printStack(Thread thread) {
		Thread.dumpStack();
	}

	public static void printThreads() {
		Thread[] threads = new Thread[Thread.activeCount()];
		Thread.enumerate(threads);
		for (int i = 0; i < threads.length; i++) {
			System.out.println("/t/t" + threads[i]);
			printStack(threads[i]);
		}
	}

	public static void pause(int ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	// expects the timer to be already started.  takes 3sec samples
	public static void calculateFpsRate() {
		long diff = System.currentTimeMillis() - time;
		if (diff > 3000) {
			System.out.println("FPS = " + (frameCount / 3) + ", approx. " +
				(3000 / frameCount) + "ms/frame " +
				" (" + frameCount + " frames in 3 sec)");
			frameCount = 0;
			time = System.currentTimeMillis();
		}
		frameCount++;
	}

	public static void initConsole(Network network) {
		if (console != null) console.dispose();
		console = new Console(network);
		console.show();
	}

	public static void consolePrint(String message) {
		if (console != null) console.addMessage(message);

		// temp logging feature. disable later
		message = message.replace(Protocol.separator, ' ');
		message += Protocol.separator;
		log += message;
	}

	public static void closeConsole() {
		if (console != null)
			console.dispose();
		console = null;
	}

	public static void log(String message) {
		log += message;
	}

	public static String getLog() {
		return log;
	}

	public static void clearLog() {
		log = "";
	}
}
