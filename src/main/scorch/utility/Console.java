package scorch.utility;

import scorch.Network;
import scorch.Protocol;

import java.awt.*;
import java.awt.event.*;

/**
 * @author Mikhail Kruk
 * @author Alexander Rasin
 */
class Console extends Frame implements FocusListener, ActionListener {
	private final TextArea textArea;
	private final TextField textField;
	private final Network network;

	public Console(Network network) {
		super("Scorch Debug Console");

		addWindowListener(new ConsoleWindowListener());

		this.network = network;

		setLayout(new BorderLayout());
		textArea = new TextArea(20, 80);
		textArea.addFocusListener(this);
		textField = new TextField(80);
		textField.addActionListener(this);

		Panel buttonPanel = new Panel(new FlowLayout());

		Button allButton = new Button("Select All");
		allButton.addActionListener(this);
		buttonPanel.add(textField);
		buttonPanel.add(allButton);

		Button clearButton = new Button("Clear");
		clearButton.addActionListener(this);
		buttonPanel.add(clearButton);

		textArea.setEditable(false);
		add(textArea, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		pack();
	}

	public void focusLost(FocusEvent event) {
	}

	public void focusGained(FocusEvent event) {
		if (event.getComponent() == textArea)
			textField.requestFocus();
	}

	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == textField) {
			String message = textField.getText();
			message = message.replace(' ', Protocol.separator);
			network.sendMessage(message);
			textField.setText("");
		}
		if (event.getActionCommand().equals("Select All"))
			textArea.selectAll();
		if (event.getActionCommand().equals("Clear"))
			textArea.setText("");
	}

	public void addMessage(String messageString) {
		messageString = messageString.replace(Protocol.separator, ' ');
		textArea.append(messageString + "\n");
	}

	class ConsoleWindowListener extends WindowAdapter {
		public void windowClosing(WindowEvent event) {
			Debug.closeConsole();
		}
	}
}
