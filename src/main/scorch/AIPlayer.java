package scorch;

import scorch.utility.Debug;
import scorch.weapons.ExplosionInfo;
import scorch.weapons.RoundMissile;
import scorch.weapons.SimpleExplosion;

/**
 * This class implements all types of AIPlayers. For now all three types
 * have the same strategy and differ only in some constants which are
 * supposed to determine how good AI player is.
 *
 * @author Mikhail Kruk
 */
public class AIPlayer extends ScorchPlayer implements Runnable {
	public static final String[] AI_TYPES = {"Shooter", "Cyborg", "Killer"};
	public static final int NUMBER_OF_TYPES = AI_TYPES.length;

	private static final long[] bounty = {10000, 20000, 30000};
	private static final int[] accuracy = {5, 4, 2}; // lower is better
	private static final double[] radiusFactor = {3.0, 2.0, 1.5};

	private int type; // AI type, index in the above arrays

	private Thread thread;

	public AIPlayer(int id, PlayerProfile profile, ScorchGame owner) {
		super(id, profile, owner);

		String name = profile.getName();
		for (type = 0; type < NUMBER_OF_TYPES; type++)
			if (name.equals(AI_TYPES[type]))
				break;

		if (type >= NUMBER_OF_TYPES)
			System.err.println("AIPlayer: internal error, unknown ai type: " + name);
	}

	// override this to make AI tanks look different
	public void setTankType(int tankType) {
		this.tankType = type;
	}

	public long getBounty() {
		return bounty[type];
	}

	public int getAccuracy() {
		return accuracy[type];
	}

	public double getRadiusFactor() {
		return radiusFactor[type];
	}

	public void makeTurn() {
		if (isFirstTurn())
			autoDefense();
		else
			aimedFire();
	}

	private void aimedFire() {
		thread = new Thread(this, "ai-aimer-thread");
		thread.start();
	}

	public void buyAmmunition() {
		//System.out.println(getName()+" buying ammo for "+getCash()); // TODO
	}

	private void autoDefense() {
		if (useAutoDefense()) {
			//System.out.println(getName()+" using AutoDefense"); // TODO
		}
		owner.sendEOT();
	}

	public void run() {
		aim();
		owner.aiFire(this);
	}

	// The algorithm for AI aiming
	// Just tries to shoot at different angles with different power until it finds a target
	// TODO: Here is the list of improvements which *must* be done to AI:
	//   - different weapons
	//   - normal strength selection
	//   - try to fire in direction of opponent even if can't hit directly
	private void aim() {
		RoundMissile missile;
		ExplosionInfo explosionInfo;
		int startAngle = getAngle(), startPower = getPower();
		int currentAngle, currentPower, accuracy;
		double radiusFactor;

		accuracy = getAccuracy();
		radiusFactor = getRadiusFactor();

		currentAngle = getAngle();
		do {
			// power increment depends on accuracy,
			// accuracy is angle increment. confused? good.
			currentPower = increasePower(10 * accuracy);
			Debug.println("ai is aiming, power is: " + currentPower + " angle: " +
				getAngle() + " startPower: " + startPower);

			if (Math.abs(currentPower - startPower) < 10 * accuracy) {
				currentPower = startPower;
				currentAngle = increaseAngle(accuracy);
				if (Math.abs(currentAngle - startAngle) < accuracy)
					currentAngle = startAngle;
			}

			// make a fake missile to see were it lands
			int angle = getAngle();

			missile = new RoundMissile(
				bitmap,
				new Physics(
					getTurretX(2.0),
					bitmap.getHeight() - getTurretY(2.0),
					angle,
					getPower() / 8.0
				),
				new SimpleExplosion(
					bitmap,
					(int) (SimpleExplosion.MISSILE * radiusFactor)
				)
			);

			// to determine if there was an explosion at all
			explosionInfo = missile.getExplosionInfo();

			try {
				Thread.sleep(3);
			}
			catch (InterruptedException error) {}
		}
		while ((explosionInfo == null || scorchField.killTanks(missile, this, true) <= 0) &&
			(currentAngle != startAngle || currentPower != startPower));
	}
}
