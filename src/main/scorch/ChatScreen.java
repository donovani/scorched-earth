package scorch;

import scorch.gui.ChatPanel;
import scorch.gui.GamesPanel;
import scorch.gui.UsersPanel;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
public class ChatScreen extends Panel {
	private static final String[] COMMANDS = {
		"/exit",
		"/create",
		"/join",
		"/me",
		"/msg",
		"/observe",
		"/whois",
		"/version",
		"/ping"
	};

	private static final double
		chatSize = 0.8,
		gamesSize = 0.4,
		controlsSize = 0.4;

	private final ChatPanel chatPanel;
	private final GamesPanel gamesPanel;
	private final UsersPanel usersPanel;

	public ChatScreen(int width, int height, ScorchGame owner) {
		super();

		setLayout(null);

		chatPanel = new ChatPanel(0, (int) (height * gamesSize), (int) (width * chatSize), (int) ((1 - gamesSize) * height), owner.container);
		gamesPanel = new GamesPanel(0, 0, (int) (width * chatSize), (int) (height * gamesSize), owner.container);
		usersPanel = new UsersPanel((int) (width * chatSize), 0, (int) ((1 - chatSize) * width), height, owner.container);

		chatPanel.display();
		gamesPanel.display();
		usersPanel.display();
	}
}
