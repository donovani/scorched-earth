package scorch;

import java.awt.*;

class Tooltip {
	private static final Color hintColor = new Color(255, 255, 223);

	private String tip;
	private int x, y;
	private final ScorchField owner;

	private FontMetrics fontMetrics = null;
	private int fontHeight;

	public Tooltip(ScorchField owner) {
		this.owner = owner;
	}

	public void setText(String text, int x, int y) {
		boolean update = ((text != null && !text.equals(tip)) || (text == null && tip != null));

		if (update) {
			this.tip = text;
			this.x = x;
			this.y = y;
			owner.repaint();
		}
	}

	public void paint(Graphics graphics) {
		int dx, dy;
		int ownerWidth = owner.getWidth(), ownerHeight = owner.getHeight();

		if (tip == null) return;

		if (fontMetrics == null) {
			fontMetrics = owner.getFontMetrics(owner.getFont());
			fontHeight = fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();
		}

		int stringWidth = fontMetrics.stringWidth(tip) + 6;
		if (x + stringWidth > ownerWidth)
			dx = ownerWidth - stringWidth - 1;
		else
			dx = x;

		dy = y - fontHeight;

		graphics.setColor(hintColor);
		graphics.fillRect(dx, dy, fontMetrics.stringWidth(tip) + 6, fontHeight + 4);
		graphics.setColor(Color.black);
		graphics.drawRect(dx, dy, fontMetrics.stringWidth(tip) + 6, fontHeight + 4);
		graphics.drawString(tip, dx + 3, dy + 2 + fontMetrics.getMaxAscent());
	}
}
