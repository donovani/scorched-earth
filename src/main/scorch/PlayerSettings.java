package scorch;

/**
 * Player setting go in here
 * @author Mikhail Kruk
 */
public class PlayerSettings {
	public int tankType;
	public boolean sounds;

	public PlayerSettings(int tankType, boolean soundsLoaded) {
		this.tankType = tankType;
		this.sounds = soundsLoaded;
	}

	public PlayerSettings(PlayerProfile profile) {
		this(profile.getTankType(), profile.getSounds());
	}

	public String toString() {
		return "" + tankType + Protocol.separator + sounds;
	}
}
