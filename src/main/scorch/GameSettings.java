package scorch;

/**
 * Game settings are stored in this class before being sent
 * to the server as well as after they are received from server
 * @author Mikhail Kruk
 */
public class GameSettings {
	public static final int NO_WIND = 1, CONSTANT_WIND = 0, CHANGING_WIND = 2;

	public float gravity;
	public int maxRounds;
	public boolean hazards;
	public int wind;
	public long initialCash;
	public boolean lamerMode;

	public GameSettings(float gravity, int maxRounds, boolean hazards, int wind, long initialCash, boolean lamerMode) {
		this.gravity = gravity;
		this.maxRounds = maxRounds;
		this.hazards = hazards;
		this.wind = wind;
		this.initialCash = initialCash;
		this.lamerMode = lamerMode;
	}

	public GameSettings(PlayerProfile profile) {
		this(profile.getGravity(), profile.getNumberOfRounds(),
			profile.getHazards(), profile.getWind(), profile.getInitialCash(),
			profile.getLamerMode());
	}

	public String toString() {
		return "" + gravity + Protocol.separator + maxRounds + Protocol.separator +
			hazards + Protocol.separator + wind + Protocol.separator + initialCash +
			Protocol.separator + lamerMode;
	}
}
