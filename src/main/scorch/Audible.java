package scorch;

import scorch.utility.Debug;

import java.applet.AudioClip;
import java.util.Vector;

/**
 * Signifies that the implementing supports sounds
 *
 * @author Nathan Roslavker
 * @author Mikhail Kruk
 */
public abstract class Audible {
	protected static Vector sounds;

	protected void startSound(int index) {
		if (sounds != null && sounds.size() > index && ScorchGame.sounds)
			((AudioClip) sounds.elementAt(index)).play();
	}

	protected void loopSound(int index) {
		if (sounds != null && sounds.size() > index)
			((AudioClip) sounds.elementAt(index)).loop();
	}

	protected void stopSound(int index) {
		if (sounds != null && sounds.size() > index)
			((AudioClip) sounds.elementAt(index)).stop();
	}

	protected static int addSound(AudioClip sound) {
		if (sounds == null)
			sounds = new Vector();
		sounds.addElement(sound);

		return sounds.indexOf(sound);
	}

	protected static void loadSounds(ScorchGame owner) {
		Debug.println("empty load sounds call");
		sounds = null;
	}
}
