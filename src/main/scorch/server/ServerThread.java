package scorch.server;

import scorch.PlayerProfile;
import scorch.Protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.StringTokenizer;

/**
 * ServerThread is a separate thread that is run for each connecting client.
 * This thread is represents the client on the server side.
 * It accepts messages the client sends, & executes actions accordingly.
 * Each logged-in client has access to the game it is in.
 * ServerThread is added to the game, has a reference to its game (for broadcasts),
 * and can ask the server when the player is attempting to log in,
 * to make sure login is valid.
 *
 * @author Alexander Rasin
 */
public class ServerThread extends Player implements Runnable {

	private final static long PING_TIME = 20 * 1000;
	private final static long PONG_TIMEOUT = 60 * 1000;

	private Socket socket;
	private String hostName;

	//input and output streams
	private BufferedReader in = null;
	private PrintWriter out = null;
	private final Thread thread;
	private long pingTime, pongTime;
	private String password = null, resolution = "";
	private boolean isMaster = false;
	private boolean keepRunning = true, madeTurn = false;

	public ServerThread(Socket socket) {
		this.socket = socket;
		pingTime = pongTime = System.currentTimeMillis();
		thread = new Thread(this);
		thread.start();
	}

	protected void makeMaster() {
		isMaster = true;
		//sendMessage("MASTER");
	}

	public void setReady(boolean val) {
		ready = val;
	}

	public boolean madeTurn() {
		//	System.out.println("RETURNING " + made_turn);
		return madeTurn;
	}

	public boolean isMaster() {
		return isMaster;
	}

	// Note that this method will try to resolve the host of the client (unless it already has it).
	// So method will be used in the ScorchShell
	public String getHostName() {
		if (hostName == null)
			hostName = socket.getInetAddress().getHostName();

		return hostName;
	}

	/**
	 * Ping the client if it has been idle too long.
	 * Disconnect if timeout has been reached.
	 */
	private void checkPing() {
		long now = System.currentTimeMillis();
		if (now - pongTime > PONG_TIMEOUT) {
			System.out.println("Player " + name + " id " + id + " timed out");
			//CHANGE THIS... PROTOCOL
			dropPlayer("your connection timed out.");
		} else if (now - pingTime > PING_TIME) {
			sendMessage(Protocol.ping);
			pingTime = now;
		}
	}

	public void sendMessage(String msg) {
		out.println(msg);
	}

	private String getMessage() {
		try {
			while (keepRunning && !in.ready()) {
				checkPing();
				Thread.sleep(500);
			}
			// In the event the thread is already disconnected
			if (!keepRunning)
				return null;

			String msg = in.readLine();
			return msg;
		} catch (Exception e) {
			System.err.println("SERVER: Reading from " + in + " failed " + e);
			dropPlayer(null);
		}

		return null;
	}

	/**
	 * Drop a player from a game.
	 * @param reason A text reason for disconnection. If reason == null, then the player quit.
	 */
	public void dropPlayer(String reason) {
		if (reason != null)
			sendMessage(Protocol.disconnect + Protocol.separator +
				"You've been disconnected because " + reason);

		if (myGame != null) {
			// Actually leave the game.
			// The player is removed first, so that the broadcasts only go to other players.
			myGame.leave(this);
		}

		if (myGame != null) {
			myGame.checkTurn();
			myGame = null;
		}

		disconnect();
	}

	private synchronized void disconnect() {
		if (socket == null)
			return;
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			System.err.println(e);
		}
		socket = null;
		keepRunning = false;
	}

	/**
	 * See if the message received is a valid login.
	 * The player cannot join a game unless they provide a valid login.
	 * This method parses the input and reads the profile from server if applicable.
	 * @param message The raw input to be passed to the server
	 * @return True if login was successful
	 */
	private boolean logIn(String message) {
		password = "";
		boolean loggedIn = false;

		StringTokenizer tokenizer = new StringTokenizer(message, "" + Protocol.separator);

		if (tokenizer.countTokens() < 2)
			return false;

		if (((message = tokenizer.nextToken()).equals(Protocol.login)) ||
			(message.equals(Protocol.newPlayer))) {
			if (!message.equals(Protocol.newPlayer)) {
				if (tokenizer.hasMoreTokens())
					name = tokenizer.nextToken();
				if (tokenizer.hasMoreTokens())
					password = tokenizer.nextToken();
				//game resolution.
				if (tokenizer.hasMoreTokens())
					resolution = tokenizer.nextToken();
			}

			profile = ScorchServer.lookupPlayer(name);

			//System.out.println(" request " + message + " found " + profile);

			if (message.equals(Protocol.newPlayer) && (profile == null)) {
			/*	if (tokenizer.hasMoreTokens())
			    profile = new PlayerProfile
				(name, password, tokenizer.nextToken());
			else
			    profile = new PlayerProfile(name, password,"");
			*/
				profile = new PlayerProfile(tokenizer);
				name = profile.getName();
				password = profile.getPassword();
				while (tokenizer.hasMoreTokens())
					resolution += tokenizer.nextToken();

				if (ScorchServer.lookupPlayer(name) != null)
					profile = new PlayerProfile(null, "", "");
				else
					// Create a new player and write their profile to disk
					ScorchServer.newPlayer(profile);
			} else
				//new user command failed
				if (message.equals(Protocol.newPlayer))
					profile = new PlayerProfile(null, null);

			// Guest login. password = desired user name
			if (name.equals(Protocol.guest)) {
				if (ScorchServer.lookupPlayer(password) == null) {
					profile = new PlayerProfile(name, password, "");
					name = password;
					password = Protocol.guest;
				} else
					//guest
					profile = new PlayerProfile(null, null);
			}

			if ((profile != null) && (profile.getName() == null)) {
				sendMessage(Protocol.loginFailed + Protocol.separator +
					name + Protocol.separator +
					Protocol.usernameTaken);
				loggedIn = false;
			} else if (ScorchServer.alreadyPlaying(name)) {
				//System.out.println(ScorchServer.alreadyPlaying(name));
				sendMessage(Protocol.loginFailed + Protocol.separator +
					name + Protocol.separator +
					Protocol.alreadyLoggedIn);
				loggedIn = false;
			} else if (profile == null) {
				sendMessage(Protocol.loginFailed + Protocol.separator +
					name + Protocol.separator +
					Protocol.wrongUsername);
				loggedIn = false;
			} else if ((password == null || !password.equals(profile.getPassword()))) {
				sendMessage(Protocol.loginFailed + Protocol.separator +
					name + Protocol.separator +
					Protocol.wrongPassword);
				loggedIn = false;
			} else
				loggedIn = true;
		}
		return loggedIn;
	}

	/**
	 * Initialize input/output streams for connection
	 */
	private void initStreams() {
		try {
			in = new BufferedReader(new InputStreamReader
				(socket.getInputStream()));
			//setting autoflush to true... wonder if it works
			out = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			System.err.println("ServerThread: connection failed " + e);
			System.exit(1);
		}
	}

	/**
	 * the run method of the thread.
	 * It continuously listens to client messages and responds/performs appropriate action.
	 */
	public void run() {
		String message, command;
		StringTokenizer tokenizer;
		boolean loggedIn = false;

		initStreams();

		do {
			//wait for a successful login command (or a quit command)
			message = getMessage();

			if (message == null || message.equals(Protocol.quit)) {
				disconnect();
				return;
			}
		}
		while (!logIn(message));

		System.out.println(" User name " + name + " logged in.");

		myGame = ScorchServer.register(this, resolution);
		checkPing();

		while (keepRunning && ((message = getMessage()) != null)) {
			pingTime = pongTime = System.currentTimeMillis();

			tokenizer = new StringTokenizer(message, Protocol.separator + "");

			// No command (empty string?)
			if (tokenizer.countTokens() == 0)
				continue;

			// The client command
			command = tokenizer.nextToken();

			// Answer to ping can be ignored
			if (command.equals(Protocol.pong))
				continue;

			// shout and setgameoptions needs to be simply broadcasted
			if (command.equals(Protocol.shout)) {
				System.out.println(message);
				myGame.broadcast(message);
				continue;
			}
			if (command.equals(Protocol.jvmInfo)) {
				jvm = tokenizer.nextToken();
				continue;
			}
			// Memorize game options (send them then the game starts)
			if (command.equals(Protocol.setGameOptions) && isMaster()) {
				myGame.gameStarted();
				myGame.setOptions(message);

				// Check if all the options necessary have been
				// received and the clients can be initialized
				myGame.checkInitialize();
				continue;
			}
			if (command.equals(Protocol.endOfTurn)) {
				ready = true;
				madeTurn = false;
				// If all players are ready for the next turn, notify clients of who goes next

				//DEBUG
				myGame.checkSync(message, name);

				myGame.checkTurn();
				continue;
			}
			if (command.equals(Protocol.clientLog)) {
				//DEBUG?
				Disk.recordClientLog(message, id);
				continue;
			}

			// Memorize the player options. Will broadcast them later.
			if (command.equals(Protocol.setPlayerOptions)) {
				playerOptions = message;
				// Check if all the options neccesary have been received and the clients can be initialized
				myGame.checkInitialize();
				continue;
			}

			// Player used a weapon in his turn. Broadcast, but note the fact.
			if (command.equals(Protocol.useWeapon)) {
				madeTurn = true;

				myGame.broadcast(message);
				continue;
			}

			// Player says something to a player from his game.
			if (command.equals(Protocol.say)) {
				int plid = Integer.parseInt(tokenizer.nextToken());
				String said = tokenizer.nextToken();
				Player p = null;

				p = myGame.findPlayerByID(plid);
				if (p != null) {
					p.sendMessage
						(Protocol.say + Protocol.separator + said);
					if (id != plid)
						sendMessage
							(Protocol.say + Protocol.separator + said);
				} else
					sendMessage(Protocol.say + Protocol.separator +
						"Message not delivered, player left the game (" + said + ")");

				continue;
			}

			// The user requested to change their profile
			if (command.equals(Protocol.changeProfile)) {
				// Make a new profile (and cut out the command that goes in front of string message)
				profile = new PlayerProfile
					(message.substring(Protocol.changeProfile.length() + 1
					));

				if (profile.getPassword().equals("*"))
					profile.setPassword(password);
				else
					password = profile.getPassword();

				ScorchServer.changeProfile(profile);
				continue;
			}

			// A player died. The master client will notify us.
			if (command.equals(Protocol.playerDead)) {
				if (isMaster)
					myGame.playerDied
						((Integer.parseInt(tokenizer.nextToken())));
				continue;
			}

			// Master performs a mass kill. Done to finish a round.
			if (command.equals(Protocol.massKill)) {
				if (isMaster)
					myGame.massKill(this);
				continue;
			}

			// Maximum players in the game
			if (command.equals(Protocol.setMaxPlayers)) {
				if (tokenizer.hasMoreTokens())
					myGame.setMaxPlayers
						(Integer.parseInt(tokenizer.nextToken()));
				continue;
			}

			// Add AI player
			if (command.equals(Protocol.addAiPlayer)) {
				if (!tokenizer.hasMoreTokens())
					sendMessage(Protocol.commandFailed);
				else {
					// Client checks number of AI players added
					myGame.reserveSpot();
					// Add new AI player of type that was recieved
					myGame.addPlayer(new ServerAIPlayer(tokenizer.nextToken()),
						Protocol.aiLoggedIn);
				}
				continue;
			}

			if (command.equals(Protocol.topTen)) {
				sendMessage(ScorchServer.getTopTen());
				continue;
			}

			if (command.equals(Protocol.quit)) {
				dropPlayer(null);
				continue;
			}

			// This is a temporary (sic?) - unknown messages are broadcast for debug purposes.
			myGame.broadcast(message);
		}
		disconnect();
	}
}

