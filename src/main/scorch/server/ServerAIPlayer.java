package scorch.server;

import scorch.PlayerProfile;
import scorch.Protocol;

/**
 * This is a class for AI player.  Since there is no separate client for AI
 * player (AI players are stored on the master client), it does not implement
 * send message.
 * It also overrides the unsetReady since in AI client ready is defaulted to
 * be true (no separate client, no actions to draw).
 * @author Alexander Rasin
 */
public class ServerAIPlayer extends Player {
	public ServerAIPlayer(String type) {
		this.name = type;
		this.ready = true;

		this.profile = new PlayerProfile(name, "*", "");
	}

	public synchronized void setGame(Game g, int pl_id) {
		this.myGame = g;
		id = pl_id;
		playerOptions = Protocol.setPlayerOptions + Protocol.separator +
			id + Protocol.separator + 0;
	}

	public void dropPlayer(String reason) {
		myGame.leave(this);
	}

	public String getHostName() {
		return "On the master's client";
	}

	//AI is always ready as it does not draw (has no separate client)
	public void setReady(boolean val) {
		// Nothing
	}
}

