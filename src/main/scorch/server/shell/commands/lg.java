package scorch.server.shell.commands;

import scorch.server.ScorchServer;
import scorch.server.shell.ServerShell;

import java.util.Vector;

/**
 * Command to list all current games
 */
public class lg extends shellCommand {
	public static String help = "To list the current games";

	//arguments are ignored here for now.
	public static void run(Vector args, Object owner) {
		((ServerShell) owner).print(ScorchServer.allGamesToString());
	}
}
