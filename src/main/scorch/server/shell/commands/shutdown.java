package scorch.server.shell.commands;

import scorch.server.shell.ServerShell;

import java.util.Vector;

/**
 * Shutdown the server.
 * Stops listening on the port and disables access to the profile database.
 * The games will be able to run to conclusion.
 */
public class shutdown extends shellCommand {
	public static String help = "Shut down the server. games finished without access to profile db.";

	public static void run(Vector args, Object owner) {
		((ServerShell) owner).println("Shutting down server...");
		((ServerShell) owner).shutdownServer();
	}
}
