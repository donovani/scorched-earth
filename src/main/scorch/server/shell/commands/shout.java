package scorch.server.shell.commands;

import scorch.Protocol;
import scorch.server.Game;
import scorch.server.ScorchServer;
import scorch.server.shell.ServerShell;

import java.util.Vector;

/**
 * Command to "shout" to all players in a game.
 */
public class shout extends shellCommand {
	static Game g;
	public static String help = "shout something to all player in a game.";

	//arguments are ignored here for now.
	public static void run(Vector args, Object owner) {
		ServerShell shell = (ServerShell) owner;
		String message = "";

		if (args.size() == 0) {
			shell.println("Usage: shout GameID [message]");
			return;
		}

		try {
			//yet to learn of a better way to check if string is number
			try {
				g = ScorchServer.findGameByID
					(Integer.parseInt
						(args.elementAt(0).toString()));
			} catch (NumberFormatException e) {
				g = null;
			}

			for (int i = 1; i < args.size(); i++)
				message = message + " " + args.elementAt(i);

			if (g == null)
				shell.println("No such game: " + args.elementAt(0));
			else
				g.broadcast(Protocol.say + Protocol.separator +
					"(Message from Server):" + message);
		} catch (Exception e) {
			shell.println("say failed: " + e);
		}
	}
}
