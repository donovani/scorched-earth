package scorch.server.shell.commands;

import scorch.server.ScorchServer;
import scorch.server.shell.ServerShell;

import java.util.Vector;

/**
 * Command to print the number of desyncs (mostly for debugging)
 */
public class desync extends shellCommand {
	public static String help = "To print current number of desyncs.";

	//arguments are ignored here for now.
	public static void run(Vector args, Object owner) {
		((ServerShell) owner).println("Observed number of desyncs so far: " +
			ScorchServer.getDesyncCount() + " out of "
			+ ScorchServer.potentialDesyncCount
			+ " opportunities.\n");
	}
}
