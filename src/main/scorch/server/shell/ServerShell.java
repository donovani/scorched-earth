package scorch.server.shell;

import scorch.server.ScorchServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * The Scorch Server Shell (console)
 */
public class ServerShell implements Runnable {
	private static final String prompt = "ServerShell-> ";
	private static final String separators = " ";
	private final Class[] argType = new Class[2];

	private final String[] commandNames = {"lg", "boot", "help", "say", "whois",
		"desync", "passwd", "shout", "addkg",
		"shutdown"};
	private final String[] commandHelp = new String[commandNames.length];
	private final Hashtable commandsHash = new Hashtable();
	private final Hashtable classHash = null;//new Hashtable();

	private ScorchServer owner = null;
	private BufferedReader in = null;
	private PrintWriter out = null;
	private Socket socket = null;


	public ServerShell(ScorchServer ss) {
		super();

		this.owner = ss;
		// Take over standard input and output
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new PrintWriter(System.out, true);
	}

	// Alternative constructor to use for spawning by RemoteServerShell
	public ServerShell(BufferedReader i, PrintWriter o, Socket s) {
		super();

		this.socket = s;
		this.in = i;
		this.out = o;
	}

	/**
	 * Provides common interface for output of all commands.
	 * @param message The text to be passed to the output stream
	 */
	public void print(String message) {
		out.print(message);
		out.flush();
	}

	/**
	 * Provides common interface for output of all commands.
	 * @param message The text to be passed to the output stream
	 */
	public void println(String message) {
		out.println(message);
	}

	private boolean executeCommand(String commandString) {
		Object[] args = new Object[2];
		String command = null;
		Vector v = new Vector();
		Method commandMethod = null;

		if (commandString == null) return true;

		StringTokenizer tokenizer = new StringTokenizer(commandString, separators);

		if (tokenizer.hasMoreTokens())
			command = tokenizer.nextToken();

		if (command == null)
			return true;

		if ("quit".equals(command))
			return false;

		//get the rest of arguments
		while (tokenizer.hasMoreTokens())
			v.addElement(tokenizer.nextToken());

		args[0] = v;
		args[1] = this;

		commandMethod = (Method) commandsHash.get(command);

		try {
			if (commandMethod == null)
				println("Command " + command + " not found.\n");
			else
				commandMethod.invoke(null, args);
		} catch (Exception e) {
			println("Invoke failed: " + e);
		}

		return true;
	}

	private void loadCommands() {
		Class commandClass;
		try {
			argType[0] = Class.forName("java.util.Vector");
			argType[1] = Class.forName("java.lang.Object");
		} catch (Exception e) {
			println("" + e);
			System.exit(1);
		}

		print("Loading commands ");

		for (int i = 0; i < commandNames.length; i++) {
			try {
				commandClass = Class.forName("scorch.server.shell.commands." + commandNames[i]);

				commandHelp[i] = commandNames[i] +
					"\t\t" +
					commandClass.getField("help").get(null) +
					"\n";

				commandsHash.put(commandNames[i], commandClass.getMethod("run", argType));
				print(". ");
			} catch (Exception e) {
				println("Command " + commandNames[i]
					+ " not found.\n" + e);
			}
		}
		println("Done.\n");
	}

	/*
	protected void initStreams()
	{
		// Take over standard input.
		in = new BufferedReader(new InputStreamReader(System.in));
	}
	*/

	public void run() {
		//load the available commands into the hashtable
		loadCommands();

		print(prompt);
		try {

			while (this.executeCommand(in.readLine()))
				print(prompt);

		} catch (Exception e) {
			System.err.println("Shell error: " + e);
		}

		println("\nExiting... Goodbye");
		disconnect();
	}

	public String[] getCommandHelp() {
		return commandHelp;
	}

	private void disconnect() {
		try {
			in.close();
			out.close();
			if (socket != null)
				socket.close();
		} catch (IOException e) {
			System.err.println("Shell: disconnect failed " + e);
		}
	}

	public void shutdownServer() {
		owner.shutdown();
	}
    /*
    public static void main(String[] args)
    {
	new Thread(new ServerShell()).start();
    }
    */
}
