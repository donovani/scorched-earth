package scorch.server;

import scorch.PlayerProfile;
import scorch.Protocol;
import scorch.server.shell.ServerShell;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

/**
 * ScorchServer class is the class representing the actual server.
 * <p>
 * When client connection is established, it starts a ServerThread
 * that represents the client on the server side.
 * <p>
 * ScorchServer also keeps track of the games in order to be able to
 * run global checks for a user that has already in another game.
 * <p>
 * It also has access to class Disk that stores and looks up profiles -
 * games can call on the Server in order to lookup or write a profile of
 * a player.
 * <p>
 * Once logged in successfully, all the clients register on the server
 * that either assigns a client to existing game or makes a new one if
 * there are no spots available.
 *
 * @author Alexander Rasin
 */
public class ScorchServer {
	public static final String
		version = "Scorched Earth 2000 Server, v1.271, 07/26/2000";

	private Socket accept = null;
	private ServerSocket socket = null;
	private final int port;
	private static int gameCount = 0;
	public static int potentialDesyncCount = 0;

	//all the games on the server
	private static final Vector games = new Vector();
	//responsible for profile storage and lookup
	private static Disk disk = null;
	private static final Vector topTen = new Vector();
	//can modify top ten to top 'any other number'
	private static final int topX = 10;

	public static void main(String[] args) {
		disk = new Disk();
		ScorchServer server = null;
		ServerShell shell = null;

		System.out.println(version + "\n");
		System.out.println(
			"Copyright (C) 1999-2000 KAOS Software\n" +
			"Scorched Earth 2000 comes with ABSOLUTELY NO WARRANTY;\n" +
			"This is free software, and you are welcome to redistribute it under certain conditions.\n" +
			"Please read the LICENSE for details."
		);

		try {
			//new Thread(new RemoteServerShell()).start();
			if (args.length > 0)
				server = new ScorchServer(Integer.parseInt(args[0]));
			else
				server = new ScorchServer(4242);

			shell = new ServerShell(server);
			new Thread(shell).start();
			server.runServer();
		} catch (Throwable t) {
			System.out.println("ERROR starting server: " + t);
		}
		System.exit(0);
	}

	public ScorchServer(int port) {
		this.port = port;
		//runServer();
	}

	/**
	 * Check if a player is already participating in a game running on the server
	 */
	public static synchronized boolean alreadyPlaying(String name) {
		Game game = null;

		//see if the player is already logged into one of the games
		for (int i = 0; i < games.size(); i++) {
			game = (Game) games.elementAt(i);
			if (game.alreadyPlaying(name))
				return true;
		}
		return false;
	}

	/**
	 * Lookup a player in the profile hashtable
	 */
	public static synchronized PlayerProfile lookupPlayer(String name) {
		return disk.getProfile(name);
	}

	//write the hashtable to disk
	//public static synchronized void writeTable()
	//{
	//	disk.writeTable();
	//}

	//makes a new player in the profile database.
	public static synchronized void newPlayer(PlayerProfile profile) {
		disk.add(profile);
		//writeTable();
	}

	//change user's profile (as requested by the user)
	public static synchronized void changeProfile(PlayerProfile profile) {
		//System.out.println("CHANGING " + profile);
		disk.change(profile);
		//System.out.println("RESULT " + lookupPlayer(profile.getName()));
		//writeTable();
	}

	/**
	 * Removes the user profile
	 */
	public static synchronized void deleteProfile(PlayerProfile profile) {

	}

	public static String allGamesToString() {
		String all = "";
		Game g = null;
		int multi_player = 0;

		for (int i = 0; i < games.size(); i++) {
			g = (Game) games.elementAt(i);
			if (g.getHumanCount() > 1) multi_player++;
			all = all + g + "\n";
		}

		all += "Total of " + games.size() + " games, " + multi_player +
			" multiplayer.\n\n";

		return all;
	}

	public static int getDesyncCount() {
		return Disk.getDesyncCount();
	}

	public static int getGameCount() {
		return gameCount;
	}

	public static Game findGameByID(int index) {
		Game g;

		for (int i = 0; i < games.size(); i++) {
			g = ((Game) games.elementAt(i));
			if (g.getID() == index)
				return g;
		}

		return null;
	}

	public static PlayerProfile findProfileByName(String name) {
		return disk.findProfileByName(name);
	}

	public static Player findPlayerByName(String name) {
		Player pl = null;

		for (int i = 0; i < games.size(); i++) {
			pl = ((Game) games.elementAt(i)).findPlayerByName(name);
			if (pl != null)
				return pl;
		}

		return null;
	}

	public static void insertTopTen(PlayerProfile profile) {
		int position = 0;

		//the profile is already in the top list
		//	if (topTen.contains(profile))
		//    return;

		for (int i = 0; i < topTen.size(); i++)
			if (profile.getName().equals
				(((PlayerProfile) topTen.elementAt(i)).getName()))
				topTen.removeElementAt(i);

		while (position < topX && position < topTen.size()) {
			if (profile.getOverallCashGained() >
				((PlayerProfile) topTen.elementAt
					(position)).getOverallCashGained())
				break;

			position++;
		}

		if (position == topTen.size() && !(position < topX))
			return;

		//System.out.println("Inserting " + profile.getName() + " at " + position);
		topTen.insertElementAt(profile, position);

		//if we are over the size of hall of fame, remove the last player
		if (topTen.size() > topX)
			topTen.removeElement(topTen.lastElement());
	}

	public static String getTopTen() {
		String result = Protocol.topTen + Protocol.separator;

		for (int i = 0; i < topTen.size(); i++)
			result = result + topTen.elementAt(i) +
				Protocol.separator;

		return result;
	}

	/**
	 * Register a logged in player.
	 * Essentially adds a player to a game they will participate in.
	 * If there is no available game - the server will make a new one.
	 * A player can only participate in the game that matches his resolution.
	 * @param player The ServerThread representing a player
	 * @param resolution The target game resolution it's played in
	 * @return A new Game instance that the player has joined
	 */

	public static Game register(ServerThread player, String resolution) {
		Game game = null;

		// Find an empty game and add the player to it.
		for (int i = 0; i < games.size(); i++) {
			game = (Game) games.elementAt(i);
			if (game.reserveSpot(resolution)) {
				game.addPlayer(player, Protocol.loggedIn);
				return game;
			}
		}

		// No available games. Make a new one and add the player to it
		game = new Game(gameCount++, resolution);
		games.addElement(game);
		game.addPlayer(player, Protocol.loggedIn);
		return game;
	}

	public static void removeGame(Game game) {
		games.removeElement(game);
	}

	public static void broadcast(String message) {
		for (int i = 0; i < games.size(); i++)
			((Game) games.elementAt(i)).broadcast(message);
	}

	/**
	 * Stop listening for connection & disable disk access
 	 */
	public void shutdown() {
		broadcast(
			Protocol.say + Protocol.separator + "Server is shutting down." +
			"You may continue playing, but all profile modifications (such as changing password) will be lost"
		);
		broadcast(
			Protocol.say + Protocol.separator + "If you notice any " +
			"strange behavior in the game please report to " +
			"alexr@scorch2000.com"
		);
		try {
			socket.close();
		} catch (IOException e) {
			System.out.println("ScorchServer: error shutting down " + e);
		}
		disk.shutdown();
	}

	public void runServer() {
		ServerThread st = null;
		try {
			// Defines maximum of connections in a queue
			socket = new ServerSocket(port, 5);

			while (true) {
				//System.out.println("SERVER: accepting on port "+port);
				accept = socket.accept();

				st = new ServerThread(accept);

				Thread.yield();
			}
		} catch (Exception e) {
			//System.err.println("SERVER: listening failed "+ e);
			//System.exit(-1);
		}

		try {
			while (games.size() > 0)
				Thread.sleep(1000);
		} catch (Exception e) {
		}
	}
}
