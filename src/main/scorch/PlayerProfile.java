package scorch;

import scorch.utility.Crypt;
import scorch.utility.Debug;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * The profile of the player sent from the server
 * @author Mikhail Kruk
 */
public class PlayerProfile implements Serializable {
	// If username is guest the user is considered guest, their profile is not stored,
	// and the username is extracted from the password field.

	// fields stored in profile
	private String name, password;
	private String email;

	private int overallKills;
	private long overallCashGained; // Total amount of money gained

	private int tankType = -1, numberOfRounds = -1, wind = -1;
	private float gravity = -1;
	private long initialCash = -1;
	private boolean hazards = false;
	private long timeCreated, timeAccessed;
	private boolean sounds = false;

	// run-time data
	private boolean isGuest = false;
	private boolean modified = false; // dirty flag for server needs
	private boolean lamerMode = false; // do not store this

	// constructors
	public PlayerProfile(String profile) {
		this(new StringTokenizer(profile, Protocol.separator + ""));
	}

	public PlayerProfile(StringTokenizer tokenizer) {
		try {
			this.name = tokenizer.nextToken();
			this.password = tokenizer.nextToken();
			this.email = tokenizer.nextToken();
			this.overallKills = Integer.parseInt(tokenizer.nextToken());
			this.overallCashGained = Integer.parseInt(tokenizer.nextToken());
		} catch (NoSuchElementException e) {
			Debug.println
				("PlayerProfile constructor: Broken profile");
		}

		try {
			this.tankType = Integer.parseInt(tokenizer.nextToken());
			this.numberOfRounds = Integer.parseInt(tokenizer.nextToken());
			this.wind = Integer.parseInt(tokenizer.nextToken());
			this.gravity = Float.parseFloat(tokenizer.nextToken());
			this.initialCash = Long.parseLong(tokenizer.nextToken());
			this.hazards = (Boolean.valueOf(tokenizer.nextToken())).booleanValue();
			this.timeCreated = Long.parseLong(tokenizer.nextToken());
			this.timeAccessed = Long.parseLong(tokenizer.nextToken());
			this.sounds = (Boolean.valueOf(tokenizer.nextToken())).booleanValue();
		} catch (NoSuchElementException error) {
			// old style profile, we don't care
			// or do we?
		}

		if (name != null && name.equals(Protocol.guest)) {
			Debug.println("Setting user to guest: " + password);
			setGuest(true);
			this.name = this.password;
			this.password = Protocol.guest;
		}
	}

	public PlayerProfile(String name, String password) {
		this(name, password, "");
	}

	public PlayerProfile(String name, String password, String email) {
		this.name = name;
		this.password = password;

		// It seems that Alex and I didn't get this straight...
		// TODO: See if there's any bugs with this
		if (name != null && name.equals(Protocol.guest)) {
			Debug.println("Setting user to guest: " + password);
			setGuest(true);
			this.name = this.password;
			this.password = Protocol.guest;
		}

		setEmail(email);
	}

	public void setEmail(String email) {
		if (email == null || email.equals(""))
			this.email = "not specified";
		else
			this.email = email;
	}

	public void setPassword(String password) {
		if (password == null || password.equals(""))
			this.password = "*";
		else
			this.password = password;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public int getOverallKills() {
		return overallKills;
	}

	public long getOverallCashGained() {
		return overallCashGained;
	}

	public void setOverallKills(int overallKills) {
		this.overallKills = overallKills;
	}

	public void setOverallCashGained(long overallCashGained) {
		this.overallCashGained = overallCashGained;
	}

	public void increaseOverallKills(int amount) {
		this.overallKills += amount;
	}

	public void increaseOverallCashGained(long amount) {
		this.overallCashGained += amount;
	}

	public boolean isGuest() {
		return isGuest;
	}

	public void setGuest(boolean isGuest) {
		this.isGuest = isGuest;
	}

	public boolean isModified() {
		return modified;
	}

	public void setModified(boolean modified) {
		this.modified = modified;
	}

	public int getTankType() {
		return tankType;
	}

	public int getNumberOfRounds() {
		return numberOfRounds;
	}

	public int getWind() {
		return wind;
	}

	public float getGravity() {
		return gravity;
	}

	public long getInitialCash() {
		return initialCash;
	}

	public boolean getHazards() {
		return hazards;
	}

	public boolean getSounds() {
		return sounds;
	}

	public void setTankType(int tankNumber) {
		tankType = tankNumber;
	}

	public void setNumberOfRounds(int numberOfRounds) {
		this.numberOfRounds = numberOfRounds;
	}

	public void setWind(int wind) {
		this.wind = wind;
	}

	public void setGravity(float gravity) {
		this.gravity = gravity;
	}

	public void setInitialCash(long initialCash) {
		this.initialCash = initialCash;
	}

	public void setHazards(boolean hazards) {
		this.hazards = hazards;
	}

	public void setSounds(boolean soundsLoaded) {
		sounds = soundsLoaded;
	}

	public void setLamerMode(boolean lamerMode) {
		this.lamerMode = lamerMode;
	}

	public boolean getLamerMode() {
		return lamerMode;
	}

	public long getTimeAccessed() {
		return timeAccessed;
	}

	public long getTimeCreated() {
		return timeCreated;
	}

	public void setTimeAccessed(long timeAccessed) {
		this.timeAccessed = timeAccessed;
	}

	public void setTimeCreated(long timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String makeLoginString() {
		String result = isGuest() ?
			(password + Protocol.separator + name) :
			(name + Protocol.separator + password);
		return result;
	}

	public String toString() {
		String result = makeLoginString() +
			Protocol.separator + email + Protocol.separator + overallKills +
			Protocol.separator + overallCashGained +
			Protocol.separator + tankType +
			Protocol.separator + numberOfRounds +
			Protocol.separator + wind +
			Protocol.separator + gravity +
			Protocol.separator + initialCash +
			Protocol.separator + hazards +
			Protocol.separator + timeCreated +
			Protocol.separator + timeAccessed +
			Protocol.separator + sounds;

		Debug.println("PlayerProfile.toString(): " + result);
		return result;
	}

	/**
	 * Encrypts the profile's password.
	 * Takes first and last letter of the username as the salt.
	 */
	public PlayerProfile encrypt() {
		if (!isGuest) {
			String salt = "" + name.charAt(0) + name.charAt(name.length() - 1);
			password = Crypt.crypt(salt, password);
		}
		return this;
	}
}
