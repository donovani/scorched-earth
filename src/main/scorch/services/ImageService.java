package scorch.services;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.InputStream;

/**
 * Singleton service that acts as a wrapper over the loading images.
 * Usable for both applets & applications, as it's done via AWT.
 *
 * @author Ian Donovan
 */
public class ImageService {

	private static ImageService instance;

	private ImageService() {
	}

	public static ImageService instance() {
		if (instance == null)
			instance = new ImageService();
		return instance;
	}

	public Image load(String resourcePath) {
		try {
			InputStream resourceInputStream = getClass().getResourceAsStream(resourcePath);
			Image image = ImageIO.read(resourceInputStream);
			resourceInputStream.close();
			return image;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
