package scorch.services;

import javax.sound.sampled.*;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Singleton service that acts as a wrapper over the lower-level Clip API.
 * Usable for both applets & applications, as it's done via the core AudioSystem.
 *
 * @author Ian Donovan
 * @see <a href="https://docs.oracle.com/javase/tutorial/sound/accessing.html">Java AudioSystem Tutorial</a>
 */
public class AudioService {

	private static AudioService instance;

	private final Map<String, DataLine> audioSources;

	private AudioService() {
		audioSources = new HashMap<>();
	}

	public static AudioService instance() {
		if (instance == null)
			instance = new AudioService();
		return instance;
	}

	/**
	 * Function that loads an audio file into memory as a Clip.
	 * This loads the entire file into memory, and so is ideal for sound effects.
	 *
	 * @param resourcePath The path to the audio resource file, to be loaded via ClassLoader.
	 * @param unloadOnEnd  If the file should unload after use. False keeps the file in memory.
	 * @return A Clip object with the specified file loaded, or null if it fails.
	 */
	public Clip loadSFX(String resourcePath, boolean unloadOnEnd) {
		System.out.println("DEBUG: Loading sound " + resourcePath);
		try {
			// Create clip & have it open the stream
			AudioInputStream audioInputStream = openAudioInputStream(resourcePath);
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);

			// Add listener for closed clip cleanup
			clip.addLineListener(event -> {
				if (event.getType() == LineEvent.Type.CLOSE) {
					System.out.println("DEBUG: Clip has been closed: " + resourcePath);
					audioSources.remove(resourcePath, clip);
				}
			});

			// Add listener to stop + unload the clip when playback is complete
			if (unloadOnEnd) {
				clip.addLineListener(event -> {
					if (event.getType() == LineEvent.Type.STOP) {
						System.out.println("DEBUG: Clip has ended: " + resourcePath);
						clip.close();
					}
				});
			}
			audioSources.put(resourcePath, clip);
			return clip;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private AudioInputStream openAudioInputStream(String resourcePath) throws Exception {
		ClassLoader classloader = getClass().getClassLoader();
		InputStream inputStream = classloader.getResourceAsStream(resourcePath);
		if (inputStream == null)
			throw new Exception("Unable to load audio stream at " + resourcePath);
		BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
		return AudioSystem.getAudioInputStream(bufferedInputStream);
	}

	public void play(Clip clip) {
		this.play(clip, true);
	}

	public void play(Clip clip, boolean fromStart) {
		System.out.println("DEBUG: Playing sound: " + getAudioSourceName(clip));
		if (clip != null) {
			if (fromStart)
				clip.setFramePosition(0);
			clip.start();
		}
	}

	public void loop(Clip sfx) {
		this.loop(sfx, Clip.LOOP_CONTINUOUSLY);
	}

	public void loop(Clip sfx, int times) {
		if (sfx != null)
			sfx.loop(times);
	}

	public void pause(Clip clip) {
		System.out.println("DEBUG: Pausing sound: " + getAudioSourceName(clip));
		if (clip != null) {
			int pausePosition = clip.getFramePosition();
			clip.stop();
			clip.setFramePosition(pausePosition);
		}
	}

	public void stop(Clip clip) {
		System.out.println("DEBUG: Stopping sound: " + getAudioSourceName(clip));
		if (clip != null) {
			clip.stop();
			clip.flush();
			clip.setFramePosition(0);
		}
	}

	public void unload(DataLine audio) {
		System.out.println("DEBUG: Unloading sound: " + getAudioSourceName(audio));
		if (audio != null) {
			audio.flush();
			audio.close();
			audioSources.remove(keyOf(audio));
		}
	}

	public void unloadAll() {
		for (DataLine source : audioSources.values()) {
			if (source != null && source.isOpen()) {
				unload(source);
			}
		}
	}

	private String getAudioSourceName(DataLine audio) {
		String key = keyOf(audio);
		return key == null ? "<UNMANAGED AUDIO>" : key;
	}

	private String keyOf(DataLine audio) {
		for (String resourceName : audioSources.keySet()) {
			if (audioSources.get(resourceName) == audio)
				return resourceName;
		}
		return null;
	}
}
