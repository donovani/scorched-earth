package scorch;

/**
 * This class manages the estimation of the projectile trajectories, Winds, and Hazard conditions.
 * @author Ramya Ramesh
 * @author Alexander Rasin
 * @author Makhail Kruk
 */
public class Physics {

	// Default Earths gravity
	public static final float EARTH_GRAVITY = 9.8f;
	public static final int MAX_WIND = 10;


	static private float gravity = EARTH_GRAVITY;  // Gravity value
	static private int windVelocity; // Constant Wind Velocity


	private final double stepSize;

	// Angle and speed of projectile launch
	private final int angle;
	private final double speed;
	private final double xVelocity;
	private final double yVelocity;

	// Starting X and Y locations of projectile
	private final int startX;
	private final int startY;

	// Current X and Y positions
	private int x, y;

	// Current step in trajectory
	private int stepCount;


	public Physics(int x, int y, int angle, double power) {
		this.angle = angle; // Set angle of launch
		speed = power; // set initial speed
		startX = x; // set start X location
		startY = y; // set start Y location

		this.x = x;
		this.y = y;

		// Horizontal and vertical components of velocity
		double radians = (double) this.angle * Math.PI / 180.00;

		xVelocity = speed * Math.cos(radians);
		yVelocity = speed * Math.sin(radians);

		// Set stepSize
		/*
		stepSize = Math.abs(4/VelX0) * Math.sqrt(Math.abs(VelX0)/62.0);

		if (Angle == 90 || Speed == 0)
				stepSize = 0.08;
		else
				if (Angle > 80 && Angle < 100)
					stepSize = stepSize * (((Math.abs(90.0 - (double)Angle)-5.0)/2.0)+5) / 10.0;
		*/
		stepSize = 0.1;

		// Initialize step to 0
		stepCount = 0;
	}

	// Set gravity to arbitrary value
	public static void setGravity(float gravityValue) {
		gravity = gravityValue;
	}

	public int getStartX() {
		return startX;
	}

	public int getStartY() {
		return startY;
	}

	public int getAngle() {
		return angle;
	}

	// Set wind to one of three values: NO_WIND, CONSTANT_WIND or CHANGING_WIND
	public static void setWind(int NewWind) {
		windVelocity = NewWind;
	}

	public static int getWind() {
		return windVelocity;
	}


	// Calculate trajectory for specified Steps.
	// Assumes that the vectors xPositions[] and yPositions[] have already been allocated
	// Note stepSize determines jump size and may need to be set properly
	public void calculateTrajectory(int[] xPositions, int[] yPositions, int steps) {
		// ERROR Condition if steps is <= 0
		if (steps <= 0)
			return;

		double deltaX, deltaY;

		// Iterate through each step
		for (int ii = stepCount; ii < stepCount + steps; ++ii) {

			// Compute X and Y increments according to equations
			deltaX = (windVelocity + xVelocity) * ii * stepSize;
			deltaY = ii * stepSize * (yVelocity - 0.5 * gravity * ii * stepSize);

			xPositions[ii - stepCount] = (int) (startX + deltaX);
			yPositions[ii - stepCount] = (int) (startY + deltaY);
		}
		// Update the StepN position
		stepCount += steps;

		// Store current location
		x = xPositions[steps - 1];
		y = yPositions[steps - 1];

	}

	public double getHSpeed() {
		return xVelocity;
	}

	public String toString() {
		return "x: " + x + " y: " + y + " power: " + (speed * 8) + " angle: " + angle;
	}
}
