package scorch;

/**
 * The protocol that client and server use.
 * Contains all the instructions that the client and the server can use,
 * each one of them has expected parameters in comments.
 * @author Alexander Rasin
 */
public final class Protocol {
	public static char separator = (char) 0;

	//general commands
	public static String commandFailed = "COMMANDFAILED";//'reason'
	public static String debug = "DEBUG"; // 'String'
	public static String guest = "guest";   // guest user
	public static String setPlayerOptions = "PLOPTIONS";
	//'player id' 'tank type'
	public static String massKill = "MK";
	public static String boot = "BOOT"; // player id
	//kill all the tanks in the game. only executed by master
	public static String topTen = "TOP10";

	//Commands understood by server
	public static String login = "LOGIN"; // 'User Name' 'Password'
	public static String newPlayer = "NEW"; //'user' 'password' 'profile'
	public static String jvmInfo = "JVM"; //'jvm version'
	public static String removePlayer = "RMPLAYER"; //'username' 'password'
	public static String pong = "PONG"; // answer to ping
	public static String setGameOptions = "OPTIONS"; //'intial cash' 'wind' 'nature hazards'
	public static String addAiPlayer = "ADDAI"; //'ai player type'
	public static String setMaxPlayers = "SETMAXPL"; //'number'
	public static String playerDead = "PLDEAD"; //'player id'
	public static String endOfTurn = "EOT";
	public static String shout = "SHOUT"; //'message'
	public static String say = "SAY"; //'pl id' 'message'
	public static String update = "UPDATE"; //'power' 'angle'
	public static String useWeapon = "USEWEAPON"; //'weapon_id'
	public static String useItem = "USEITEM"; //'item_id' 'quantity'
	public static String quit = "QUIT";
	public static String changeProfile = "CHPROF"; //'new profile'
	public static String doneBuying = "DONEBUYING";
	public static String endOfRound = "EOR"; //'stats of round'
	public static String endOfGame = "EOG"; //'stats of game'

	//Commands understood by client
	public static String ping = "PING"; //ping
	public static String loggedIn = "LOGGEDIN"; //'name' 'Id'(int) 'profile'
	public static String aiLoggedIn = "AILOGGEDIN"; //'name' 'Id' 'profile'
	public static String loginFailed = "LOGINFAILED"; //'reason'

	// Reasons for failed login
	public static String wrongPassword = "PASSWD";    //wrong password
	public static String alreadyLoggedIn = "INGAME";  //user already playing
	public static String wrongUsername = "UNAME";     // wrong username
	public static String usernameTaken = "UNAMETAKE"; // username already taken

	// Leaving the game
	public static String disconnect = "DISCONNECT"; //'message'
	public static String playerLeft = "LEFT"; //'id'
	public static String gameOptions = "OPTIONS";
	//'initial cash' 'wind' 'nature hazards' 'seed'
	public static String playerUsedWeapon = "USEWEAPON"; //'weapon_id'
	public static String playerUsedItem = "USEDITEM"; //'item_id' 'quantity'
	public static String makeTurn = "MAKETURN"; //'player_id'
	public static String requestLog = "RLOG";
	public static String clientLog = "CLOG"; // 'log'
}
