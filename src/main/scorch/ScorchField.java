package scorch;

import scorch.backgrounds.Background;
import scorch.backgrounds.GradientBackground;
import scorch.backgrounds.PlainBackground;
import scorch.backgrounds.StarsBackground;
import scorch.utility.Debug;
import scorch.weapons.*;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Random;
import java.util.Vector;

/**
 * The class that handles all the game stuff.
 * This is the place where animation threads live, all the explosions, terrain generation goes in here.
 * BTW terrain generation sucks, but on the other hand it looks kind of better than fractal one.
 * Chat support is also here (for some reason yet unknown)
 * Many, many things in this file must be fixed or redone from scratch
 *
 * @author Mikhail Kruk
 */
public final class ScorchField extends Canvas implements Runnable, MouseMotionListener, FocusListener {
	private Image backBuffer;
	private final Image extraBackBuffer;
	private Graphics backBufferGraphics;
	private final int width;
	private final int height;
	private final Bitmap bitmap;
	private final ScorchGame scorchGame;
	private Color groundColor;
	private Thread thread;
	private boolean sendEOT = false;
	private final Vector currentAnimations; // Explodable

	private final ScorchChat chat;
	private final Tooltip tooltip;

	private ScorchPlayer player; // the player which fires
	private long earnedCash = 0; // cash and kills earned in this *round*
	private int kills = 0;

	private final Random random;

	// TERRAIN GENERATION MUST BE REIMPLEMENTED
	// We never got a chance to do it right.
	// The code that generates terrain was actually written to test the Bitmap engine
	// and somehow no one bothered to fix this...
	public ScorchField(int width, int height, Random random, ScorchGame scorchGame) {
		super();

		this.width = width;
		this.height = height;
		this.scorchGame = scorchGame;
		this.random = random;

		Background background = randomBackground();

		bitmap = new Bitmap(width, height, background, random);
		bitmap.setSandColor(groundColor.getRGB());

		extraBackBuffer = createImage(bitmap.getImageProducer());

		Debug.startTimer();

		int gg = Math.round(3f / 8f * (float) height);
		bitmap.setColor(null);
		bitmap.fillRect(0, height - gg, width, gg);
		bitmap.setColor(groundColor);
		bitmap.fillRect(0, 0, width, height - gg);

		Debug.stopTimer("initial fill");
		Debug.startTimer();

		bitmap.setColor(null);
		for (int i = 0; i < 20; i++) {
			bitmap.fillEllipse(
				Math.abs(random.nextInt()) % width,
				Math.abs(random.nextInt()) % height,
				10 + Math.abs(random.nextInt()) % 90,
				10 + Math.abs(random.nextInt()) % 60
			);
		}
		bitmap.setDensity(1f);
		bitmap.setColor(groundColor);
		bitmap.fillEllipse(width / 2, height, width, height / 8);
		bitmap.setDensity(1f);

		Debug.stopTimer("ellipse fill");
		Debug.startTimer();

		drop(0, width);
		bitmap.newPixels(0, 0, width, height);
		Debug.stopTimer("drop");

		placeTanks();

		currentAnimations = new Vector();

		chat = new ScorchChat(this, bitmap.getBackground());
		tooltip = new Tooltip(this);

		addMouseMotionListener(this);
		addFocusListener(this);
	}

	private void hideTanks() {
		for (int i = 0; i < scorchGame.getPlayersNum(); i++) {
			ScorchPlayer sp = scorchGame.getPlayer(i);

			if (!sp.isAlive()) continue;
			sp.hideFrame(false, false);
		}
	}

	private void dropTanks() {
		Debug.println("Starting to drop the tanks:", 10);
		for (int i = 0; i < scorchGame.getPlayersNum(); i++) {
			ScorchPlayer player = scorchGame.getPlayer(i);

			if (!player.isAlive()) continue;

			player.setFalling(true);
			addAnimation(player);
		}
	}

	private void showTanks() {
		for (int i = 0; i < scorchGame.getPlayersNum(); i++) {
			ScorchPlayer player = scorchGame.getPlayer(i);

			if (!player.isAlive()) continue;

			player.drawTank(true);
		}
	}

	private void drawShields() {
		for (int i = 0; i < scorchGame.getPlayersNum(); i++) {
			ScorchPlayer player = scorchGame.getPlayer(i);
			if (player.isAlive()) {
				player.drawShield(backBufferGraphics);

				if (Debug.dev || Debug.desyncTest) {
					backBufferGraphics.drawString(
						"" + (float) player.getPowerLimit() / (float) ScorchPlayer.MAX_POWER * 100,
						player.getX(),
						player.getY() + 2 * player.getHeight()
					);
				}
			}
		}
	}

	/**
	 * Randomly disperse players over the terrain.
	 * Also make each player object aware of the ScorchField on which they live
	 * so that they can interact with the environment.
 	 */
	private void placeTanks() {
		int[][] tank;
		int tankType;
		int numPlayers = scorchGame.getPlayersNum();
		ScorchPlayer player;
		Vector positions = new Vector(numPlayers);
		int ct, t;

		for (int i = 0; i < numPlayers; i++)
			positions.addElement(Integer.valueOf(i));

		for (int i = 0; i < numPlayers; i++) {
			t = Math.abs(random.nextInt()) % positions.size();
			ct = ((Integer) positions.elementAt(t)).intValue();
			positions.removeElementAt(t);

			player = scorchGame.getPlayer(ct);
			player.onFieldInit(this, bitmap, ct);
			tankType = player.getTankType();
			tank = Tanks.TANKS[tankType];

			bitmap.setColor(null);
			for (int j = 1; j < height; j++) {
				int count = 0, k;
				for (k = 0; k < Tanks.getTankWidth(tankType) &&
					count < Tanks.getTankWidth(tankType); k++)
					if (!bitmap.isBackground(k + width / (numPlayers + 1) * (i + 1), j)) {
						count++;
						bitmap.setPixel(k + width / (numPlayers + 1) * (i + 1), j - 1);
					}

				if (count >= Tanks.getTankWidth(tankType)) {
					player.setPosition(
						width / (numPlayers + 1) * (i + 1),
						j - Tanks.getTankHeight(tankType)
					);
					player.drawNextFrame(true);

					j = height;
					k = height;
				}
			}
		}
	}

	private void drop(int startX, int endX) {
		new Dropper(bitmap, startX, endX);
	}

	public void update(Graphics graphics) {
		paint(graphics);
	}

	public void paint(Graphics graphics) {
		if (backBuffer == null) {
			backBuffer = createImage(width, height);
			backBufferGraphics = backBuffer.getGraphics();
		}

		backBufferGraphics.drawImage(extraBackBuffer, 0, 0, this);
		chat.paint(backBufferGraphics, width);
		drawShields();

		tooltip.paint(backBufferGraphics);

		graphics.drawImage(backBuffer, 0, 0, this);
	}

	public synchronized void run() {
		int i = 0;

		if (thread == null) return;

		do {
			runCurrentAnimation();
			// now explode everyone who wants to explode
			for (i = 0; i < scorchGame.getPlayersNum(); i++) {
				ScorchPlayer player = scorchGame.getPlayer(i);
				if (player.isDying()) {
					addAnimation(player);
					break;
				}
			}
		}
		while (i < scorchGame.getPlayersNum()); // while someone explodes

		if (sendEOT) {
			if (player != null)
				player.updateKills(earnedCash, kills);
			player = null; // just in case
			scorchGame.sendEOT("run()");
		}
		sendEOT = false;
		thread = null;
	}

	// Runs an animation NOW.
	// This specifies whether a new Thread should be spawned or not.
	public synchronized void runAnimationNow(Explodable animation) {
		sendEOT = false; // you never need to send EOT from such calls, I think

		addAnimation(animation);
		thread = Thread.currentThread();
		run();
	}

	private synchronized void addAnimation(Explodable animation) {
		currentAnimations.addElement(animation);
	}

	private synchronized void runCurrentAnimation() {
		int i = 0;
		Explodable current;

		if (scorchGame.galslaMode) {
			while (thread != null) {
				for (i = 0; i < scorchGame.getPlayersNum(); i++)
					scorchGame.getPlayer(i).drawNextFrame(true);
				try {
					Thread.sleep(60);
				} catch (InterruptedException e) {
				}
			}
			return;
		}

		while (currentAnimations.size() > 0 && thread != null &&
			!scorchGame.galslaMode) {
			i = 0;
			while (i < currentAnimations.size()) {
				current = (Explodable) (currentAnimations.elementAt(i));

				if (current.drawNextFrame(true)) {
					bitmap.newPixels();
					i++;
				} else {
					bitmap.newPixels();
					currentAnimations.removeElementAt(i);
					processAnimation(current);
				}
			}
			try {
				Thread.sleep(40); // this shouldn't be constant?
			} catch (InterruptedException e) {
			}
		}
	}

	// this method take care of stuff left after the animations ends
	private synchronized void processAnimation(Explodable explosion) {
		if ((explosion instanceof GenericMissile) ||
			(explosion instanceof ScorchPlayer && !((ScorchPlayer) explosion).isAlive())) {
			fireComplete(explosion);
			return;
		}
		if (explosion instanceof ScorchPlayer && ((ScorchPlayer) explosion).isAlive()) {
			killTanks(null, null, false); // kills from falling
		}
	}

	private synchronized void fireComplete(Explodable explosion) {
		ExplosionInfo info = explosion.getExplosionInfo();

		if (info != null) {
			showTanks();

			if (info.explosionArea != null) {
				drop(info.explosionArea.x,
					info.explosionArea.x + info.explosionArea.width);
				showTanks();
				bitmap.newPixels(info.explosionArea.x, 0,
					info.explosionArea.width + 1, height);
			}
			//if( info.center != null ) TODO? maybe it's ok?
			killTanks(explosion, player, false);
		}
		// always drop tanks. I think this is needed only if someone used fuel
		// but there seems to be no easy way to tell, so drop.
		dropTanks();
	}

	public ScorchPlayer getTankAt(int x, int y) {
		ScorchPlayer player;
		for (int i = 0; i < scorchGame.getPlayersNum(); i++) {
			player = scorchGame.getPlayer(i);
			if (!player.isAlive()) continue;
			if (x > player.getX() && x < player.getX() + player.getWidth() &&
				y > player.getY() && y < player.getY() + player.getHeight())
				return player;
		}
		return null;
	}

	// Checks if players are in the range of any round explosion. Works in two modes:
	// * AI Mode: used by AI to count number of victims of a possible shot
	// * Normal Mode: just kill all the victims
	public int killTanks(Explodable explosion, ScorchPlayer caller, boolean aiMode) {
		ScorchPlayer player;
		int counter = 0, currentDamage;
		boolean alive = true;

		//Debug.printThreads();

		for (int i = 0; i < scorchGame.getPlayersNum(); i++) {
			player = scorchGame.getPlayer(i);

			// skip players which are dead or are about to explode
			if (!player.isAlive() || player.isDying())
				continue;

			if (explosion != null)
				currentDamage = explosion.calculateDamage(player);
			else
				currentDamage = 0; // to drop tanks

			if (!aiMode) {
				alive = player.decreasePowerLimit(currentDamage);

				if (!alive) {
					if (player != caller) {
						// bonus if >1 kill with one shot
						earnedCash *= 1.3;

						earnedCash += player.getBounty();
						kills++;
					} else // killed himself
					{
						earnedCash -= 10000;
					}

					player.setExplosion(randomExplosion(player));
				}

				// give money for any damage done to other players
				if (player != caller)
					earnedCash += (10L * currentDamage);
			}

			if (aiMode && currentDamage > 0) {
				if (player == caller)
					counter -= ScorchPlayer.MAX_PLAYERS * ScorchPlayer.MAX_POWER;
				else
					counter += explosion.calculateDamage(player);
			}
		}
		return counter;
	}

	public void stop() {
		if (thread != null)
			thread = null;
	}

	public void start() {
		if (thread != null)
			thread.start();
	}

	public synchronized void playerLeft(ScorchPlayer player) {
		if (thread != null)
			System.err.println("ScorchField.playerLeft(): internal error 0. " +
				"please report to meshko@scorch2000.com");

		chat.addSystemMessage("Player " + player.getName() + " left the game");

		if (player.isAlive()) {
			player.setExplosion
				(new SimpleExplosion(bitmap, SimpleExplosion.MISSILE));
			//player.setFalling(true);

			runAnimationNow(player);

			//new Thread(this, "leftpl-thread");
			//thread.start();

			// we must wait here..
		/*try
		    {
			thread.join();
		    }
		catch(InterruptedException e)
		{}*/
		}
	}

	public synchronized void massKill() {
		if (thread != null)
			System.err.println("ScorchField.massKill(): internal error 0. please report to meshko@scorch2000.com");

		newSystemMessage("MASSKILL by master");

		for (int i = 0; i < scorchGame.getPlayersNum(); i++) {
			ScorchPlayer player = scorchGame.getPlayer(i);

			if (!player.isAlive()) continue;

			player.setExplosion
				(new SimpleExplosion(bitmap, SimpleExplosion.MISSILE));
			//player.setFalling(true);
			addAnimation(player);
		}

		sendEOT = true;
		thread = Thread.currentThread(); //new Thread(this, "masskill-thread");
		run(); //thread.start();
	}

	// TODO: Move this in the Background class?
	private Background randomBackground() {
		Color randomColor1, randomColor2;
		Color[] colors = {
			Color.black,
			new Color(0, 150, 0),
			new Color(254, 0, 0),
			new Color(0, 0, 254),
			new Color(254, 254, 254),
			new Color(0, 254, 254),
			new Color(254, 254, 0),
			new Color(34, 23, 65)
		};
		Background background;

		randomColor1 = colors[Math.abs(random.nextInt() % colors.length)];
		randomColor1 = new Color(randomColor1.getRed(), randomColor1.getGreen(),
			Math.min(255, randomColor1.getBlue() + 100));

		groundColor = colors[Math.abs(random.nextInt() % colors.length)];
		groundColor = new Color(
			Math.max(20, groundColor.getRed() - 150),
			Math.max(20, groundColor.getGreen() - 150),
			Math.max(20, groundColor.getBlue() - 150)
		);

		switch (Math.abs(random.nextInt() % 3)) {
			case 0:
				background = new StarsBackground(width, height, random);
				groundColor = new Color(
					Math.min(255, groundColor.getRed() + 30),
					Math.min(255, groundColor.getGreen() + 30),
					Math.min(255, groundColor.getBlue() + 30)
				);
				break;
			case 1:
				randomColor2 = colors[Math.abs(random.nextInt() % colors.length)];
				background = new GradientBackground(width, height, randomColor1, randomColor2, 127);
				break;
			case 2:
				randomColor1 = new Color(
					Math.max(20, randomColor1.getRed() - 50),
					Math.max(20, randomColor1.getGreen() - 50),
					Math.min(255, randomColor1.getBlue() + 30)
				);
				background = new PlainBackground(width, height, randomColor1);
				break;
			default:
				background = null;
		}

		return background;
	}

	// this is where each turn begins
	public synchronized void fire(ScorchPlayer player, int weapon) {
		Explosion explosion;
		GenericMissile missile;
		Physics physics;

		explosion = player.getWeaponExplosion(weapon);

		earnedCash = 0;
		kills = 0;
		this.player = player;

		// Hide shield so that missile can go through player.drawShield(false, false);

		int angle = this.player.getAngle();

		physics = new Physics(this.player.getTurretX(2.0),
			bitmap.getHeight() - this.player.getTurretY(2.0),
			angle, this.player.getPower() / 8.0);

		Debug.log(this.player + " shoots:");
		Debug.log("" + physics);

		if (explosion instanceof MIRVExplosion)
			missile = new MIRVMissile(bitmap, physics, explosion);
		else
			missile = new RoundMissile(bitmap, physics, explosion);

		// Redraws shield. It's a bit of a hack:
		// We hope that missile precalculated the trajectory beyond
		// the shield - it's an almost reasonable assumption.
		// player.drawShield(true, false);

		if (player.checkTracer())
			missile.setTracerColor(new Color(player.getColor()));

		addAnimation(missile);

		sendEOT = true;
		thread = new Thread(this, "animation-thread");
		thread.start();
	}

	// Adds a new chat message to the chat
	public void newChatMessage(String message) {
		chat.addMessage(message);
		if (message.indexOf("Galsla") != -1) {
			chat.addSystemMessage("PRIVET GALKA!!!");
			scorchGame.galslaMode = true;
			thread = new Thread(this, "galsla-thread");
			thread.start();
		}
	}

	// System message displayed through the chat
	public void newSystemMessage(String msg) {
		chat.addSystemMessage(msg);
	}

	public void focusGained(FocusEvent e) {
		transferFocus();
	}

	public void focusLost(FocusEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
		ScorchPlayer player = getTankAt(e.getX(), e.getY());
		if (player != null)
			tooltip.setText(player.getToolTip(), e.getX(), e.getY());
		else
			tooltip.setText(null, 0, 0);
	}

	// TODO: Move this to the Explosion class?
	private Explosion randomExplosion(ScorchPlayer player) {
		Explosion explosion = null;
		int explosionNumber = 7;
		int explosionType = Math.abs(random.nextInt()) % explosionNumber;
		int playerWidth = player.getWidth();
		int playerHeight = player.getHeight();

		switch (explosionType) {
			case 0:
				explosion = new FireExplosion(bitmap, (int) (playerWidth * 1.5), playerHeight * 4);
				break;
			case 1:
				explosion = new SimpleExplosion(bitmap, SimpleExplosion.MISSILE);
				break;
			case 2:
				explosion = new SimpleExplosion(bitmap, SimpleExplosion.BABY_NUKE);
				break;
			case 3:
				explosion = new SimpleExplosion(bitmap, SimpleExplosion.NUKE);
				break;
			case 4:
				explosion = new SandExplosion(bitmap);
				break;
			case 5:
				explosion = new LaserExplosion(bitmap, playerWidth);
				break;
			case 6:
				explosion = new FunkyExplosion(bitmap, random, 6);
				break;
			default:
				System.err.println("ScorchField.getRandomExplosion(): error");
		}

		return explosion;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	/*
	private class starter implements Runnable
  {
		private Explodable animation;

		public starter(Explodable animation)
		{
				this.animation = animation;
				ScorchField.this.thread = new Thread(this, "starter thread");
				ScorchField.this.thread.start();
		}

		public void run()
		{
				ScorchField.this.addAnimation(animation);
				ScorchField.this.run();
		}
	}
	*/
}
