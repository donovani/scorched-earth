package scorch;

import java.awt.*;

/**
 * currently not used...
 * @author Mikhail Kruk
 */
public class GradientPanel extends Panel implements Runnable {
	private int steps = 21, current = 0, stripWidth, stripHeight;
	private final int[] colors;
	private Image backBuffer;
	private Graphics backBufferGraphics;
	private boolean animate = true;

	public GradientPanel(int width, int height, Color color1, Color color2) {
		super();

		setSize(width, height);

		this.steps = steps;

		int red1, red2, green1, green2, blue1, blue2;
		int i;

		red1 = color1.getRed();
		green1 = color1.getGreen();
		blue1 = color1.getBlue();

		red2 = color2.getRed();
		green2 = color2.getGreen();
		blue2 = color2.getBlue();

		colors = new int[steps * 2];
		for (i = 0; i < steps; i++)
			colors[i] = (255 << 24) | ((red1 + (red2 - red1) / steps * i) << 16) |
				((green1 + (green2 - green1) / steps * i) << 8) |
				(blue1 + (blue2 - blue1) / steps * i);
		for (int j = i; j < steps * 2; j++)
			colors[j] = colors[2 * i - j];
	}

	public void paint(Graphics graphics) {
		update(graphics);
	}

	public void update(Graphics graphics) {
		if (backBuffer == null) {
			Dimension size = getSize();
			backBuffer = createImage(size.width, size.height);
			backBufferGraphics = backBuffer.getGraphics();

			stripWidth = Math.round(size.width / (steps * 2));
			stripHeight = size.height;

			Thread thread = new Thread(this, "gradient-thread");
			thread.start();
		}
		graphics.drawImage(backBuffer, 0, 0, this);
	}

	public void draw() {
		for (int i = 0; i < steps * 2; i++) {
			backBufferGraphics.setColor(new Color(colors[(current + i) % (2 * steps)]));
			backBufferGraphics.fillRect(i * stripWidth, 0, (i + 1) * stripWidth, stripHeight);
		}

		current++;
		if (current == steps * 2) current = 0;
	}

	public void run() {
		while (animate) {
			try {
				Thread.sleep(100);
			}
			catch (InterruptedException error) {}
			draw();
			repaint();
		}
	}

	public void stop() {
		animate = false;
	}
}
