package scorch;

import scorch.utility.Debug;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Describes all the network interaction with the server;
 * calls the callback methods from the ScorchGame.
 *
 * @author Mikhail Kruk
 */
public class Network implements Runnable {
	private Socket socket;
	private Thread listener;
	private BufferedReader inputStream;
	private final PrintWriter outputStream;
	private final int pcount = 0;
	private final ScorchGame owner;

	private boolean listen = true;

	private int port;

	public Network(String host, int port, ScorchGame owner) throws IOException {
		this.owner = owner;

		socket = new Socket(host, port);
		socket.setSoTimeout(1000);
		inputStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		outputStream = new PrintWriter(socket.getOutputStream(), true);
		Debug.println("Connected to " + host + " on " + port);

		listen = true;
		listener = new Thread(this, "network-thread");
		listener.start();
	}

	private void disconnect() {
		sendMessage(Protocol.quit);
		if (socket == null)
			return;

		Debug.println("Setting listen to false...");
		listen = false;

		try {
			socket.close();
			socket = null;

			if (inputStream != null) {
				inputStream.close();
				inputStream = null;
			}
		} catch (Throwable e) {
			System.err.println("Disconnect: " + e);
			e.printStackTrace();
		}

		listener = null;
	}

	synchronized public void sendMessage(String message) {
		if (outputStream == null) {
			Debug.println("sendMessage: Failed to write to the network");
			return;
		}
		outputStream.print(message + "\r\n");
		outputStream.flush();
		Debug.consolePrint("server <---| " + message);
	}

	private String getAnswer() {
		String message = null;
		try {
//			while(listen && !is.ready() )
//			{
//				try
//				{
//					Thread.sleep(200);
//				}
//				catch (Exception e)
//				{
//					System.err.println(e);
//				}
//			}
			Debug.println("Going to block, listen is " + listen);

			while (listen && message == null) {
				try {
					message = inputStream.readLine();
				} catch (SocketException error) {
					if (listen)
						owner.showDisconnect("Connection to the server lost");
					disconnect();
					return message;
				} catch (InterruptedIOException error) {
					message = null;
				}
			}
			Debug.println("Read done, listen is " + listen);
			Debug.consolePrint("server |---> " + message);
			return message;
		} catch (IOException error) // now I don't think this can happen,
		{
			System.err.println("getAnswer: " + error);

			if (listen)
				owner.showDisconnect("Connection to the server lost");
			disconnect();
			return message;
		}
	}

	public void run() {
		String commandString;
		for (commandString = getAnswer(); listen && commandString != null; commandString = getAnswer()) {
			int index = commandString.indexOf(Protocol.separator);
			String command = commandString.substring(0, (index == -1 ? commandString.length() : index));
			String rest = (index == -1 ? "" : commandString.substring(index + 1));
			StringTokenizer tokenizer = new StringTokenizer(rest, Protocol.separator + "");

			if (command.equals(Protocol.ping)) {
				sendMessage(Protocol.pong);
				continue;
			}

			if (owner.galslaMode)
				continue;

			/*try
				{
				Thread.currentThread().sleep(1000);
				}
			catch(InterruptedException e)
			{}*/

			// here we've made a stupid protocol design decision
			// the player id comes after profile, so profile structure
			// is fixed. Now to add fields to the profile, we have
			// to make parser a bit obscure
			if (command.equals(Protocol.loggedIn) ||
				command.equals(Protocol.aiLoggedIn)) {
				PlayerProfile profile;

				profile = new PlayerProfile(rest);

				// find the last token and hope it is player ID
				while (tokenizer.countTokens() > 1)
					tokenizer.nextToken();

				owner.loggedIn(
					Integer.parseInt(tokenizer.nextToken()),
					profile,
					command.equals(Protocol.aiLoggedIn)
				);
				continue;
			}
			if (command.equals(Protocol.loginFailed)) {
				owner.errorLogin(tokenizer.nextToken(), tokenizer.nextToken());
				continue;
			}
			if (command.equals(Protocol.playerLeft)) {
				owner.playerLeft(Integer.parseInt(tokenizer.nextToken()));
				continue;
			}
			if (command.equals(Protocol.gameOptions)) {
				owner.receiveGameSettings(
					new GameSettings(
						Float.parseFloat(tokenizer.nextToken()),
						Integer.parseInt(tokenizer.nextToken()),
						Boolean.valueOf(tokenizer.nextToken()).booleanValue(),
						Integer.parseInt(tokenizer.nextToken()),
						Long.parseLong(tokenizer.nextToken()),
						Boolean.valueOf(tokenizer.nextToken()).booleanValue()
					),
					Long.parseLong(tokenizer.nextToken())
				);
				continue;
			}
			if (command.equals(Protocol.shout) ||
				command.equals(Protocol.say)) {
				owner.newChatMessage(rest);
				continue;
			}
			if (command.equals(Protocol.setPlayerOptions)) {
				owner.receivePlayerOptions(
					Integer.valueOf(tokenizer.nextToken()).intValue(),
					Integer.valueOf(tokenizer.nextToken()).intValue()
				);
				continue;
			}
			if (command.equals(Protocol.disconnect)) {
				owner.showDisconnect(rest);
				continue;
			}
			if (command.equals(Protocol.update)) {
				owner.receiveUpdate(
					Integer.parseInt(tokenizer.nextToken()),
					Integer.parseInt(tokenizer.nextToken()),
					Integer.parseInt(tokenizer.nextToken())
				);
				continue;
			}
			if (command.equals(Protocol.useWeapon)) {
				owner.fire(Integer.parseInt(tokenizer.nextToken()));
				continue;
			}
			if (command.equals(Protocol.useItem)) {
				owner.receiveUseItem(
					Integer.parseInt(tokenizer.nextToken()),
					Integer.parseInt(tokenizer.nextToken())
				);
				continue;
			}
			if (command.equals(Protocol.makeTurn)) {
				owner.makeTurn(Integer.parseInt(tokenizer.nextToken()));
				continue;
			}
			if (command.equals(Protocol.endOfRound)) {
				owner.receiveEOR();
				continue;
			}
			if (command.equals(Protocol.endOfGame)) {
				owner.receiveEOG();
				continue;
			}
			if (command.equals(Protocol.massKill)) {
				owner.receiveMassKill();
				continue;
			}
			if (command.equals(Protocol.topTen)) {
				Vector profiles = new Vector();
				while (tokenizer.hasMoreTokens())
					profiles.addElement(new PlayerProfile(tokenizer));
				owner.showTopTen(profiles);
				continue;
			}
			if (command.equals(Protocol.requestLog)) {
				sendMessage( Protocol.clientLog + Protocol.separator + Debug.getLog() );
				continue;
			}
			System.err.println("Network: Unhandled command: " + command);
		}

		if (listen)
			owner.showDisconnect("Connection to the server lost");
		disconnect();

		if (inputStream != null) {
			try {
				inputStream.close();
				inputStream = null;
			} catch (IOException e) {
				System.err.println("Exception while trying to close socket: " + e);
			}
		}
		Debug.println("Disconnected from server; last message: " + commandString);
		Debug.println("listen was: " + listen);
	}

	/*
	public void sendLogin(String name, String password)
	{
		sendMessage(Protocol.login + Protocol.separator + name +
			Protocol.separator + password);
	}
	*/

	public void sendLogin(PlayerProfile profile, String options) {
		profile.encrypt();
		sendMessage(Protocol.login + Protocol.separator +
			profile.makeLoginString() +
			Protocol.separator + options);
		sendMessage(Protocol.jvmInfo + Protocol.separator + ScorchGame.JVM);
	}

	/*
	public void sendNewUser(String name, String password, String email)
	{
		sendMessage(
			Protocol.newplayer + Protocol.separator + name + Protocol.separator + password +Protocol.separator + email
		);
	}
	*/

	public void sendNewUser(PlayerProfile profile, String options) {
		profile.encrypt();
		sendMessage(Protocol.newPlayer + Protocol.separator + profile + Protocol.separator + options);
		sendMessage(Protocol.jvmInfo + Protocol.separator + ScorchGame.JVM);
	}

	public void sendMaxPlayers(int maxPlayers) {
		sendMessage(Protocol.setMaxPlayers + Protocol.separator + maxPlayers);
	}

	public void sendAIPlayer(String player) {
		sendMessage(Protocol.addAiPlayer + Protocol.separator + player);
	}

	public void sendPlayerSettings(int id, PlayerSettings playerSettings) {
		sendMessage(Protocol.setPlayerOptions + Protocol.separator + id + Protocol.separator + playerSettings);
	}

	public void sendGameSettings(GameSettings gSettings) {
		sendMessage(Protocol.setGameOptions + Protocol.separator + gSettings);
	}

	public void sendShout(String msg, String player, int idx) {
		ScorchPlayer scorchPlayer;

		if (idx < 0)
			sendMessage(Protocol.shout + Protocol.separator + "<" + player + ">" + " " + msg);
		else {
			scorchPlayer = owner.getPlayer(idx);
			int pid = scorchPlayer.getID();
			sendMessage(
				Protocol.say + Protocol.separator + pid + Protocol.separator +
					"[" + player + " => " + scorchPlayer.getName() + "]" + " " + msg
			);
		}
	}

	public void sendUpdate(int id, int power, int angle) {
		//Debug.printStack();
		sendMessage(
			Protocol.update + Protocol.separator + id +
				Protocol.separator + power +
				Protocol.separator + angle
		);
	}

	public void sendEOT(String desyncTestString) {
		sendMessage(Protocol.endOfTurn + Protocol.separator + desyncTestString);
	}

	public void sendUseWeapon(int weaponId) {
		sendMessage(Protocol.useWeapon + Protocol.separator + weaponId);
	}

	public void sendUseItem(int itemId, int param) {
		sendMessage(
			Protocol.useItem + Protocol.separator + itemId + Protocol.separator + param
		);
	}

	public void sendTankDead(int id) {
		sendMessage(Protocol.playerDead + Protocol.separator + id);
	}

	public void sendMassKill() {
		sendMessage(Protocol.massKill);
	}

	public void sendUpdateUser(PlayerProfile profile, boolean encrypt) {
		if (encrypt) profile.encrypt();
		sendMessage(Protocol.changeProfile + Protocol.separator + profile);
	}

	public void sendTopTen() {
		sendMessage(Protocol.topTen);
	}

	public void quit() {
		Debug.clearLog();
		disconnect();
	}
}
