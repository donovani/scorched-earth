package scorch.windows;

import java.awt.*;

class ScorchScrollBar extends Container {
	private final ScorchScrollPanel owner;
	private final ScorchScrollButton upButton;
	private final ScorchScrollButton downButton;

	public ScorchScrollBar(ScorchScrollPanel owner) {
		super();
		setLayout(new BorderLayout(0, 0));
		this.owner = owner;

		upButton = new ScorchScrollButton(ScorchScrollButton.SCROLL_UP, owner);
		downButton = new ScorchScrollButton(ScorchScrollButton.SCROLL_DOWN, owner);
		add(upButton, BorderLayout.NORTH);
		add(downButton, BorderLayout.SOUTH);
		add(new ScorchSeparator(), BorderLayout.WEST);
		add(new ScorchSeparator(), BorderLayout.EAST);

	}
}
