package scorch.windows;

import java.awt.*;

/**
 * The class that represents the main part of Scorch window GUI
 * @author Mikhail Kruk
 */
public class ScorchWindow extends ScorchPanel {
	private int dragX, dragY;
	private boolean drag;
	private Image dragBuffer;

	protected String name; // windows name
	protected Container owner; // each window has an owner

	private Component border;

	protected boolean doubleBorder = true; // border type

	// all the components are inserted into the mainPanel, not in the ScorchWindow directly
	public Container mainPanel;

	public ScorchWindow(
		int x,
		int y,
		int width,
		int height,
		String name,
		Color backgroundColor,
		Color textColor,
		Container owner
	) {
		this(x, y, width, height, name, owner);
		this.backgroundColor = backgroundColor;
		this.textColor = textColor;
	}

	public ScorchWindow(int x, int y, int w, int h, String name, Container owner) {
		super(x, y, w, h);
		this.name = name;
		this.owner = owner;

		setBackground(backgroundColor);

		this.mainPanel = new Panel();

		if (name != null) {
			mainPanel.setLocation(windowBorder * 3, 5 * windowBorder + fontHeight);
			mainPanel.setSize(
				width - 6 * windowBorder,
				height - 8 * windowBorder - fontHeight
			);
		} else {
			mainPanel.setLocation(windowBorder * 3, 3 * windowBorder);
			mainPanel.setSize(
				width - 6 * windowBorder,
				height - 6 * windowBorder
			);
		}

		super.setLayout(null);
		super.add(mainPanel);

		/*
		owner.addMouseMotionListener(this);
	  owner.addMouseListener(this);
	  */
		setVisible(false);
	}

	public void setDoubleBorder(boolean useDoubleBorder) {
		doubleBorder = useDoubleBorder;
	}

	public void display() {
		owner.add(this, 0);
		setVisible(false);
		place();
		validate();
		setVisible(true);
		requestFocus();
	}

	/*
	public void update(Graphics g)
	{
		paint(g);
	}

	public void paint(Graphics g)
	{
		if( drag )
		{
			if( dragBuffer == null )
			{
				dragBuffer = createImage(width, height);
				//sPaint(dragBuffer.getGraphics());
				dragBuffer.getGraphics().setColor(Color.white);
				dragBuffer.getGraphics().drawRect(0,0,100,100);
			}
			//g.drawImage(dragBuffer, 0, 0, this);
			g.setColor(Color.white);
			g.drawRect(0,0,100,100);
		}
		else
	 		sPaint(g);
	}
	*/

	public void paint(Graphics graphics) {
		if (!doubleBorder) {
			super.paint(graphics);
			return;
		}

		if (fontMetrics == null) {
			fontMetrics = graphics.getFontMetrics();
			fontHeight = fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();
		}

		graphics.setColor(backgroundColor);
		graphics.fill3DRect(0, 0, width, height, true);

		if (name != null) {
			graphics.setColor(backgroundColor);
			graphics.fill3DRect(windowBorder, windowBorder, width - 2 * windowBorder,
				windowBorder + fontHeight, false);
			graphics.fill3DRect(windowBorder, 3 * windowBorder + fontHeight,
				width - 2 * windowBorder,
				height - 4 * windowBorder - fontHeight, false);
			graphics.fill3DRect(windowBorder * 2, 4 * windowBorder + fontHeight,
				width - 4 * windowBorder,
				height - 6 * windowBorder - fontHeight, true);

			graphics.drawString(name, (width - fontMetrics.stringWidth(name)) / 2,
				windowBorder + fontMetrics.getMaxAscent());
			mainPanel.setLocation(windowBorder * 3, 5 * windowBorder + fontHeight);
		} else {
			graphics.fill3DRect(windowBorder, windowBorder,
				width - 2 * windowBorder,
				height - 2 * windowBorder, false);
			graphics.fill3DRect(windowBorder * 2, windowBorder * 2,
				width - 4 * windowBorder,
				height - 4 * windowBorder, true);
		}
		graphics.setColor(textColor);
	}

	public void remove(Component component) {
		mainPanel.remove(component);
	}

	public void add(Component component, Object constraints) {
		mainPanel.add(component, constraints);
	}

	public Component add(Component component, int index) {
		return mainPanel.add(component, index);
	}

	public Component add(Component component) {
		return mainPanel.add(component);
	}

	public void setLayout(LayoutManager layoutManager) {
		if (mainPanel != null)
			mainPanel.setLayout(layoutManager);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getFontHeight() {
		return fontHeight;
	}

	protected void place() {
		if (width == 0 && height == 0) {
			mainPanel.validate();

			Dimension preferredSize = mainPanel.getPreferredSize();
			mainPanel.setSize(preferredSize);

			width = preferredSize.width + 6 * windowBorder;
			height = preferredSize.height + 8 * windowBorder + fontHeight;
		}

		setSize(width, height);
		if (x == -1 && y == -1) center();
		setLocation(x, y);
	}

	protected void center() {
		Dimension d = owner.getSize();
		x = (d.width - width) / 2;
		y = (d.height - height) / 2;
		/*
		setLocation(x, y);
	  validate();
	  */
	}

	protected GridBagConstraints makeConstraints(
		int gridX,
		int gridY,
		int gridWidth,
		int gridHeight,
		int fill,
		int weightX,
		int weightY,
		int anchorValue,
		Insets insets,
		int horizontalPadding,
		int verticalPadding
	) {
		GridBagConstraints constraints = new GridBagConstraints();

		constraints.gridx = gridX;
		constraints.gridy = gridY;
		constraints.gridwidth = gridWidth;
		constraints.gridheight = gridHeight;
		constraints.fill = fill;
		constraints.weightx = weightX;
		constraints.weighty = weightY;
		constraints.anchor = anchorValue;
		constraints.ipadx = horizontalPadding;
		constraints.ipady = verticalPadding;
		constraints.insets = insets;

		return constraints;
	}

	protected GridBagConstraints makeConstraints(
		int gridX,
		int gridY,
		int gridWidth,
		int gridHeight,
		int fill,
		int anchorValue,
		Insets insets,
		int horizontalPadding,
		int verticalPadding
	) {
		return makeConstraints(
			gridX,
			gridY,
			gridWidth,
			gridHeight,
			fill,
			1,
			1,
			anchorValue,
			insets,
			horizontalPadding,
			verticalPadding
		);
	}

	protected GridBagConstraints makeConstraints(
		int gridX,
		int gridY,
		int gridWidth,
		int gridHeight,
		int fill,
		int anchorValue,
		Insets insets
	) {
		return makeConstraints(
			gridX,
			gridY,
			gridWidth,
			gridHeight,
			fill,
			anchorValue,
			insets,
			0,
			0
		);
	}

	public void close() {
		setVisible(false);
		owner.remove(this);
	}

    /*public boolean handleEvent(Event event)
    {
	if( event.id == Event.MOUSE_DOWN )
	    {
		if(event.x > wndBorder && event.x < width-wndBorder &&
		   event.y > wndBorder && event.y < fontHeight + wndBorder )
		    {
			drag_x = event.x; drag_y = event.y;
			drag = true;
			border = new sBorder(x,y,width,height,owner);
			//setVisible(false);
			dragBuffer = null;
			return true;
		    }
		else
		    return super.handleEvent(event);
	    }
	if( event.id == Event.MOUSE_UP && drag )
	    {
		x += (event.x-drag_x);
		y += (event.y-drag_y);
		setLocation(x, y);
		owner.remove(border);
		border = null;

		drag = false;
		return true;
	    }
	if( (event.id == Event.MOUSE_DRAG )//|| event.id == Event.MOUSE_EXIT )
	    && drag )
	    {
		if( event.x != drag_x || event.y != drag_y )
		    {
			x += (event.x-drag_x);
			y += (event.y-drag_y);
			border.setLocation(x, y);
			validate();
			drag_x = event.x; drag_y = event.y;
		    }
		return true;
	    }
	return super.handleEvent(event);
    }
    */
}

/*class sBorder extends Component
    {
    private int w, h;

    public sBorder(int x, int y, int w, int h, Container owner)
    {
	super();
	owner.add(this);

	setLocation(x,y);
	setSize(w, h);
	this.w = w;
	this.h = h;
    }

    public void update(Graphics g)
    {
	paint(g);
    }

    public void paint(Graphics g)
    {
	g.setColor(Color.black);
	g.drawRect(0,0,w,h);
	g.setColor(Color.white);
	g.drawRect(1,1,w-2,h-2);
	g.setColor(Color.black);
	g.drawRect(2,2,w-4,h-4);
    }
}
*/
