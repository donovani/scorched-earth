package scorch.windows;


import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

class ScorchScrollButton extends Component implements MouseListener, MouseMotionListener {
	private static final Color d = Color.darkGray, w = Color.white,
		l = Color.lightGray, n = null;
	private static final Color[][] BUTTON_UP = {
		{n, n, n, n, n, w, n, n, n, n, n},
		{n, n, n, n, w, l, d, n, n, n, n},
		{n, n, n, n, w, l, d, n, n, n, n},
		{n, n, n, w, l, l, l, d, n, n, n},
		{n, n, n, w, l, l, l, d, n, n, n},
		{n, n, w, l, l, l, l, l, d, n, n},
		{n, n, w, l, l, l, l, l, d, n, n},
		{n, w, l, l, l, l, l, l, l, d, n},
		{n, w, l, l, l, l, l, l, l, d, n},
		{w, l, l, l, l, l, l, l, l, l, d},
		{d, d, d, d, d, d, d, d, d, d, d}};

	private static final Color[][] BUTTON_DOWN = {
		{w, w, w, w, w, w, w, w, w, w, w},
		{w, l, l, l, l, l, l, l, l, l, d},
		{n, w, l, l, l, l, l, l, l, d, n},
		{n, w, l, l, l, l, l, l, l, d, n},
		{n, n, w, l, l, l, l, l, d, n, n},
		{n, n, w, l, l, l, l, l, d, n, n},
		{n, n, n, w, l, l, l, d, n, n, n},
		{n, n, n, w, l, l, l, d, n, n, n},
		{n, n, n, n, w, l, d, n, n, n, n},
		{n, n, n, n, w, l, d, n, n, n, n},
		{n, n, n, n, n, d, n, n, n, n, n}};

	private static final Color[][][] BUTTONS = {BUTTON_UP, BUTTON_DOWN};
	public static final int SCROLL_UP = 0;
	public static final int SCROLL_DOWN = 1;

	private static final int buttonSize = 17;

	private final int type;
	private boolean buttonPressed = false;
	private boolean mousePressed = false;
	private final ScorchScrollPanel owner;

	public ScorchScrollButton(int type, ScorchScrollPanel owner) {
		super();

		this.type = type;
		this.owner = owner;

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public Dimension getPreferredSize() {
		return new Dimension(buttonSize, buttonSize);
	}

	public void paint(Graphics graphics) {
		Dimension size = getSize();
		int width = size.width, height = size.height;
		Color color;

		int sx = (width - BUTTONS[type][0].length) / 2,
			sy = (height - BUTTONS[type].length) / 2;

		graphics.setColor(Color.lightGray);
		graphics.fill3DRect(0, 0, width, height, true);

		for (int i = 0; i < BUTTONS[type][0].length; i++)
			for (int j = 0; j < BUTTONS[type].length; j++) {
				color = BUTTONS[type][j][i];
				if (color != null) {
					if (buttonPressed) {
						if (color == d)
							color = w;
						else if (color == w)
							color = d;
					}
					graphics.setColor(color);
					graphics.fillRect(sx + i, sy + j, 1, 1);
				}
			}
	}


	public void mouseClicked(MouseEvent event) {
	}

	public void mouseEntered(MouseEvent event) {
		/*
		// check coordinates to fix netscape bug.
		if( mousePressed && event.getX() >= 0 && event.getY() >= 0 )
		{
			System.err.println(event.getPoint());
			buttonPressed = true;
			repaint();
		}
		*/
	}

	public void mouseExited(MouseEvent event) {
		if (buttonPressed) {
			buttonPressed = false;
			repaint();
		}
	}

	public void mouseDragged(MouseEvent event) {
		int x = event.getX(), y = event.getY();

		if (buttonPressed &&
			(x < 0 || x >= buttonSize || y < 0 || y >= buttonSize)) {
			buttonPressed = false;
			repaint();
			return;
		}

		if (!buttonPressed &&
			(x >= 0 && x < buttonSize && y >= 0 && y < buttonSize)) {
			buttonPressed = true;
			repaint();
		}
	}

	public void mouseMoved(MouseEvent event) {
	}

	public void mousePressed(MouseEvent event) {
		buttonPressed = true;
		mousePressed = true;
		repaint();
	}

	public void mouseReleased(MouseEvent event) {
		if (buttonPressed) {
			buttonPressed = false;
			repaint();
			owner.buttonPress(type);
		}
		mousePressed = false;
	}
}
