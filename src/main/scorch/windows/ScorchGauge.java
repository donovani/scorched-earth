package scorch.windows;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
public class ScorchGauge extends ScorchPanel {
	private double percent;
	private int value;
	private final int max;

	public ScorchGauge(int value, int max, Container owner) {
		super(0, 0, -1, -1);

		this.percent = (double) value / max;
		this.value = value;
		this.max = max;
	}

	// when value displayed by gauge is changed, call this method
	public void updateValue(int val) {
		value = val;
		if (percent == (double) value / max) return;
		percent = (double) value / max;
		repaint();
	}

	public void update(Graphics graphics) {
		int w;

		super.update(graphics);
		w = (int) ((width - 2 - 2 * windowBorder) * percent);

		if (w > 0) {
			graphics.setColor(Color.red);
			graphics.fill3DRect(
				windowBorder + 1,
				windowBorder + 1,
				w,
				height - 2 * windowBorder - 2,
				true);
		}

		graphics.setColor(Color.white);
		graphics.drawString(
			value + "",
			(width - 2 * windowBorder - fontMetrics.stringWidth(value + "")) / 2,
			(height + fontMetrics.getMaxAscent()) / 2 - 1
		);
	}

	public Dimension getPreferredSize() {
		return new Dimension(0, (int) (fontHeight * 1.8));
	}
}
