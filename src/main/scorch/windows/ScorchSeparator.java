package scorch.windows;

import java.awt.*;

class ScorchSeparator extends ScorchPanel {
	public ScorchSeparator() {
		super(-1, -1);
	}

	public Dimension getPreferredSize() {
		Dimension size = super.getPreferredSize();
		size.width = windowBorder;
		return size;
	}

	public void paint(Graphics graphics) {
		Dimension size = getSize();
		int w = size.width, h = size.height;

		graphics.setColor(Color.lightGray);
		graphics.fill3DRect(0, 0, windowBorder, h, true);
	}
}
