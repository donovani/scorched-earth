package scorch.windows;

import java.awt.*;

/**
 * The root of scorchWindows package;
 * basic java panel functionality plus Scorch look and feel
 * @author Mikhail Kruk
 */
public class ScorchPanel extends Panel {
	public static final int windowBorder = 3;

	protected int width, height, x, y;
	protected FontMetrics fontMetrics = null;
	protected int fontHeight;

	protected Color backgroundColor = Color.lightGray,  // default window color
		textColor = Color.black;

	public ScorchPanel(int width, int height) {
		this(0, 0, width, height);
	}

	public ScorchPanel(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;

		setFont(new Font("Dialog", Font.BOLD, 12));
		fontMetrics = getFontMetrics(getFont());
		fontHeight = fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();
	}

	public void paint(Graphics g) {
		update(g);
	}

	public void update(Graphics graphics) {
		if (width == -1 && height == -1) {
			width = getSize().width;
			height = getSize().height;
		}

		graphics.setColor(backgroundColor);
		graphics.fill3DRect(0, 0, width, height, true);

		graphics.fill3DRect(windowBorder, windowBorder,
			width - 2 * windowBorder,
			height - 2 * windowBorder, false);
	}
}
