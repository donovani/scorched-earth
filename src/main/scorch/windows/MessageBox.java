package scorch.windows;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.StringTokenizer;

/**
 * Generic tool for providing small dialog boxes with a few buttons;
 * callbacks can be supplied to the constructor and associated with buttons.
 * @author Mikhail Kruk
 */
public class MessageBox extends ScorchWindow implements ActionListener {
	protected String[] buttons, callbacks;
	protected Object[] args;
	protected Container peer;
	protected int textAlign = Label.CENTER;

	public MessageBox(
		String title,
		String message,
		String[] buttons,
		String[] callbacks,
		Container owner,
		Container peer
	) {
		this(title, message, buttons, callbacks, owner);
		this.peer = peer;
		peer.setEnabled(false);
	}

	public MessageBox(
		String title,
		String message,
		String[] buttons,
		String[] callbacks,
		Container owner
	) {
		this(title, message, buttons, callbacks, null,
			Label.CENTER, owner);
	}

	public MessageBox(
		String title,
		String message,
		String[] buttons,
		String[] callbacks,
		int textAlign,
		Container owner
	) {
		this(title, message, buttons, callbacks, null,
			textAlign, owner);
	}

	public MessageBox(
		String title,
		String message,
		String[] buttons,
		String[] callbacks,
		Object[] args,
		Container owner
	) {
		this(title, message, buttons, callbacks, args, Label.CENTER, owner);
	}

	public MessageBox(
		String title,
		String message,
		String[] buttons,
		String[] callbacks,
		Object[] args,
		int textAlign,
		Container owner
	) {
		super(-1, -1, 0, 0, title, owner);

		this.buttons = buttons;
		this.callbacks = callbacks;
		this.args = args;
		this.textAlign = textAlign;

		Panel buttonPanel = new Panel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		for (int i = 0; i < buttons.length; i++) {
			Button button = new Button(buttons[i]);
			buttonPanel.add(button);
			button.addActionListener(this);
		}

		/* build the text (multiline) panel */
		Panel textPanel = new Panel();
		StringTokenizer tokenizer = new StringTokenizer(message, "" + '\n');
		textPanel.setLayout(new GridLayout(tokenizer.countTokens(), 1, 0, 0));
		while (tokenizer.hasMoreTokens())
			textPanel.add(new Label(tokenizer.nextToken(), textAlign));

		// this will put text into a nice 3d box. May become option one day
		// ScorchPanel spl = new ScorchPanel(-1,-1);
		// spl.add(textPanel);

		setLayout(new BorderLayout(0, 0));
		add(textPanel, BorderLayout.CENTER);
		if (buttons.length > 0)
			add(buttonPanel, BorderLayout.SOUTH);

		validate();
	}

	public void close() {
		// if the box was modal, enable other windows
		if (peer != null)
			peer.setEnabled(true);
		super.close();
	}

	public void actionPerformed(ActionEvent event) {
		Method method = null;
		String command = event.getActionCommand();

		if (peer != null)
			peer.setEnabled(true);

		if (callbacks == null) {
			close();
			return;
		}
		for (int i = 0; i < buttons.length; i++) {
			if (buttons[i].equals(command)) {
				close();
				if (callbacks[i] == null) return;
				try {
					Class[] type;
					Object[] arg;
					if (args == null || args[i] == null) {
						type = new Class[0];
						arg = new Object[0];
					} else {
						type = new Class[1];
						arg = new Object[1];
						type[0] = args[i].getClass();
						arg[0] = args[i];
					}
					method = owner.getClass().getMethod(callbacks[i], type);
					method.invoke(owner, arg);
				} catch (Throwable t) {
					System.err.println("MessageBox: " + t);
					System.err.println("method: " + callbacks[i]);
				}
				return;
			}
		}
	}
}
