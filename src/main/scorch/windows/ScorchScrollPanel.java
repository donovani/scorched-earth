package scorch.windows;

import java.awt.*;
import java.util.Vector;

/**
 * All the GUI elements needed to build a scrollable table with horizontal entries
 *
 * @author Mikhail Kruk
 */
public class ScorchScrollPanel extends ScorchPanel {
	private final int rows; // number of visible rows
	private int startIndex; // the first rows displayed in panel (currently)
	private final int totalRows; // total number of rows in the panel
	private Panel mainPanel;
	private final Panel wrapperPanel; // wrapper panel around mainPanel
	private ScorchScrollBar scrollbar;
	private final Vector panels;
	private Container owner;

	public ScorchScrollPanel(Container owner, int width, int height, Vector panels, int rows) {
		super(width, height);

		this.totalRows = panels.size();
		this.rows = Math.min(rows, totalRows);
		this.panels = panels;

		startIndex = 0;

		wrapperPanel = new Panel(new BorderLayout(0, 0));
		wrapperPanel.setBackground(Color.gray);

		rebuild();

		// do we need a scroll bar?
		if (rows < totalRows) {
			scrollbar = new ScorchScrollBar(this);
			wrapperPanel.add(scrollbar, BorderLayout.EAST);
		}

		add(wrapperPanel);
	}

	private void rebuild() {
		Panel newPanel = new Panel(new GridLayout(rows, 1, 0, 0)), childPanel;
		newPanel.setBackground(Color.gray);

		for (int i = startIndex; i < startIndex + rows; i++) {
			childPanel = (Panel) panels.elementAt(i);
			newPanel.add(childPanel);
			newPanel.validate();
		}

		if (mainPanel != null)
			wrapperPanel.remove(mainPanel);

		mainPanel = newPanel;

		wrapperPanel.add(mainPanel, BorderLayout.CENTER);
		wrapperPanel.validate();
		//mainPanel.validate();
	}

	public void setIndex(int index) {
		if (startIndex == index ||
			(startIndex == 0 && index < 0) ||
			(index + rows >= totalRows && startIndex == totalRows - rows))
			return;

		startIndex = index;

		if (startIndex < 0)
			startIndex = 0;
		else if (startIndex + rows >= totalRows)
			startIndex = totalRows - rows;

		rebuild(); // really changed
	}

	public void buttonPress(int type) {
		if (type == ScorchScrollButton.SCROLL_UP) {
			if (startIndex > 0)
				setIndex(startIndex - 1);
			return;
		}
		if (type == ScorchScrollButton.SCROLL_DOWN) {
			if (startIndex + rows < totalRows)
				setIndex(startIndex + 1);
			return;
		}
		System.err.println("sScrollPanel.buttonPress(): Unknown button type: " + type);
	}

	public boolean handleEvent(Event event) {
		int newIndex = startIndex;

		if (event.id == Event.KEY_ACTION) {
			switch (event.key) {
				case Event.PGUP:
					newIndex -= (rows);
					break;
				case Event.PGDN:
					newIndex += (rows);
					break;
				case Event.UP:
					newIndex--;
					break;
				case Event.DOWN:
					newIndex++;
					break;
			}
			setIndex(newIndex);
		}
		return super.handleEvent(event);
	}
}
