package scorch;

/**
 * This interface indicates that the implementing class is either an explosion or contains an explosion.
 * Explosion, Player, and GenericMissile implement this interface (also see ExplosionInfo).
 * @author Nathan Roslavker
 */

import scorch.weapons.ExplosionInfo;

public interface Explodable extends FrameShow {
	ExplosionInfo getExplosionInfo();

	int calculateDamage(ScorchPlayer sp);
}
