package scorch;

import scorch.gui.*;
import scorch.items.Fuel;
import scorch.items.Item;
import scorch.services.ImageService;
import scorch.utility.Debug;
import scorch.weapons.FireExplosion;
import scorch.weapons.GenericMissile;
import scorch.weapons.SimpleExplosion;
import scorch.windows.MessageBox;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.net.URL;
import java.util.Random;
import java.util.Vector;

/**
 * Abstraction based on ScorchApplet that separates out
 * the logic of the game from the Frame containing it.
 */
public class ScorchGame implements FocusListener {
	public static String Version = "v1.061, 4/11/2001";

	public static boolean sounds = false;

	private Random rand;
	private static final String paramWidth = "gameWidth";
	private static final String paramHeight = "gameHeight";
	private static final String paramPort = "port";        // server port
	private static final String paramLeaveURL = "leaveURL";
	private static final String paramHelpURL = "helpURL";
	private static final String paramDev = "dev";            // development version
	private static final String paramDT = "dt";     // desync test
	private static final String paramBanners = "banners";


	private static ScorchGame instance = null;

	private final URL leaveURL;
	private final URL helpURL;

	private int gameWidth = 800;
	private int gameHeight = 600;
	private int port = 4242;
	private int width;
	private int height;

	public static String JVM;

	private ScorchField scorch;
	private ScorchFrame scorchFrame;
	private Network network;
	private Vector players;
	private PlayersLister playersList;
	private GameSettings gameSettings;
	private MessageBox messageBox;
	private ChatBox chatBox;
	private MainToolbar mainToolbar;
	private GradientPanel gradientPanel; // turned off

	private boolean master, massKilled = false, newPlayer = false;
	private int myID, activePlayer = -1, roundCount = 0, moveCount = 0;
	private boolean paid = false;
	private ScorchPlayer myPlayer;

	private Vector banners;
	private Vector bannersURL;
	private Label timerLabel;

	private final String baseUrl;

	public boolean galslaMode = false;

	public Container container;


	public ScorchGame(Container container, int width, int height, String baseUrl, int port, URL leaveUrl, URL helpUrl) {
		this.container = container;
		this.gameWidth = width;
		this.gameHeight = height;
		this.baseUrl = baseUrl;
		this.port = port;
		this.leaveURL = leaveUrl;
		this.helpURL = helpUrl;
	}


	public void init() {
		String runMode = "";

		instance = this;

		container.addFocusListener(this);

		JVM = System.getProperty("java.version") + " " +
			System.getProperty("java.vendor") + " " +
			System.getProperty("os.name") + " " +
			System.getProperty("os.arch");

		if (Debug.desyncTest) {
			System.out.println("\tDesync-test mode");
			runMode = runMode + "desync-test ";
		}
		if (Debug.dev) {
			System.out.println("\tDeveloper's mode");
			runMode = runMode + "dev mode ";
		}
		if (Debug.debugLevel > 0) {
			System.out.println("\tDebug level set to " + Debug.debugLevel);
			runMode = runMode + "debug level = " + Debug.debugLevel + " ";
		}

		runMode = runMode + Version;

		Dimension size = container.getSize();
		width = size.width;
		height = size.height;
		container.setBackground(Color.black);

		container.setLayout(null);

		Label ver = new Label(runMode, Label.RIGHT);
		ver.setForeground(Color.white);
		container.add(ver);
		ver.setLocation(0, height - 20);
		ver.setSize(width, 20);
		timerLabel = new Label("", Label.RIGHT);
		timerLabel.setForeground(Color.white);
		container.add(timerLabel);
		timerLabel.setLocation(0, 0);
		timerLabel.setSize(width, 20);

		loginWindow("");

		container.validate();
	}

	public void start() {
		if (scorch != null)
			scorch.start();
	}

	public void stop() {
		if (gradientPanel != null) {
			gradientPanel.stop();  // it's a good stop, not thread's stop()
			gradientPanel = null;
		}

		if (messageBox != null) {
			messageBox.close();
			messageBox = null;
		}

		if (chatBox != null) {
			chatBox.close();
			chatBox = null;
		}

		if (scorchFrame != null)
			scorchFrame.close();
		if (mainToolbar != null) {
			mainToolbar.hideMenu();
			mainToolbar.close();
		}

		if (scorch != null)
			scorch.stop(); // it's a good stop, not thread's stop
		if (network != null)
			network.quit();

		Debug.closeConsole();

		System.out.println("Scorched Earth 2000 Terminated.");
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getGameWidth() {
		return gameWidth;
	}

	public int getMyID() {
		return myID;
	}

	public ScorchPlayer getMyPlayer() {
		return myPlayer;
	}

	public ScorchPlayer getPlayerByID(int id) {
		ScorchPlayer player;

		for (int i = 0; i < getPlayersNum(); i++) {
			player = getPlayer(i);
			if (player.getID() == id)
				return player;
		}

		return null;
	}

	public synchronized ScorchPlayer getPlayer(int num) {
		return (ScorchPlayer) (players.elementAt(num));
	}

	public synchronized ScorchPlayer getActivePlayer() {
		return getPlayerByID(activePlayer);
	}

	public synchronized int getPlayersNum() {
		return players.size();
	}

	public synchronized Vector getPlayers() {
		return players;
	}

	public String getName() {
		return myPlayer.getName();
	}

	public boolean isMaster() {
		return master;
	}

	public boolean isGuest() {
		return myPlayer.getProfile().isGuest();
	}

	public boolean isNewPlayer() {
		return newPlayer;
	}

	public boolean isMassKilled() {
		return massKilled;
	}

	public int getMaxRounds() {
		return gameSettings.maxRounds;
	}

	public int getRoundCount() {
		return roundCount;
	}


	public synchronized void sendTankDead(int id) // made synchronous 6/8/0
	{
		if (master)
			network.sendTankDead(id);
	}

	public synchronized void sendEOT() {
		sendEOT("");
	}

	public synchronized void sendEOT(String message) // made synchronous 6/8/0
	{
		String desyncTestString = "" + rand.nextInt();

		for (int i = 0; i < getPlayersNum(); i++)
			desyncTestString = desyncTestString + Protocol.separator + getPlayer(i).getPowerLimit();

		Debug.log("sendEOT(): " + message + " " + Thread.currentThread());
		network.sendEOT(desyncTestString);
	}

	public void updateUser(PlayerProfile profile, boolean encrypt) {
		if (!profile.isGuest())
			network.sendUpdateUser(profile, encrypt);
	}

	// UI callbacks

	private synchronized boolean connect() {
		if (network == null)
			try {
				// TODO: Validate this works with baseUrl, originally was getDocumentBase().getHost()
				network = new Network(baseUrl, port, this);

				if (Debug.dev)
					Debug.initConsole(network);

				return true;
			} catch (Exception e) {
				String[] buttons = {"OK"};
				String[] callbacks = {"Quit"};
				MessageBox error = new MessageBox(
					"Error",
					"Connection to the server failed", buttons, callbacks, container
				);
				error.display();
			}
		return false;
	}

	public synchronized void selectWeapon(int type) {
		myPlayer.setWeapon(type);
		mainToolbar.updateWeapon();
	}

	public /*synchronized*/ void useItem(int type, int arg) {
		myPlayer.useItem(type, arg);
		network.sendUseItem(type, arg);
	}

	// called by the Fuel item when user activates it
	public synchronized void showFuelWindow(Fuel fuel) {
		FuelBox fb = new FuelBox(this);
		mainToolbar.enableKeys(false);
		fb.display();
	}

	// called from Fuel Window whet it is closed to let the game continue
	public synchronized void closeFuelWindow() {
		mainToolbar.enableKeys(true);
	}

	public synchronized void loginWindow(String name) {
		LoginWindow login = new LoginWindow(name, this);
		login.display();
	}

	public synchronized void login(PlayerProfile profile, String gamePassword) {
		newPlayer = false;
		if (connect())
			network.sendLogin(profile, gameWidth + "x" + gameHeight + " " + Version + gamePassword);
	}

	public synchronized void newUser(String name) {
		NewUser newUser = new NewUser(name, this);
		newUser.display();
	}

	public synchronized void sendNewUser(PlayerProfile profile) {
		newPlayer = true;
		if (connect())
			network.sendNewUser(profile, gameWidth + "x" + gameHeight + " " + Version);
	}

	public synchronized void createGame(int maxPlayers, String[] aiPlayers) {
		GameOptions options = new GameOptions(
			myPlayer.getName() + " (master)",
			maxPlayers,
			myPlayer.getProfile(),
this
		);
		options.display();
		playersList = options;

		network.sendMaxPlayers(maxPlayers);
		for (int i = 0; i < aiPlayers.length; i++)
			network.sendAIPlayer(aiPlayers[i]);
	}

	public synchronized void setGameOptions(PlayerProfile profile) {
		messageBox = new MessageBox(
			"Message",
			"Please wait while players set their options",
			new String[0],
			new String[0],
			container
		);
		messageBox.display();

		playersList = null;
		network.sendPlayerSettings(myID, new PlayerSettings(profile));
		network.sendGameSettings(new GameSettings(profile));

		if (profile.getSounds())
			loadSounds();
	}

	public synchronized void joinGame(PlayerSettings playerSettings) {
		playersList = null;
		network.sendPlayerSettings(myID, playerSettings);

		messageBox = new MessageBox(
			"Message",
			"Please wait while master sets game options",
			new String[0],
			new String[0],
			container
		);
		messageBox.display();

		if (playerSettings.sounds)
			loadSounds();
	}

	public synchronized void showChat(char character) {
		if (chatBox != null) return;
		chatBox = new ChatBox(character, this);
		chatBox.display();
	}

	// Called by chat window when it's closed.
	// Say determines whether user decided to send message or not, playerIndex determines recipient of the message
	public synchronized void closeChat(boolean say, int playerIndex) {
		if (say) {
			if (playerIndex > 0)
				playerIndex--;
			else
				playerIndex = -1;
			network.sendShout(chatBox.getMessage(), myPlayer.getName(), playerIndex);
		}
		chatBox = null;
		mainToolbar.requestFocus();
	}

	// TODO: Determine if needed
	public synchronized int changeAngle(int amount) {
		return myPlayer.increaseAngle(amount);
	}

	// TODO: Determine if needed
	public synchronized int changePower(int amount) {
		return myPlayer.increasePower(amount);
	}

	public void updatePowerLabel(ScorchPlayer player) {
		if (player == myPlayer)
			mainToolbar.updatePowerLabel(player.getPower());
	}

	public synchronized void setWeapon(int weapon) {
		myPlayer.setWeapon(weapon);
	}

	public synchronized void sendUpdate(ScorchPlayer player) {
		network.sendUpdate(player.getID(), player.getPower(), player.getAngle());
	}

	public /*synchronized*/ void receiveUseItem(int itemId, int arg) {
		ScorchPlayer player;
		if (activePlayer == myID)
			return;
		player = getActivePlayer();
		player.useItem(itemId, arg);
	}

	public synchronized void fire() {
		int weaponNumber = myPlayer.getWeapon();
		sendUpdate(myPlayer);
		network.sendUseWeapon(weaponNumber);
		scorch.fire(myPlayer, weaponNumber);
		myPlayer.decrementWeapon();
	}

	public synchronized void fire(int weapon) {
		mainToolbar.enableKeys(false);
		if (activePlayer == myID)
			return;

		ScorchPlayer player = getActivePlayer();
		scorch.fire(player, weapon);
	}

	/**
	 * User selects mass kill, we notify server and do nothing.
	 * The server waits until the current turn is finished (doesn't wait if it is master turn),
	 * and then forwards the MASSKILL message to all players.
	 * See ScorchGame.receiveMassKill() for more.
 	 */
	public synchronized void massKill() {
		if (!massKilled) {
			mainToolbar.enableKeys(false);
			massKilled = true;
			network.sendMassKill();
		}
	}

	public synchronized void displayReference() {
		String help = "Hot keys:\nArrow keys : change power and angle setting with step 1\nCtrl : (with arrow keys) change power and angle with step 5\nPgUp/PgDn : change power with step 10\nHome/End : change angle with step 10\nSpace : Fire\nF1 : this reference window\nF2 : Statistics\nF3 : Edit profile\nF4 : Mass kill\nF5 : Inventory\nF10 : System menu\nTo chat during the game just start typing and chat window will popup.\n";
		String[] buttons = {"OK, thanks", "Open manual"};
		String[] callbacks = {null, "showHelp"};
		MessageBox referenceBox = new MessageBox("Quick Reference", help, buttons, callbacks, Label.LEFT, container);
		referenceBox.display();
	}

	public synchronized void requestTopTen() {
		network.sendTopTen();
	}

	// Network callbacks

	public synchronized void loggedIn(int id, PlayerProfile profile, boolean ai) {
		ScorchPlayer newPlayer;

		if (players == null) // I am logged in. Initialize everything
		{
			// first player is always me
			myID = id;
			players = new Vector(ScorchPlayer.MAX_PLAYERS);
			master = (myID == 0);

			if (Debug.dev) {
				ChatScreen chatScreen = new ChatScreen(width, height, this);
				container.add(chatScreen);
				chatScreen.setLocation(0, 0);
				chatScreen.validate();
				chatScreen.setVisible(true);
			}

			/*
			gradient = new GradientPanel(width, height, new Color(Tanks.getTankColor(myID)), Color.black);
			add(gradient);
			validate();
			*/

			ScorchWindow options;
			if (master)
				options = new StartGame(this);
			else {
				options = new JoinGame(profile, this);
        // options = new GameOptions(profile.getName(), -1, false, this);
				playersList = (PlayersLister) options;
			}
			options.display();

		}

		if (!ai)
			newPlayer = new ScorchPlayer(id, profile, this);
		else
			newPlayer = new AIPlayer(id, profile, this);

		if (id == myID) {
			myPlayer = newPlayer;
		}

		for (int i = 0; i <= getPlayersNum(); i++)
			if ((i == getPlayersNum()) || (getPlayer(i).getID() > id)) {
				players.insertElementAt(newPlayer, i);
				break;
			}

		if (playersList != null) {
			String name = profile.getName();
			if (id == 0) name = name + " (master)";
			if (ai) name = name + " (AI)";
			playersList.addPlayer(name);
		}
	}

	public synchronized void showDisconnect(String message) {
		String[] buttons = {"OK"};
		String[] callbacks = {"Quit"};
		MessageBox error = new MessageBox("Message", message, buttons, callbacks, container);
		network.quit();
		container.removeAll();
		error.display();
	}

	// This doesn't need to be synchronized, because it is called by the server only
	// when all users controls are disabled (when all users expect MAKETURN command).
	// It's also only called by the Network thread, so it's safe.
	// The problem with this is that Network is unusable while player explodes.
	// This means that chat will not work and on a *slow* system client may time out.
	// The fix would be to start separate threads in ScorchField, but it doesn't work
	// for playerLeft, since server doesn't expect EOT and sends MAKETURN immediately.
	// TODO: Change protocol so that LEFT would work the same way as MASSKILL does (same applies to the receiveMassKill)
	public void playerLeft(int ID) {
		ScorchPlayer player = getPlayerByID(ID);

		if (player == null) {
			System.err.println("ScorchApplet.playerLeft(): Player " + ID +
				" not found");
			return;
		}

		if (chatBox != null) // dirty trick to save private messages
		{
			chatBox.close();
			chatBox = null;
		}

		if (playersList != null) {
			if (ID != 0)
				playersList.removePlayer(player.getName());
			else
				playersList.removePlayer(player.getName() + " (master)");
		}

		players.removeElement(player);

		if (scorch != null)
			scorch.playerLeft(player);
	}

	public synchronized void errorLogin(String name, String reason) {
		String[] buttons = {"Retry", "Cancel"};
		String[] callbacks = {"loginWindow", "Quit"};
		String[] args = {name, null};
		MessageBox errorBox;
		String message = "Login failed for unknown reason";

		network.quit();
		network = null;

		if (reason.equals(Protocol.wrongUsername)) {
			message = "Unrecognized username";
			//args[0] = "";
		}
		if (reason.equals(Protocol.usernameTaken)) {
			message = "This username is already taken";
			args[0] = "";
		}
		if (reason.equals(Protocol.wrongPassword))
			message = "Incorrect password";
		if (reason.equals(Protocol.alreadyLoggedIn))
			message = "User " + name + " is already logged in";

		errorBox = new MessageBox("Login failed", message, buttons, callbacks, args, container);
		errorBox.display();
	}

	public synchronized void Quit() {
		stop();
		// TODO: Determine platform agnostic version
		// getAppletContext().showDocument(leaveURL);
		System.err.println("Not implemented");
	}

	public synchronized void banner(URL bannerUrl) {
		if (bannerUrl != null) {
			// TODO: Determine platform agnostic version
			// getAppletContext().showDocument(bannerUrl,"_blank");
			System.err.println("Not implemented");
			paid = true;
		}
	}

	public synchronized void showHelp() {
		// TODO: Determine platform agnostic version
		// getAppletContext().showDocument(helpURL,"Scorched Earth 2000 Help");
		System.err.println("Not implemented");
	}

	public synchronized void showTopTen(Vector profiles) {
		StatsWindow statsWindow = new StatsWindow(StatsWindow.TOP_TEN, profiles, this);
		statsWindow.display();
	}

	public synchronized void shop() {
		if (messageBox != null) {
			messageBox.close();
			messageBox = null;
		}

		ShopWindow sw = new ShopWindow(
			(int) (gameWidth * 0.9),
			(int) (gameHeight * .7),
			this
		);
		sw.display();
	}

	public synchronized void startGame() {
		if (gradientPanel != null) {
			gradientPanel.stop();
			gradientPanel = null;
		}
		container.removeAll();

		messageBox = new MessageBox
			("Message", "Please wait while game loads",
				new String[0], new String[0], container);
		messageBox.display();

		chatBox = null;

		for (int i = 0; i < getPlayersNum(); i++) {
			ScorchPlayer player = getPlayer(i);
			player.resetAll();
		}

		massKilled = false;

		scorchFrame = new ScorchFrame(gameWidth, gameHeight, rand, this);
		scorchFrame.display();
		scorch = scorchFrame.getScorchField();

		// TODO: See if we have to do this EACH round?
		mainToolbar = new MainToolbar(players, this);
		mainToolbar.display();
		mainToolbar.enableKeys(false);

		playersList = mainToolbar.getPlayersList();
		if (playersList == null)
			System.err.println("ScorchApplet.startGame() playersList is null.");

		if (gameSettings.wind == GameSettings.CONSTANT_WIND)
			Physics.setWind(rand.nextInt() % Physics.MAX_WIND);

		if (master) aiBuyItems();

		moveCount = 0;
		sendEOT();

		if (messageBox != null) messageBox.close();
		messageBox = new MessageBox(
			"Message",
			"Please wait for other players to sync",
			new String[0],
			new String[0],
			container,
			mainToolbar
		);
		messageBox.display();

		// if it is first turn of a guest or a new player show help
		if (roundCount == 0 && (isGuest() || isNewPlayer()))
			displayReference();

		if (!paid && banners != null) {
			Random r = new Random();
			int bannerNumber = Math.abs(r.nextInt() % banners.size());
			(new BannerWindow(this, (String) banners.elementAt(bannerNumber),
				(String) bannersURL.elementAt(bannerNumber))).display();
		}

		scorch.newSystemMessage("Round #" + (++roundCount) + " out of " + getMaxRounds());
	}

	private void aiBuyItems() {
		for (int i = 0; i < getPlayersNum(); i++) {
			ScorchPlayer aiPlayer = ((ScorchPlayer) players.elementAt(i));
			if (aiPlayer instanceof AIPlayer)
				((AIPlayer) aiPlayer).buyAmmunition();
		}
	}

	public synchronized void makeTurn(int id) {
		activePlayer = id;

		if (playersList != null)
			((PlayersListControl) playersList).highlight(getPlayerByID(id));

		if (messageBox != null) {
			messageBox.close();
			messageBox = null;
		}

		if (Debug.desyncTest) {
			getPlayerByID(id).activateParachute(3);
			network.sendUseItem(Item.Parachute, 3);
		}

		if (gameSettings.wind == GameSettings.CHANGING_WIND) {
			Physics.setWind(rand.nextInt() % Physics.MAX_WIND);
			scorch.repaint();
		}

		// for desync testing stop round after some number of shots
		moveCount++;
		if (master && Debug.desyncTest && moveCount > 90) {
			massKill();
			if (id == myID) return;
		}

		if (id == myID) // our player's turn
		{
			if (Debug.desyncTest) // skip turn if desync test
			{
				mainToolbar.enableKeys(false);
				fire();
			} else {
				if (!myPlayer.isFirstTurn()) {
					mainToolbar.enableKeys(true);
					// request focus only if not in chat window
					if (chatBox == null)
						mainToolbar.requestFocus();
				} else {
					if (myPlayer.useAutoDefense()) {
						AutoDefenseWindow autoDefenseWindow = new AutoDefenseWindow(this);
						autoDefenseWindow.display();
					} else
						sendEOT();
				}
			}
		} else {
			mainToolbar.enableKeys(false);

			ScorchPlayer player = getPlayerByID(id);

			if (master && player instanceof AIPlayer)
				((AIPlayer) player).makeTurn(); // takes care of first turn too
			else if (player.isFirstTurn())
				sendEOT();

			/*
			if( player.isFirstTurn() )
				sendEOT();
			else
				if( master && player instanceof AIPlayer)
					((AIPlayer)player).aimedFire();
			*/
		}
	}

	synchronized void aiFire(ScorchPlayer aiPlayer) {
		sendUpdate(aiPlayer);
		network.sendUseWeapon(aiPlayer.getWeapon());
	}

	synchronized void receiveGameSettings(GameSettings gameSettings, long seed) {
		this.gameSettings = gameSettings;
		rand = new Random(seed); //seed

		for (int i = 0; i < getPlayersNum(); i++)
			((ScorchPlayer) players.elementAt(i)).setRandom(rand);

		Physics.setGravity(this.gameSettings.gravity);

		if (this.gameSettings.wind == GameSettings.NO_WIND)
			Physics.setWind(0);

		// give all the items in the lamer mode. This must be done before
		// startGame() is called because startGame() loads some UI components
		if (gameSettings.lamerMode)
			myPlayer.giveWeapons();

		// no clue why this check is here. investigate
		if (playersList == null) {
			myPlayer.setCash(gameSettings.initialCash);
			if (gameSettings.initialCash <= 0)
				startGame();
			else
				shop();
		} else
			System.err.println("ScorchApplet.receiveGameSettings(): playersList == null");
	}

	synchronized void receivePlayerOptions(int id, int type) {
		ScorchPlayer player = getPlayerByID(id);

		if (player != null)
			player.setTankType(type);
		else
			System.err.println("receivePlayerOptions: invalid id");
	}

	synchronized void receiveUpdate(int id, int newPower, int newAngle) {
		// ignore the update for itself if server forwards it for some reason
		if (id == myID)
			return;

		ScorchPlayer player = getPlayerByID(id);

		// If AI player is updated master should ignore this
		if (master && player instanceof AIPlayer)
			return;

		player.setAngle(newAngle);
		player.setPower(newPower);
	}

	synchronized void receiveEOR() {
		massKilled = true; // to disable mass kill until next round starts

		if (playersList != null)
			((PlayersListControl) playersList).highlight(null);

		StatsWindow statsWindow = new StatsWindow(StatsWindow.END_OF_ROUND, this);

		if (Debug.desyncTest)
			startGame();
		else
			statsWindow.display();
	}

	synchronized void receiveEOG() {
		if (playersList != null)
			((PlayersListControl) playersList).highlight(null);
		updateUser(myPlayer.getProfile(), false);

		StatsWindow sw = new StatsWindow(StatsWindow.END_OF_GAME, this);
		sw.display();
	}

	synchronized void newChatMessage(String message) {
		if (scorch != null)
			scorch.newChatMessage(message);
	}

	// Server decided that it's ok now to mass kill, so just do it.
	// See comments to the playerLeft()
	public void receiveMassKill() {
		scorch.massKill();
	}

	public void focusLost(FocusEvent event) {
	}

	public void focusGained(FocusEvent event) {
		if (mainToolbar != null) mainToolbar.requestFocus();
	}

	public void setTimerLabel(int time) {
		if (time < 0)
			timerLabel.setText("");
		else
			timerLabel.setText("" + time);
	}

	private void loadSounds() {
		sounds = true;

		GenericMissile.loadSounds(this);
		SimpleExplosion.loadSounds(this);
		FireExplosion.loadSounds(this);
	}

	public static Image getImage(String path) {
		if (instance == null) {
			System.err.println("getImage() call failed!");
			return null;
		}
		// TODO: Validate this works with ImageService, original was:
		//       applet.getImage(applet.getCodeBase(), path)
		return ImageService.instance().load(path);
	}
}
