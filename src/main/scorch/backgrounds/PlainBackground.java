package scorch.backgrounds;

import java.awt.*;

/**
 * The simplest background -- just one color
 * @author Mikhail Kruk
 */
public class PlainBackground extends Background {
	private final int color;

	public PlainBackground(int width, int height, Color color) {
		super(width, height);

		this.color = color.getRGB();
	}

	public int getPixelColor(int x, int y) {
		return color;
	}
}
