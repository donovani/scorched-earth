package scorch.backgrounds;

import java.util.Random;

/**
 * The night stars background.
 * @author Mikhail Kruk
 */
public class StarsBackground extends Background {
	private final int[] stars;
	private final Random random;

	public StarsBackground(int width, int height, Random random) {
		super(width, height);

		this.random = random;

		stars = new int[width];

		for (int i = 0; i < width; i++) {
			if (i % 3 == 0)
				stars[i] = Math.abs(random.nextInt()) % height;
			else
				stars[i] = -1;
		}
	}

	public int getPixelColor(int x, int y) {
		int i;

		if (x < 0 || x >= width || y < 0 || y >= height)
			return 0;

		i = 255 - (255 * y / height);

		if (stars[x] == y)
			return (255 << 24) | (i << 16) | (i << 8) | i;
		else
			return (255 << 24) | (0 << 16) | (0 << 8) | 0;
	}
}
