package scorch.weapons;

import scorch.*;
import scorch.services.AudioService;

import javax.sound.sampled.Clip;
import java.awt.*;

/**
 * Encapsulates basic missile behavior, such as trajectory calculation,
 * collision detection, and explosion upon collision.
 *
 * @author Nathan Roslavker
 * @author Mikhail Kruk
 */

public class GenericMissile extends PhysicalObject implements Explodable {
	private static Clip sfx;

	protected final static int b = Color.black.getRGB();
	protected final static int w = Color.white.getRGB();

	protected Explosion explosion;

	protected int width, height;
	protected int previousX = -1, previousY = -1;
	protected Color tracer = null;
	protected ExplosionInfo info = null;
	protected int frameNumber = 0;
	protected int[][] missile;

	protected int[] trajectoryX, trajectoryY; // trajectory arrays
	protected int step = 0;
	private final int stepCount = 50;

	//private state variables
	protected final static int EXPLOSION = 1, MISSILE = 0, END = 2;
	protected int state;

	public GenericMissile(
		Bitmap bitmap,
		Physics physics,
		int[][] sprite,
		Explosion explosion
	) {
		super(bitmap, physics);

		height = sprite.length;
		width = sprite[0].length;

		setPosition(physics.getStartX(), bitmap.getHeight() - physics.getStartY());

		this.missile = sprite;
		this.explosion = explosion;

		trajectoryX = new int[stepCount];
		trajectoryY = new int[stepCount];

		physics.calculateTrajectory(trajectoryX, trajectoryY, stepCount);

		state = MISSILE;
	}

	// This function returns an approximate point where the collision occurred.
	// If the return value is null, there was no collision.
	protected Point calculateCollision(int x, int y) {
		if (previousX == -1 || previousY == -1) return null; // first call
		if (y < 0 || previousY < 0) return null; // out of the screen
		return bitmap.intersectLine(previousX, previousY, x, y);
	}

	public int calculateDamage(ScorchPlayer sp) {
		if (state == END)
			return explosion.calculateDamage(sp);
		else
			return 0;
	}

	public ExplosionInfo getExplosionInfo() {
		if (state == END)
			return explosion.getExplosionInfo();
		else {
			ExplosionInfo explosionInfo = null;
			Point point = null; //lcCollision(xt[0],bitmap.getHeight()-yt[0]);
			int i;
			do {
				for (i = 0; i < stepCount && trajectoryX[i] < bitmap.getWidth() && trajectoryX[i] >= 0 && point == null; i++) {
					if ((bitmap.getHeight() - trajectoryY[i]) >= 0)
						point = calculateCollision(trajectoryX[i], bitmap.getHeight() - trajectoryY[i]);
					previousX = trajectoryX[i];
					previousY = bitmap.getHeight() - trajectoryY[i];
				}
				if (i == stepCount)
					physics.calculateTrajectory(trajectoryX, trajectoryY, stepCount);
			}
			while (i == stepCount);

			if (point != null) {
				// prepare for calculateDamage()
				explosionInfo = new ExplosionInfo();
				x = point.x;
				y = point.y;
				explosion.setPosition(x, y);
				state = END;
			}
			return explosionInfo;
		}
	}

	public boolean isExploding() {
		return (state == EXPLOSION);
	}

	public void drawFrame(boolean update) {
		if (!isExploding()) {
			if (tracer == null) {
				bitmap.drawSprite(x - width / 2, y - height / 2, missile, 0);
				if (update)
					bitmap.newPixels(x - width / 2, y - height / 2, width, height);
				bitmap.hideSprite(x - width / 2, y - height / 2, missile, 0);
			}
		} else
			explosion.drawFrame(update);
	}

	public void hideFrame() {
		if (!isExploding()) {
			if (tracer == null) {
				bitmap.newPixels(x - width / 2, y - height / 2,
					width, height);
			}
		} else
			explosion.hideFrame();
	}

	protected void initExplosion(int ex, int ey) {
		if (explosion instanceof Directional)
			((Directional) explosion).setSpeed(physics.getHSpeed());

		hideFrame();
		explosion.setPosition(ex, ey);
		setPosition(ex, ey);
		state = EXPLOSION;
	}

	public boolean drawNextFrame(boolean update) {
		Point point;

		switch (state) {
			case MISSILE:
				if (frameNumber++ > 0) {
					if (update) hideFrame();

					if (tracer != null && frameNumber > 1) {
						bitmap.setDirectDraw(true);
						bitmap.setColor(tracer);
						bitmap.drawLine(x, y, previousX, previousY);
						bitmap.setDirectDraw(false);

				/*if( update )
				    bitmap.newPixels(Math.min(x, prev_x)-2,
						     Math.min(y, prev_y)-2,
						     Math.abs(prev_x-x)+4,
						     Math.abs(prev_y-y)+4);*/
						bitmap.setColor(null);
						bitmap.drawLine(x, y, previousX, previousY);
					}
				} else {
					// OLD: startSound(sndSHOT);
					AudioService.instance().play(sfx);
				}

				if (step >= stepCount) {
					step = 0;
					physics.calculateTrajectory(trajectoryX, trajectoryY, stepCount);
				}

				previousX = x;
				previousY = y;

				x = trajectoryX[step];
				y = bitmap.getHeight() - trajectoryY[step];

				while (true) {
					while (step < stepCount &&
						Math.abs(trajectoryX[step] - x) < 2 &&
						Math.abs(trajectoryY[step] - (bitmap.getHeight() - y)) < 2)
						step++;

					if (step == stepCount) {
						step = 0;
						physics.calculateTrajectory(trajectoryX, trajectoryY, stepCount);
					} else
						break;
				}

				// in bounds?
				if (x >= 0 && x < bitmap.getWidth()) {
					if (y >= 0) {
						if ((point = calculateCollision(x, y)) != null) {
							initExplosion(point.x, point.y);
							return true;
						}

						drawFrame(update);
					}
					step++;
					return true;
				} else {
					return false; // missile left the screen
				}
			case EXPLOSION:
				if (explosion.drawNextFrame(update))
					return true;
				else
					state = END;
			case END:
				return false;
			default:
				System.err.println("GenericMissile: invalid state");
				return false;
		}
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;

		previousX = this.x;
		previousY = this.y;
	}

	public void setTracerColor(Color tc) {
		tracer = tc;
	}

	public Color getTracerColor() {
		return tracer;
	}

	public static void loadSounds(ScorchGame owner) {
		// OLD: sndSHOT = addSound(owner.getAudioClip(owner.getCodeBase(), "Sound/shot.au"));
		sfx = AudioService.instance().loadSFX("sound/shot.wav", false);
	}
}
