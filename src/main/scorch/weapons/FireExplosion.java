package scorch.weapons;

import scorch.Bitmap;
import scorch.ScorchGame;
import scorch.services.AudioService;

import javax.sound.sampled.Clip;
import java.awt.*;

/**
 * This class draws flames of fire calculated in real time.
 * It is one of the ways for a tank to explode.
 *
 * @author Nathan Roslavker
 */
public class FireExplosion extends Explosion {
	protected FireColorModel palette;
	protected int width;
	protected int height;
	protected int pixels = 0;
	protected float intensity = 0;
	protected int[] data;
	protected int[][] buffer;
	protected int state = 0;
	protected ExplosionInfo info;
	protected long duration = 50;

	private int frameNumber = 0;

	private static Clip sfx;

	public FireExplosion(Bitmap bitmap, int width, int height) {
		super(bitmap);
		this.width = width;
		this.height = height;
		palette = new FireColorModel(32);
		data = new int[width * height];
		buffer = new int[height][width];
		info = new ExplosionInfo();
	}

	public FireExplosion(Bitmap bitmap, int width, int height, int x, int y) {
		super(bitmap);
		this.width = width;
		this.height = height;
		setPosition(x, y);
		palette = new FireColorModel(32);
		data = new int[width * height];
		buffer = new int[height][width];
	}

	public void setArgument(int arg) {
		// ??
	}

	public void setPosition(int x, int y) {
		this.x = x - width / 2;
		this.y = y - height;
	}

	public void setDuration(long time) {
		duration = time;
	}

	public static void loadSounds(ScorchGame owner) {
		//URL url = FireExplosion.class.getResource("Sound/fire.au");
		//System.out.println(url);

		// OLD: sfx = addSound(owner.getAudioClip(owner.getCodeBase(), "Sound/fire.au"));
		sfx = AudioService.instance().loadSFX("sound/fire.wav", false);

		//sfx = addSound(owner.getAudioClip(url));
	}

	static protected long random(long min, long max) {
		return Math.round((Math.random() * (double) (max - min) + (double) min));
	}

	static protected int random(int max) {
		int returnValue;
		double rnd = Math.random();
		returnValue = (int) Math.round(rnd * ((double) max));
		if (returnValue < 0)
			System.err.println("PANIC");
		return returnValue;
	}

	public boolean drawNextFrame(boolean update) {
		pixels = 0;
		int color = 0;
		frameNumber++;

		switch (state) {
			case 0: {
				if (intensity == 0) {
					// OLD: loopSound(sndFIRE);
					AudioService.instance().loop(sfx);
				}
				// raise flame
				intensity += 0.009f;

				// maximum flame height
				if (intensity > 0.2f) {
					state = 1;
				}
			}
			break;

			case 1:
				// constant flame
				if (frameNumber >= duration) {
					state = 2;
				}
				break;

			case 2:
				intensity -= 0.007f;
				if (intensity <= 0) {
					// OLD: stopSound(sndFIRE);
					AudioService.instance().stop(sfx);
					state = 4;
					bitmap.setSandMode(true);
					bitmap.setClipping(false);
					bitmap.setDensity(1);
					bitmap.setColor(null);
					bitmap.fillEllipse(x + width / 2, y + height / 2 + 20, width / 2, 40);
					bitmap.setSandMode(false);
					return false;
				}
				break;
			default:
				return false;

		}
		try {
			for (int y = 1; y < height - 4; y += 2) {
				//char8 *pixel = pixels+ y*width;
				int pixel = pixels + y * width;

				for (int x = 0; x < width; x++) {
					//sum top pixels
					//char8 *p = pixel+(width<<1);
					int p = pixel + (width << 1);
					//int32 top= *p;
					int top = data[p];
					top += data[p - 1];
					top += data[p + 1];

					//bottom pixel
					//int32 bottom = *(pixel + (width<<2));
					int bottom = data[pixel + (width << 2)];

					//combine pixels
					int c1 = (top + bottom) >> 2;
					if (c1 > 1) c1--;

					//interpolate
					int c2 = (c1 + bottom) >> 1;

					//store pixels
					data[pixel] = (byte) c1;

					data[pixel + width] = (byte) c2;

					//next pixel
					pixel++;
				}
			}

			//setup flame generator pointer
			//char8 *generator = pixels + width*(height-4);

			int generator = pixels + width * (height - 4);

			//update flame generator
			for (int x = 0; x < width; x += 4) {
				color = random((int) (255.0f * intensity));
				data[generator] = color;
				data[generator + 1] = color;
				data[generator + 2] = color;
				data[generator + 3] = color;
				data[generator + width] = color;
				data[generator + width + 1] = color;
				data[generator + width + 2] = color;
				data[generator + width + 3] = color;
				data[generator + width * 2] = color;
				data[generator + width * 2 + 1] = color;
				data[generator + width * 2 + 2] = color;
				data[generator + width * 2 + 3] = color;
				data[generator + width * 3] = color;
				data[generator + width * 3 + 1] = color;
				data[generator + width * 3 + 2] = color;
				data[generator + width * 3 + 3] = color;

				generator += 4;
			}
		}
		catch (ArrayIndexOutOfBoundsException error) {}

		/*
		for(int i=0;i<data.length;i++)
			if(bitmap.getPixel(x+i%width,y+(int)(i/width))==Color.black.getRGB())
				data[i]=21;
		*/

		/*
		for(int i=0;i<data.length;i++)
		  buffer[(int)(i/width)][i%width]=palette.getRGB(data[i]);
		*/

		bitmap.setSandMode(true);
		bitmap.setDensity(1);
		bitmap.setColor(Color.black.getRGB());
		bitmap.setClipping(true);
		//bitmap.fillCircle(x+width/2,y+height/2,20);
		bitmap.fillEllipse(x + width / 2, y + height / 2 + 20, width / 2, 40);
		bitmap.setClipping(false);
		//bitmap.setColor(null);
		//bitmap.fillRect(x+width/2, y+height/2+20, width/2, 40);
		bitmap.drawSpriteCl(x, y, data, width, 0, palette);
		bitmap.newPixels(x, y, width, height);
		bitmap.setSandMode(false);
		return true;
	}

	public Rectangle getUpdatedArea() {
		return new Rectangle(x, y, width, height);
	}

	public ExplosionInfo getExplosionInfo() {
		ExplosionInfo ie = new ExplosionInfo();
		ie.explosionArea = new Rectangle(x, y, width, height);

		//info.center=new Point(x+width/2,y+height/2);
		//info.radius=width/2;

		return ie;
	}
}
