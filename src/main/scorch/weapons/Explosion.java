package scorch.weapons;

import scorch.Audible;
import scorch.Bitmap;
import scorch.Explodable;
import scorch.ScorchPlayer;

import java.util.Random;

/**
 * This a base class for all explosion classes.
 * It implements Animation to be able to draw itself, & communicates explosion information through ExplosionInfo.
 * Therefore, extending classes have to both implement drawNextFrame() & override getExplosionInfo().
 * @author Nathan Roslavker
*/
public abstract class Explosion extends Audible implements Explodable {
	protected Bitmap bitmap;
	protected Random random;
	protected int x, y;

	// Placeholder constructor since netscape does not allow to reflect
	// constructors with arguments. cool, eh?
	public Explosion() {
	}

	public Explosion(Bitmap bitmap, int x, int y) {
		this.bitmap = bitmap;
		setPosition(x, y);
	}

	public Explosion(Bitmap bitmap) {
		this(bitmap, 0, 0);
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/*
	public void setPosition(Point pos)
  {
		setPosition(pos.x,pos.y);
	}
	*/

	public void setRandom(Random random) {
		this.random = random;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public abstract ExplosionInfo getExplosionInfo();

	public abstract void setArgument(int arg);

	// default explosion doesn't do any damage
	public int calculateDamage(ScorchPlayer player) {
		return 0;
	}

	// TODO: make those methods abstract
	public void drawFrame(boolean update) {
	}

	public void hideFrame() {
	}
}
