package scorch.weapons;


import scorch.ScorchColorModel;

class PlasmaColorModel extends ScorchColorModel {
	public PlasmaColorModel(int bits) {
		System.out.println("PLASMA COLOR MODEL IS DISABLED. PLEASE DON'T USE IT");
		/*
		super(bits);
		byte c=(byte)255;
		byte step=(byte)16;
		for(int i=0;i<r.length;i++){
			r[i]=c;
			g[i]=0;
			b[i]=0;
			if((i%(r.length/6))==0)
				step*=-1;
			c+=step;
		}
		*/
	}

	public int getRGB(int pixel) {
		return ((255 << 24) | (r[pixel] << 16) | (g[pixel] << 8) | b[pixel]);
	}
}

