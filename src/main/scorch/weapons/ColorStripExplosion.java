package scorch.weapons;

import scorch.Bitmap;

import java.awt.*;
import java.util.Random;

/**
 * Psychedelic multicolor explosion. Used in Funky weapons.
 * @author Mikhail Kruk
 */

class ColorStripExplosion extends RoundExplosion {
	private int frameNumber = 0;
	private final int stepCount = 6;
	private int color = Color.red.getRGB();
	private final int duration = 4 * stepCount;

	private ExplosionInfo EI;

	public ColorStripExplosion(Bitmap bitmap, Random rand,
														 int x, int y, int r, int color) {
		super(bitmap);
		this.random = rand;

		setPosition(x, y);
		this.radius = r;
		this.color = color;
	}

	public void setArgument(int radiusValue) {
		radius = radiusValue;
	}

	public void drawFrame(boolean update) {
		double scale;

		for (int i = 0; i < stepCount; i++) {
			// palette scale
			scale = 1.0 / (stepCount + 1) * (1 + stepCount - ((frameNumber + i) % stepCount));
			bitmap.setColor(Bitmap.scaleColor(color, scale));
			// radius scale
			scale = 1.0 / stepCount * (stepCount - i);

			bitmap.fillCircle(x, y, (int) (radius * scale));
		}

		if (update)
			bitmap.newPixels(x - radius, y - radius, 2 * radius + 2, 2 * radius + 2);
	}

	public void hideFrame() {
		bitmap.setColor(null);
		bitmap.fillCircle(x, y, radius);
	}

	public boolean drawNextFrame(boolean update) {
		drawFrame(update);

		if (frameNumber++ >= duration) {
			bitmap.setColor(null);
			bitmap.fillCircle(x, y, radius);

			if (update) {
				bitmap.newPixels(x - radius, y - radius,
					2 * radius + 1, 2 * radius + 1);
			}

			return false;
		}

		return true;
	}
}
