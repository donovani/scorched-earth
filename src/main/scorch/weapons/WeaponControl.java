package scorch.weapons;

import scorch.ScorchGame;
import scorch.items.ItemControl;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * this class is a panel displayed in the inventory window to control the weapon.
 * For most of them (all?) it should be just "select" button
 * @author Mikhail Kruk
 */
class WeaponControl extends ItemControl implements ActionListener {
	protected int type;

	public WeaponControl(int type, ScorchGame scorchGame) {
		super(null, scorchGame);
		this.type = type;

		control = new Button("Select");
		((Button) control).addActionListener(this);

		setLayout(new FlowLayout(FlowLayout.CENTER));
		add(control);
	}

	public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand().equals("Select")) {
			owner.selectWeapon(type);
		}
	}
}
