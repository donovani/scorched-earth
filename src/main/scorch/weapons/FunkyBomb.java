package scorch.weapons;

/**
 * @author Mikhail Kruk
 */
public class FunkyBomb extends Weapon {
	public FunkyBomb() {
		type = FunkyBomb;
		price = 30000;
		argument = 6;
		explosionClass = "FunkyExplosion";
	}
}
