package scorch.weapons;

/**
 * @author Mikhail Kruk
 */
public class HeavyDigger extends Weapon {
	public HeavyDigger() {
		type = HeavyDigger;
		price = 6000;
		argument = DiggerExplosion.HEAVY_DIGGER;
		explosionClass = "DiggerExplosion";
	}
}
