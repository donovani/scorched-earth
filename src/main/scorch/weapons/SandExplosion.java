package scorch.weapons;

import scorch.Bitmap;

import java.awt.*;

/**
 * Lots of sand goes up in the air and then falls on the ground
 * @author Mikhail Kruk
 */
public class SandExplosion extends Explosion {
	public static int MAX_HEIGHT = 250;

	private int sand = -1; // sand color
	private int currentSize = 3; // line length
	private int size = MAX_HEIGHT;  // max height of explosion
	private int upperY; //increasing height(decreasing upperY)
	private int stepSize = 7; // lines per frame
	private int lineCount = 0; // number of lines drawn (<=size)

	// empty constructor. thanks, netscape
	public SandExplosion() {
	}

	public SandExplosion(Bitmap bitmap) {
		super(bitmap);
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
		upperY = y;
	}

	public void setArgument(int arg) {
		size = arg;
	}

	public void setStepSize(int stepSize) {
		this.stepSize = stepSize;
	}

	/*
		Draw constantly increasing in length
		horizontal lines with low density on top of each other.
		This creates an illusion of sand being scattered by an
		explosion
	*/
	public boolean drawNextFrame(boolean update) {
		if (sand == -1) sand = bitmap.getSandColor();

		for (int i = 0; i <= stepSize && upperY >= 0 && lineCount < size; i++) {
			bitmap.setDensity(0.2f);
			bitmap.setColor(sand);

			bitmap.drawLine(x - currentSize / 2, upperY, x + currentSize / 2, upperY);

			lineCount++;
			upperY--;
			currentSize++;
			bitmap.setDensity(1);

		}
		if (upperY >= 0 && lineCount < size) {
			bitmap.newPixels(x - currentSize / 2 - 2, upperY - 1,
				currentSize + 2, stepSize + 2);
			return true;
		} else
			return false;
	}

	public ExplosionInfo getExplosionInfo() {
		ExplosionInfo explosionInfo = new ExplosionInfo();
		explosionInfo.explosionArea =
			new Rectangle(x - currentSize / 2 - 1, upperY + 1, currentSize, y - upperY);

		return explosionInfo;
	}
}
