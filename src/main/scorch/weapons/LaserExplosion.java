package scorch.weapons;

import scorch.Bitmap;

/**
 * This explosion animates a laser beam going from the place of explosion all the way to the top of the screen.
 * The diameter width of the beam can be specified in the constructor.
 * @author Nathan Roslavker
 */
public class LaserExplosion extends Explosion {
	int width;
	LaserColorModel palette;
	long duration = 60;
	int upperY;
	final static int START = 0;
	final static int LASER = 1;
	final static int END = 2;
	int state = START;
	private int frameNumber = 0;

	//here width means the width of the laser beam
	public LaserExplosion(Bitmap bitmap, int width) {
		super(bitmap);
		this.width = width;
		palette = new LaserColorModel(32);
	}

	public void setArgument(int arg) {
		width = arg;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y + 5;
		upperY = y;
	}

	public ExplosionInfo getExplosionInfo() {
		return null;
	}

	int offset = 0;

	public boolean drawNextFrame(boolean update) {
		boolean returnValue;
		frameNumber++;

		switch (state) {
			case (START):
				state = LASER;

			case (LASER):
				for (int i = width / 2; i >= 0; i--) {
					bitmap.setColor(palette.getRGB(((32 - i) + offset) % 32));
					bitmap.drawLine(x + i, upperY, x + i, y);
					bitmap.drawLine(x - i, upperY, x - i, y);
				}
				bitmap.newPixels(x - width / 2, 0, width + 1, y + 1);
				offset++;
				if (upperY > 0) {
					upperY = upperY - 7;
					returnValue = true;
				} else {
					if (frameNumber < duration)
						returnValue = true;
					else {
						bitmap.setColor(null);
						bitmap.setDensity(1);
						bitmap.fillRect(x - width / 2, 0, width + 1, y + 1);
						bitmap.newPixels(x - width / 2, 0, width + 1, y + 1);

						returnValue = false;
						state = END;
					}
				}
				break;
			default:
				returnValue = false;
		}
		return returnValue;
	}
}


