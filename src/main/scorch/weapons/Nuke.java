package scorch.weapons;

/**
 * @author Mikhail Kruk
 */
public class Nuke extends Weapon {
	public Nuke() {
		type = Nuke;
		price = 40000;
		argument = SimpleExplosion.NUKE;
		explosionClass = "SimpleExplosion";
	}
}
