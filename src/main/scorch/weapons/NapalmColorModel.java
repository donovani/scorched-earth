package scorch.weapons;

import scorch.ScorchColorModel;

import java.awt.*;

/**
 * Color palette used in animating Napalm fire.
 * Translates index-based colors into RGB values.
 * @author Alexander Rasin
 */
class NapalmColorModel extends ScorchColorModel {
	//colors in the palette, positions of the colors from 0 to 255
	//fill in the arrays with smooth transition between them.
	private final static int blackRange = 5;
	private final static int blueRange = 12;
	private final static int redRange = 25;
	private final static int orangeRange = 40;
	private final static int yellowRange = 70;
	private final static int whiteRange = 110;

	public NapalmColorModel() {
		super();

		createGradient(
			0,
			Color.black.getRed(),
			Color.black.getGreen(),
			Color.black.getBlue(), blackRange,
			Color.black.getRed(),
			Color.black.getGreen(),
			Color.black.getBlue()
		);
		createGradient(
			blackRange,
			Color.black.getRed(),
			Color.black.getGreen(),
			Color.black.getBlue(), blueRange,
			Color.blue.getRed(),
			Color.blue.getGreen(),
			Color.blue.getBlue() / 7
		);
		createGradient(
			blueRange,
			Color.blue.getRed(),
			Color.blue.getGreen(),
			Color.blue.getBlue() / 7,
			redRange,
			Color.red.getRed(),
			Color.red.getGreen(),
			Color.red.getBlue()
		);
		createGradient(
			redRange,
			Color.red.getRed(),
			Color.red.getGreen(),
			Color.red.getBlue(),
			orangeRange,
			Color.orange.getRed(),
			Color.orange.getGreen(),
			Color.orange.getBlue()
		);
		createGradient(
			orangeRange,
			Color.orange.getRed(),
			Color.orange.getGreen(),
			Color.orange.getBlue(),
			yellowRange,
			Color.yellow.getRed(),
			Color.yellow.getGreen(),
			Color.yellow.getBlue()
		);
		createGradient(
			yellowRange,
			Color.yellow.getRed(),
			Color.yellow.getGreen(),
			Color.yellow.getBlue(),
			whiteRange,
			Color.white.getRed(),
			Color.white.getGreen(),
			Color.white.getBlue()
		);
	}

	private void createGradient(
		int startIndex, int startRed, int startGreen, int startBlue,
		int endIndex, int endRed, int endGreen, int endBlue
	) {
		//Produces smooth gradients from RGB_start to RGB_end
		//(on the variable's names s = start and e = end
		int index, max = endIndex - startIndex;

		//Set the two starting values
		r[startIndex] = startRed;
		g[startIndex] = startGreen;
		b[startIndex] = startBlue;
		r[endIndex] = endRed;
		g[endIndex] = endGreen;
		b[endIndex] = endBlue;

		//Compute the RGB increments
		float redIncrement, greenIncrement, blueIncrement;
		redIncrement = (endRed - startRed) / ((float) (max));
		greenIncrement = (endGreen - startGreen) / ((float) (max));
		blueIncrement = (endBlue - startBlue) / ((float) (max));

		//Set middle colors
		for (index = 1; index < max; index++) {
			r[(startIndex + index)] = (byte) (startRed + redIncrement * index);
			g[(startIndex + index)] = (byte) (startGreen + greenIncrement * index);
			b[(startIndex + index)] = (byte) (startBlue + blueIncrement * index);
		}
	}

	public int getRGB(int pixel) {
		return ((255 << 24) | (r[(pixel >> 16) & 255] << 16) | (g[(pixel >> 8) & 255] << 8) | (b[pixel & 255]));
	}
}
