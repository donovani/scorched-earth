package scorch.weapons;

import scorch.Bitmap;

import java.awt.*;

/**
 * Currently unused...
 * @author Mikhail Kruk
 */
public class GradientExplosion extends RoundExplosion {
	private int frameNumber = 0;

	// fake constructor for netscape
	public GradientExplosion() {
	}

	public GradientExplosion(Bitmap bitmap, int radius) {
		super(bitmap);
		this.radius = radius;
	}

	public void setArgument(int arg) {
		radius = arg;
	}

	public void drawFrame(boolean update) {
		int i = 0;

		while ((i++ < 2) && (++frameNumber < radius)) {
			bitmap.setColor(Color.red);
			bitmap.fillGradientCircle(x, y, frameNumber);
		}

		if (update)
			bitmap.newPixels(x - radius, y - radius, 2 * radius + 1, 2 * radius + 1);
	}

	public boolean drawNextFrame(boolean update) {
		drawFrame(update);

		if (frameNumber >= radius) {
			bitmap.setColor(null);
			bitmap.fillCircle(x, y, radius);
			if (update) {
				bitmap.newPixels(x - radius, y - radius,
					2 * radius + 1, 2 * radius + 1);
			}
			return false;
		} else
			return true;
	}
}
