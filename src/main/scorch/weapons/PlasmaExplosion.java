package scorch.weapons;

import scorch.Bitmap;

import java.awt.*;

/**
 * Non-functional plasma weapon.
 * FIXME: Look into slowdowns & commented out code here
 * @author Nathan Roslavker
 */
public class PlasmaExplosion extends RoundExplosion {

	final static int DEF_RADIUS = 20;
	final static long DEF_DURATION = 45;

	private int frameNumber = 0;

	static PlasmaColorModel palette = new PlasmaColorModel(32);

	int width;
	int height;
	int radius;

	int[][] data;

	long duration;

	// TODO: Figure out why these are out of a method
	int[] xValues;
	int toAdd = 0;
	int yAnswer;
	int xAnswer;
	boolean first = true;
	int counter = radius / 10;
	int step = 2;

	static int[] sineArray;

	static {
		sineArray = new int[360];
		for (int x = 0; x < sineArray.length; x++)
			sineArray[x] = (int) (Math.sin(rad(x)) * 1024);
	}

	static double rad(double angle) {
		return angle * Math.PI / 180.;
	}

	public PlasmaExplosion(Bitmap bitmap, int x, int y, int radius, long duration) {
		System.out.println("PLASMA EXPLOSION IS DISABLED. PLEASE DO NOT USE IT");
	/*
	super(bitmap,x,y);
	this.radius=radius;
	width=radius*2;
	height=radius*2;
	data=new int [height][width];
	XValues=new int[width];
	this.duration=duration;*/
	}

	public void setArgument(int arg) {
		// ???
	}

	public PlasmaExplosion(Bitmap bitmap, int x, int y) {
		this(bitmap, x, y, DEF_RADIUS, DEF_DURATION);
	}

	public ExplosionInfo getExplosionInfo() {
		ExplosionInfo explosionInfo = new ExplosionInfo();
		// FIXME: This part was not fully implemented
		//explosionInfo.center=new Point(x+radius, y+radius);
		//explosionInfo.radius=radius;
		explosionInfo.explosionArea = new Rectangle(x, y, width, height);
		return explosionInfo;
	}

	public boolean drawNextFrame() {

		frameNumber++;
		//	bitmap.setColor(b);
		//bitmap.setDensity(0.5f);
		if (first) {
			first = false;
		}

		if (frameNumber < duration) {
			toAdd++;

			for (int x = 0; x < width; x++) {

				xValues[x] = 20 * sineArray[((x << 2) + (toAdd)) % 360] +
					30 * sineArray[((x) + (toAdd << 14)) % 360] +
					50 * sineArray[((x >> 2) + (toAdd >> 4)) % 360];
			}

			for (int y = 0; y < height; y++) {

				yAnswer = 40 * sineArray[((y << 3) + (toAdd)) % 360] +
					40 * sineArray[((y) + (toAdd << 14)) % 360] +
					20 * sineArray[((y) + (toAdd >> 6)) % 360];
				for (int x = 0; x < xValues.length; x++) {
					xAnswer = xValues[x];
					data[y][x] = Math.abs(((xAnswer + yAnswer) >> 10));
					//System.out.println(data[y][x]);
					//bitmap.drawCircle(50,50,y);
				}
			}
			bitmap.setColor(null);
			bitmap.setClipping(true);
			bitmap.fillCircle(this.x + radius, this.y + radius, counter);
			bitmap.setClipping(false);
			bitmap.drawSpriteCl(this.x, this.y, data, 0, palette);
			bitmap.newPixels(this.x, this.y, width + 1, height + 1);
			if (counter < radius)
				counter += step;
			return true;
		} else {
			bitmap.setDensity(1);
			bitmap.setColor(null);
			bitmap.fillCircle(this.x + radius, this.y + radius, radius);
			bitmap.newPixels(this.x, this.y, width + 1, height + 1);
			return false;
		}
	}

	public void setPosition(int x, int y) {
		this.x = x - width / 2;
		this.y = y - height / 2;
	}

	@Override
	public boolean drawNextFrame(boolean update) {
		// FIXME: Figure out what's up with this class
		return false;
	}
}
