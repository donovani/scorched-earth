package scorch.weapons;

import scorch.Bitmap;

import java.util.Vector;

/**
 * @author Alexander Rasin
 */
class NapalmLine {
	Bitmap bitmap = null;
	Vector pointPairs = new Vector();
	private int lm;
	private int rm;
	private final int y;

	public NapalmLine(int x, int yCoordinate, Bitmap bitmap) {
		int[] pair = new int[2];
		this.y = yCoordinate;
		this.bitmap = bitmap;
		pair[0] = pair[1] = lm = rm = x;
		pointPairs.addElement(pair);
		//DEBUG
		//bmp.setColor( new Color (254, 254, 0));
		this.bitmap.setPixel(x, y);
		this.bitmap.newPixels(x, y, 1, 1);
	}


	public int expand() {
		int[] pair;
		int pixels, expansion = 0, count;

		//check if 'liquid' can flow to a lower level at one of the boundaries
		for (int i = 0; i < pointPairs.size(); i++) {
			pair = (int[]) pointPairs.elementAt(i);
			if (bitmap.isBackground(pair[0], y + 1))
				return -pair[0];
			if (bitmap.isBackground(pair[1], y + 1))
				return -pair[1];
		}
		count = 0;
		pixels = 0;
		//while no pixels could be expanded (don't want to leave this method
		//without extending any liquid - animation will stop. Keep trying all
		//the possibilities on the same level
		while ((pixels == 0) && (count < pointPairs.size())) {
			pair = (int[]) pointPairs.elementAt(expansion);//i
			expansion = (++expansion) % pointPairs.size();
			count++;
			if (bitmap.isBackground(pair[0] - 1, y)) {
				bitmap.setPixel(--pair[0], y);
				bitmap.newPixels(pair[0], y, 1, 1);
				pixels++;
			}
			if (bitmap.isBackground(pair[1] + 1, y)) {
				bitmap.setPixel(++pair[1], y);
				bitmap.newPixels(pair[1], y, 1, 1);
				pixels++;
			}
			checkBoundary(pair);
		}

		return pixels;
	}

	public void newPt(int x) {
		int[] pair = new int[2];
		pair[0] = pair[1] = x;
		//DEBUG
		//bmp.setColor( new Color( 254, 254, 0 ));
		bitmap.setPixel(x, y);
		bitmap.newPixels(x, y, 1, 1);
		checkBoundary(pair);

		pointPairs.addElement(pair);
	}

	// returns the left and right boundaries of the napalm on this line
	public int[] clearNapalm() {
		int[] pair = null;
		for (int i = 0; i < pointPairs.size(); i++) {
			pair = (int[]) pointPairs.elementAt(i);
			bitmap.drawLine(pair[0], y, pair[1], y);
		}
		pair[0] = lm;
		pair[1] = rm;
		return pair;
	}

	private void checkBoundary(int[] pair) {
		if (pair[0] < lm) lm = pair[0];
		if (pair[1] > rm) rm = pair[1];
	}
}
