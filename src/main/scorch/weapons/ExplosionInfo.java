package scorch.weapons;

import java.awt.*;

/**
 * Class to communicate the explosion information from children
 * of GenericMissile and Explosion to the ScorchField.
 * @author Nathan Roslavker
 * @author Mikhail Kruk
 */
public class ExplosionInfo {
	// The area that was redrawn by the explosion
	public Rectangle explosionArea;

	public ExplosionInfo() {
	}

	public ExplosionInfo(Rectangle rect) {
		explosionArea = rect;
	}

	public String toString() {
		return "ExplosionInfo: rectangle: " + explosionArea;
	}
}
