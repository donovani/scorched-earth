package scorch.weapons;

import scorch.ScorchColorModel;

/**
 * Color palette used in animating the laser beam
 * @author Nathan Roslavker
 */
class LaserColorModel extends ScorchColorModel {
	protected int[] r = new int[32];
	protected int[] g = new int[32];
	protected int[] b = new int[32];

	public LaserColorModel(int size) {
		super();
		int c = 255;
		int step = 16;
		for (int i = 0; i < r.length; i++) {
			r[i] = c;
			g[i] = c;
			b[i] = 255;
			if ((i % (r.length / 4)) == 0)
				step *= -1;
			c += step;
		}
	}

	public int getRGB(int pixel) {
		if (pixel < r.length)
			return ((255 << 24) | (r[pixel] << 16) | (g[pixel] << 8) | b[pixel]);
		else
			return ((255 << 24) | (r[r.length - 1] << 16) | (g[g.length - 1] << 8) | b[b.length - 1]);
	}
}
