package scorch.weapons;

import scorch.ScorchColorModel;

/**
 * Color palette used in animating fire.
 * Translates index-based colors into RGB values.
 *
 * @author Nathan Roslavker
 */
class FireColorModel extends ScorchColorModel {
	public FireColorModel(int bits) {
		super();
		int index = 0;
		byte channel = 0;

		while (index < 64) {
			//data[index] = pack(channel,0,0);
			r[index] = channel;
			g[index] = 0;
			b[index] = 0;
			channel += 4;
			index++;
		}

		// red to yellow
		channel = 0;
		while (index < 128) {
			//data[index] = pack(255,channel,0);
			r[index] = (byte) 255;
			g[index] = channel;
			b[index] = 0;
			channel += 4;
			index++;
		}

		// yellow to white
		channel = 0;
		while (index < 192) {
			//data[index] = pack(255,255,channel);
			r[index] = (byte) 255;
			g[index] = (byte) 255;
			b[index] = channel;
			channel += 4;
			index++;
		}

		// white
		while (index < 256) {
			//data[index] = pack(255,255,255);
			r[index] = b[index] = g[index] = (byte) 255;
			index++;
		}
	}

	public int getRGB(int pixel) {
		if (pixel < 20)
			return 0;
		else
			return ((255 << 24) | (r[pixel] << 16) | (g[pixel] << 8) | b[pixel]);
	}
}
