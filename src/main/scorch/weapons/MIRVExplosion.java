package scorch.weapons;

import scorch.Bitmap;
import scorch.Physics;

import java.util.Random;

/**
 * Explosion for the MIRV weapon
 * @author Mikhail Kruk
 */
public class MIRVExplosion extends ParticlesExplosion implements Directional {
	public final static int MIRV = 25, DEATH_HEAD = 55, offset = 5;
	private double speed;


	public MIRVExplosion() {
		delayExplosions = false;
	}

	public MIRVExplosion(Bitmap bitmap, Random random, int n) {
		super(bitmap);
		setRandom(random);
		setArgument(n);
		delayExplosions = false;
	}

	public void setArgument(int radius) {
		super.setArgument(radius == MIRV ? 5 : 9);
		this.radius = radius;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	protected void initParticles() {
		double power = speed - offset * explosionCount / 2;
		GenericMissile missile;

		for (int i = 0; i < explosionCount; i++) {
			missile = new RoundMissile(
				bitmap,
				new Physics(
					x,
					bitmap.getHeight() - y,
					0,
					power
				),
				new SimpleExplosion(bitmap, radius)
			);
			particles.addElement(missile);
			power += offset;
		}
	}
}
