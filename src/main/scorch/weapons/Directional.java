package scorch.weapons;

/**
 * An explosion which depends on the direction in which the missile hit the ground should implement this.
 * @author Mikhail Kruk
 */
public interface Directional {
	void setSpeed(double speed);
}
