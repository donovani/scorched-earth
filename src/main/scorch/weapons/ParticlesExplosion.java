package scorch.weapons;

import scorch.Bitmap;
import scorch.Explodable;
import scorch.ScorchPlayer;

import java.awt.*;
import java.util.Random;
import java.util.Vector;

/**
 * The super class of all explosions with multi-play exploding particles (Funky Bomb, MIRV)
 * @author Mikhail Kruk
 */
public abstract class ParticlesExplosion extends Explosion {
	protected int explosionCount, radius;

	private static final int START = 0, FLYING = 1, EXPLODING = 2, END = 3;

	private int state = START;
	protected Vector particles;
	protected Vector explosionParticles;
	protected boolean delayExplosions = true;
	private ExplosionInfo info;
	private int minX = 0;
	private int minY = 0;
	private int maxX = Integer.MAX_VALUE;
	private int maxY = Integer.MAX_VALUE;

	public ParticlesExplosion() {
	}

	public ParticlesExplosion(Bitmap bitmap) {
		super(bitmap);
	}

	public ParticlesExplosion(Bitmap bitmap, Random random, int explosionCount) {
		super(bitmap);
		setRandom(random);
		setArgument(explosionCount);
	}

	public void setArgument(int arg) {
		explosionCount = arg;
		particles = new Vector(explosionCount);
		explosionParticles = new Vector(explosionCount);
	}

	protected abstract void initParticles();

	public boolean drawNextFrame(boolean update) {
		boolean res = false;
		GenericMissile missile;

		switch (state) {
			case START:
				initParticles();
				state = FLYING;
				res = true;
				break;
			case FLYING:
				// res is used here to tell if all the particles have reached
				// the exploding state yet (out of screen is good too)
				res = false;
				int j = 0;
				while (j < particles.size()) {
					missile = (GenericMissile) particles.elementAt(j);
					if (!missile.isExploding()) {
						if (missile.drawNextFrame(true)) {
							res = true;
							j++;
						} else
							particles.removeElement(missile);
					} else {
						if (missile.getX() < maxX) maxX = missile.getX();
						if (missile.getX() > minX) minX = missile.getX();
						if (missile.getY() < maxY) maxY = missile.getY();
						if (missile.getY() > minY) minY = missile.getY();

						particles.removeElement(missile);
						explosionParticles.addElement(missile);

						if (delayExplosions) updateExploding();
					}
				}

				if (!delayExplosions) updateExploding();

				if (!res) {
					particles = explosionParticles;

					state = EXPLODING;
					// take the size of explosions into account
					maxX -= radius;
					maxY -= radius;
					minX += radius;
					minY += radius;
				}
				res = true;
				break;
			case EXPLODING:
				res = false;
				for (int i = 0; i < particles.size(); i++) {
					missile = (GenericMissile) particles.elementAt(i);
					res |= missile.drawNextFrame(false);
				}
				bitmap.newPixels(
					maxX,
					maxY,
					minX - maxX + 1, minY - maxY + 1);
				if (!res) state = END;
				break;
			case END:
				res = false;
				break;
			default:
				System.err.println("ParticlesExplosion: illegal state");
		}

		return res;
	}

	private void updateExploding() {
		for (int i = 0; i < explosionParticles.size(); i++) {
			if (delayExplosions)
				((GenericMissile) explosionParticles.elementAt(i)).drawFrame(false);
			else
				((GenericMissile) explosionParticles.elementAt(i)).drawNextFrame(false);

		}

		bitmap.newPixels(
			maxX - radius,
			maxY - radius,
			minX - maxX + 1 + 2 * radius,
			minY - maxY + 1 + 2 * radius
		);

		for (int i = 0; i < explosionParticles.size(); i++)
			((GenericMissile) explosionParticles.elementAt(i)).hideFrame();
	}

	public int calculateDamage(ScorchPlayer player) {
		int damage = 0;
		for (int i = 0; i < particles.size(); i++)
			damage += ((Explodable) particles.elementAt(i)).calculateDamage(player);
		return damage;
	}

	// TODO: Do we need those calculations?
	public ExplosionInfo getExplosionInfo() {
		boolean def = false;

		// we need to recalculate boundaries to disregard the particles which explode offscreen
		int maxX = Integer.MAX_VALUE, maxY = Integer.MAX_VALUE, minX = 0, minY = 0;

		for (int i = 0; i < particles.size(); i++) {
			info = ((Explodable) particles.elementAt(i)).getExplosionInfo();

			if (info == null || info.explosionArea == null) continue;
			def = true;

			if (info.explosionArea.x < maxX) maxX = info.explosionArea.x;
			if (info.explosionArea.y < maxY) maxY = info.explosionArea.y;
			if (info.explosionArea.width + info.explosionArea.x > minX)
				minX = info.explosionArea.width + info.explosionArea.x;
			if (info.explosionArea.height + info.explosionArea.y > minY)
				minY = info.explosionArea.height + info.explosionArea.y;
		}

		if (def)
			info = new ExplosionInfo(new Rectangle(maxX, maxY, minX - maxX, minY - maxY));
		else
			info = null;

		return info;
	}
}
