package scorch.weapons;

/**
 * The simple roller weapon
 * @author Mikhail Kruk
 */
public class Roller extends Weapon {
	public Roller() {
		type = Roller;
		price = 13000;
		argument = RollerExplosion.ROLLER;
		explosionClass = "RollerExplosion";
	}
}
