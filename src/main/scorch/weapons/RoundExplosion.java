package scorch.weapons;

import scorch.Bitmap;
import scorch.ScorchPlayer;

import java.awt.*;

/**
 * This class adds the standard calcDamage() functionality to all the round explosions.
 * Damage is calculated using the distance of a player from the center of explosion and its radius.
 * If we make explosions of different power, a variable coefficient can be added to this class,
 * and included in the calculations.
 * @author Mikhail Kruk
 */
public abstract class RoundExplosion extends Explosion {
	protected int radius;
	protected ExplosionInfo info;

	public RoundExplosion() {
		super();
	}

	public RoundExplosion(Bitmap bitmap) {
		super(bitmap);
	}

	public RoundExplosion(Bitmap bitmap, int x, int y) {
		super(bitmap, x, y);
	}


	private double distance(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

	public int calculateDamage(ScorchPlayer player) {
		int lx = player.getX(), rx = lx + player.getWidth(),
			uy = player.getTurretY(1), ly = player.getY() + player.getHeight();
		double d1 = distance(lx, uy, x, y), d2 = distance(lx, ly, x, y),
			d3 = distance(rx, uy, x, y), d4 = distance(rx, ly, x, y);
		double distance = Math.min(Math.min(d1, d2), Math.min(d3, d4));
		double splash_radius = 1.02 * (double) radius; // add splash damage

		// If a direct hit, set power to 0
		if (x >= lx && x <= rx && y >= uy && y <= ly) {
			//System.err.println(player.getName()+": direct hit!");
			// Take absolute value just in case it's already negative...
			return Math.abs(player.getPowerLimit());
		}

		// maybe (?) multiply by 2 because there is actually some damage
		// on the edge of explosion. may be it should depend on weapon?
		if (splash_radius > distance)
			return (int) (ScorchPlayer.MAX_POWER - (ScorchPlayer.MAX_POWER * distance) / splash_radius);
		else
			return 0;
	}


	public ExplosionInfo getExplosionInfo() {
		if (info == null) {
			info = new ExplosionInfo();
			info.explosionArea = new Rectangle(x - radius, y - radius, radius * 2, radius * 2);
		}
		return info;
	}
}
