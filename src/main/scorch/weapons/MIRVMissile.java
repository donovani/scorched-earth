package scorch.weapons;

import scorch.Bitmap;
import scorch.Physics;

/**
 * A missile which explodes at the peak of its trajectory, and
 * creates a number of particles which fall on the ground & explode.
 * @author Mikhail Kruk
 */
public class MIRVMissile extends GenericMissile {
	// Missile sprite
	private final static int[][] data =
		{
			{0, b, b, 0},
			{b, b, w, b},
			{w, b, w, w},
			{0, w, w, 0},
		};

	public MIRVMissile(Bitmap bitmap, Physics physics, Explosion explosion) {
		super(bitmap, physics, data, explosion);
	}

	public boolean drawNextFrame(boolean update) {
		boolean res = super.drawNextFrame(update);
		if ((state == MISSILE) && (step > 2 && trajectoryY[step - 1] < trajectoryY[step - 2])) {
			initExplosion(trajectoryX[step - 2], bitmap.getHeight() - trajectoryY[step - 2]);
			return true;
		} else
			return res;
	}
}

