package scorch.weapons;

import scorch.Bitmap;
import scorch.Physics;

/**
 * Missile which leaves a trace.
 * The tracer is actually built into the generic missile, so this class just enables it.
 * @author Mikhail Kruk
 */
public class TracerMissile extends GenericMissile {
	private static final int[][] funkyParticle = {{w}}; // fake missile image

	public TracerMissile(Bitmap bitmap, Physics physics, Explosion explosion) {
		super(bitmap, physics, funkyParticle, explosion);
		tracer = java.awt.Color.yellow;
	}
}
