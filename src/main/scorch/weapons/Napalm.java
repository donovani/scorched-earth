package scorch.weapons;

/**
 * @author Mikhail Kruk
 */
public class Napalm extends Weapon {
	public Napalm() {
		type = Napalm;
		price = 10000;
		argument = NapalmExplosion.NAPALM;
		explosionClass = "NapalmExplosion";
	}
}
