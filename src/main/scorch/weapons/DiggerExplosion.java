package scorch.weapons;

import scorch.Bitmap;

import java.awt.*;
import java.util.Random;

/**
 * The weapon which digs through the ground
 * @author Mikhail Kruk
 */
public class DiggerExplosion extends Explosion {
	public static final int BABY_DIGGER = 1000,
		DIGGER = 3000,
		HEAVY_DIGGER = 5000;

	private static final int b = Color.black.getRGB(), w = Color.white.getRGB();

	private static final int size = 1;
	private static final int count = 10;

	private int frameNumber = 0;
	private int maxX = Integer.MAX_VALUE, minX = 0,
		maxY = Integer.MAX_VALUE, minY = 0;

	private final int[] xCoordinates = new int[count];
	private final int[] yCoordinates = new int[count];
	private ExplosionInfo info;
	private int duration;

	// fake constructor for netscape
	public DiggerExplosion() {
	}

	public DiggerExplosion(Bitmap bitmap, Random random, int x, int y, int duration) {
		super(bitmap);
		this.random = random;

		setPosition(x, y);
		this.duration = duration;
	}

	public void setArgument(int durationValue) {
		duration = durationValue;
	}

	public void setPosition(int x, int y) {
		super.setPosition(x, y);
		if (xCoordinates != null && yCoordinates != null) {
			for (int i = 0; i < count; i++) {
				xCoordinates[i] = x;
				yCoordinates[i] = y;
			}
		}
	}

	public boolean drawNextFrame(boolean update) {
		int d;

		while (frameNumber < duration) {
			for (int i = 0; i < count; i++) {
				d = Math.abs(random.nextInt() % 4);
				switch (d) {
					case 0:
						if (//frameNum > duration / 2 ||
							bitmap.isGround(xCoordinates[i] + size + 1, yCoordinates[i])) {
							xCoordinates[i] += size;
							frameNumber++;
						}
						break;
					case 1:
						if (//frameNum > duration / 2 ||
							bitmap.isGround(xCoordinates[i] - size - 1, yCoordinates[i])) {
							xCoordinates[i] -= size;
							frameNumber++;
						}
						break;
					case 2:
						if (//frameNum > duration / 2 ||
							bitmap.isGround(xCoordinates[i],yCoordinates[i] - size - 1)) {
							yCoordinates[i] -= size;
							frameNumber++;
						}
						break;
					case 3:
						yCoordinates[i] += size;
						frameNumber++;
						break;
					default:
						System.err.println("Digger.drawNextFrame(): bad direction");
						frameNumber = duration;
				}
				if (xCoordinates[i] > minX) minX = xCoordinates[i];
				if (xCoordinates[i] < maxX) maxX = xCoordinates[i];
				if (yCoordinates[i] > minY) minY = yCoordinates[i];
				if (yCoordinates[i] < maxY) maxY = yCoordinates[i];
				drawDigger(xCoordinates[i], yCoordinates[i]);
			}
		}

		info = new ExplosionInfo(new Rectangle(
			maxX - size,
			maxY - size,
			minX - maxX + 2 * size,
			minY - maxY + 2 * size
		));
		bitmap.newPixels(
			maxX - size,
			maxY - size,
			minX - maxX + 2 * size,
			minY - maxY + 2 * size
		);
		return false;
	}

	private void drawDigger(int x, int y) {
		bitmap.setColor(null);
		bitmap.setPixel(x, y);
	}

	public ExplosionInfo getExplosionInfo() {
		return info;
	}
}
