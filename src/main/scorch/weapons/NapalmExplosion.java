package scorch.weapons;

import scorch.Bitmap;
import scorch.ScorchPlayer;

import java.awt.*;
import java.util.Random;

/**
 * Spreads a limited amount of flammable substance over the surface of the playing field.
 * Then this substance (napalm) burns for a short period of time.
 * @author Alexander Rasin
 */
public class NapalmExplosion extends Explosion {
	private final int LIQUID = 0;
	private final int FIRE = 1;
	private final int CLEAR = 2;

	private final int black = Color.black.getRGB();
	private final int white = Color.white.getRGB();

	private final int screen = 2;

	public final static int NAPALM_COLOR = new Color(200, 200, 0).getRGB();
	public final static int NAPALM = 140;
	public final static int NAPALM_DURATION = 120;

	public final static int HOT_NAPALM_COLOR = new Color(254, 254, 0).getRGB();
	public final static int HOT_NAPALM = 280;
	public final static int HOT_NAPALM_DURATION = 210;

	private int color = HOT_NAPALM_COLOR; //napalm color
	private int type = HOT_NAPALM; //napalm type
	private int state = LIQUID;
	private boolean first = true;
	private int pixelNum = HOT_NAPALM;
	private int fireDuration = HOT_NAPALM_DURATION;
	private final int burnoutDuration = 70;
	private int fireHeight = 150;

	//current level of napalm that is being expanded
	private int currentLevel = -1;

	// two frames for the fire animation and a filter containing a screenshot of the napalm location
	private int[][][] frames;
	private int[] fireLevels;
	private int width, height, currentFrame = 0;

	private NapalmLine[] napalmLines;
	private NapalmColorModel colorModel;

	private int minY, //upper-most y
		maxY, //lower-most y
		maxX, //right-most x
		minX; //left-most x


	public NapalmExplosion() {
	}

	public NapalmExplosion(Bitmap bitmap, Random random, int color) {
		super(bitmap, 0, 0);
		//DEBUG
		setArgument(color);
		this.random = random;
	}

	//implement Explodable
	public ExplosionInfo getExplosionInfo() {
		ExplosionInfo explosionInfo = new ExplosionInfo();

		//extra boundaries in case sand cleared out on the border...
		explosionInfo.explosionArea = new Rectangle(minX - 1,
			maxX - minX + 2,
			minY - 1,
			maxY - minY + 2);

		return explosionInfo;
	}

	public void setArgument(int type) {
		this.type = type;
	}

	public boolean drawNextFrame(boolean update) {
		int pixelsUsed = 0;

		if (first) {
			if (type == NAPALM) {
				color = NAPALM_COLOR;
				pixelNum = NAPALM;
				fireDuration = NAPALM_DURATION;
			}
			if (type == HOT_NAPALM) {
				color = HOT_NAPALM_COLOR;
				pixelNum = HOT_NAPALM;
				fireDuration = HOT_NAPALM_DURATION;
			}

			//Debug.startTimer();
		}

		//Debug.calcFPSRate( );
		bitmap.setColor(color);

		switch (state) {
			case LIQUID:
				if (first) {
					napalmLines = new NapalmLine[bitmap.getHeight()];
					for (int i = 0; i < bitmap.getHeight(); i++)
						napalmLines[i] = null;

					minY = -1;                   //upper-most y
					maxY = -1;                   //lower-most y
					maxX = -1;                   //right-most x
					minX = bitmap.getWidth() + 1;//left-most x
					currentLevel = y;
					first = false;
					//in case the napalm falls on the floor, it has
					//a tendency to start 1 below the floor...
					if (currentLevel == bitmap.getHeight())
						currentLevel--;
					napalmLines[currentLevel] =
						new NapalmLine(x, currentLevel, bitmap);
					pixelNum--;

					//napalm can go in the sand diagonally preventing
					//it from expanding.  clear out sand up,left,right
					bitmap.setColor(null);
					bitmap.setPixel(x - 1, currentLevel);
					bitmap.setPixel(x + 1, currentLevel);
					bitmap.setPixel(x, currentLevel - 1);
					bitmap.setColor(color);
				}
				//speed up the expansion.
				for (int i = 0; i < 4 && pixelNum > 0; i++) {
					pixelsUsed = napalmLines[currentLevel].expand();

					//going up
					if (pixelsUsed == 0) {
						currentLevel--;
						if (napalmLines[currentLevel] == null) {
							if (canFill(x, currentLevel)) {
								napalmLines[currentLevel] = new NapalmLine(x, currentLevel, bitmap);
								pixelNum--;
							}
							//can't go up
							else {
								fireDuration = fireDuration / 10;
								pixelNum = 0;
							}
						}
					}
					//going down, pixelsUsed is the x location.
					else if (pixelsUsed < 0) {
						currentLevel++;
						if (napalmLines[currentLevel] == null)
							napalmLines[currentLevel] =
								new NapalmLine(-pixelsUsed, currentLevel, bitmap);
						else
							napalmLines[currentLevel].newPt(-pixelsUsed);

						pixelNum--;
					}
					//expanding
					else
						pixelNum -= pixelsUsed;
				}

				if (pixelNum < 1) {
					first = true;
					state = FIRE;
				}
				break;
			case FIRE:
				if (first) {
					clearNapalm(color);

					if (minY > fireHeight)
						minY -= fireHeight;
					else {
						fireHeight = minY;
						minY = 0;
					}

					width = maxX - minX + 3;
					minX -= 1;

					height = maxY - minY + 1;

					frames = new int[3][height][width];
					// Get the rectangle with napalm from bitmap
					bitmap.getSprite(minX, minY, frames[screen]);

					colorModel = new NapalmColorModel();
					first = false;
				}

				initFrames(
					frames[0],
					frames[1],
					.10 + .18 * (burnoutDuration > fireDuration ?
						(1.0 * (fireDuration > burnoutDuration / 4 ? fireDuration : 0) / burnoutDuration) :
						1),
					frames[screen]);

				fillCurrentFrame(frames[screen]);

				bitmap.setSandMode(true);
				bitmap.drawSprite(minX, minY + 3, frames[currentFrame],
					colorModel.getRGB(black), colorModel,
					true);
				drawUnderFire(frames[screen]);
				bitmap.setSandMode(false);
				//extra margin of + 3
				bitmap.newPixels(minX - 1, minY - 1, width + 3, height + 3);

				fireDuration--;
				//Debug.pause(1000);

				if (fireDuration < 1)
					state = CLEAR;
				break;
			case CLEAR:
				bitmap.setColor(null);
				for (int i = 0; i < frames[screen].length; i++)
					for (int j = 0; j < frames[screen][0].length; j++) {
						if (frames[screen][i][j] == color)
							bitmap.setPixel(minX + j, minY + i);
						else
							bitmap.setPixel(minX + j, minY + i, frames[screen][i][j]);
					}
				bitmap.newPixels(minX, minY, frames[screen][0].length, frames[screen].length);
				return false;

			default:
				System.out.println("NapalmExplosion: Warning: Invalid Case");
		}
		return true;
	}

	private void drawUnderFire(int[][] filter) {
		for (int j = 0; j < filter[0].length; j++)
			for (int i = fireLevels[j]; i < filter.length; i++)
				bitmap.setPixel(minX + j, minY + i, filter[i][j]);
	}

	private void initFrames(int[] array1, int[] array2, double density) {
		for (int i = 0; i < array1.length; i++) {
			if (random.nextFloat() > 1 - density)
				array1[i] = array2[i] = white;
			else
				array1[i] = array2[i] = black;
		}
	}

	private void initFrames(int[][] array1, int[][] array2, double density, int[][] filter) {
		if (fireLevels == null)
			fireLevels = new int[filter[0].length];

		for (int i = 0; i < filter.length; i++)
			for (int j = 0; j < filter[0].length; j++) {
				if (filter[i][j] == color &&
					((i == filter.length - 1) ||
						(filter[i + 1][j] != color))) {
					fireLevels[j] = i;
					if (random.nextFloat() > 1 - density)
						array1[i][j] = array2[i][j] = white;
					else
						array1[i][j] = array2[i][j] = black;
				}
			}
	}

	private void fillCurrentFrame(int[][] filter) {
		if (currentFrame == 0)
			currentFrame = 1;
		else
			currentFrame = 0;

		for (int i = 0; i < frames[currentFrame].length - 1; i++)
			for (int j = 1; j < frames[currentFrame][0].length - 1; j++)
				frames[currentFrame][i][j] = (
					i == frames[currentFrame].length - 2 ?
					getAveragePixel(frames[(currentFrame + 1) % 2], j, i + 1, true) :
					getAveragePixel(frames[(currentFrame + 1) % 2], j, i + 1, false)
				);
	}

	private int getAveragePixel(int[][] array, int x, int y, boolean bottom) {
		int r, g, b;
		int div;

		b = /*(255 & (array[y][x])) + FIRE STORM DEBUG*/
			(255 & (array[y - 1][x])) + (255 & (array[y - 1][x - 1])) +
				(255 & (array[y][x - 1])) + (255 & (array[y][x + 1])) +
				(255 & (array[y - 1][x + 1])) + (bottom ? 0 :
				(255 & (array[y + 1][x - 1])) +
					(255 & (array[y + 1][x])) +
					(255 & (array[y + 1][x + 1])));
		if (bottom)
			div = 5;
		else
			div = 8;

		b /= div;
		r = g = b;

		return Bitmap.getColor(r, g, b);
	}

	private void clearNapalm(int color) {
		int[] pair;

		bitmap.setColor(color);

		for (int i = 0; i < napalmLines.length; i++)
			if (napalmLines[i] != null) {
				if (minY == -1) minY = i;
				if (maxY < i) maxY = i;
				pair = napalmLines[i].clearNapalm();
				if (minX > pair[0]) minX = pair[0];
				if (maxX < pair[1]) maxX = pair[1];
			}
		bitmap.newPixels(minX, maxY, maxX - minX, minY - maxY);
	}

	private boolean canFill(int x, int y) {
		return bitmap.isBackground(x, y);
	}

	public int calculateDamage(ScorchPlayer player) {
		int[][] filter = frames[screen];
		int center_x = player.getX() + player.getWidth() / 2,
			center_y = player.getY() + player.getHeight() / 2, damage = 0, dist = 0;


		for (int i = 0; i < filter.length; i++)
			for (int j = 0; j < filter[0].length; j++)
				if (filter[i][j] == color) {
					dist = (int) (Math.pow(center_x - (j + minX), 2) +
						Math.pow(center_y - (minY + i), 2));

					if (dist <= 800)
						damage += Math.pow((800 - dist), 2);
				}
		damage /= (type == HOT_NAPALM ? 50000 : 80000);
		return damage;
	}
}
