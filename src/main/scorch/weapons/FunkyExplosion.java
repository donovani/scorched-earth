package scorch.weapons;

import scorch.Bitmap;
import scorch.Physics;
import scorch.ScorchPlayer;

import java.awt.*;
import java.util.Random;

/**
 * The classic funky bomb; a multi-particle explosion.
 * @author Mikhail Kruk
 */
public class FunkyExplosion extends ParticlesExplosion {
	private final int[] colors = {
		Bitmap.getColor(255, 0, 0),
		Color.orange.getRGB(),
		Color.yellow.getRGB(),
		Bitmap.getColor(0, 255, 0),
		Bitmap.getColor(0, 0x7F, 0xFF),
		Bitmap.getColor(0, 0, 255)
	};

	private final static int FUNKY_SIZE = 30;

	private static final int offset = 5; // the offset of the trajectories

	private Explosion explosion; // store the baby nuke aftershock here
	private int aftershockRadius; // aftershock radius

	public FunkyExplosion() { }

	public FunkyExplosion(Bitmap bitmap, Random rand, int n) {
		super(bitmap);
		setRandom(rand);
		setArgument(n);
	}

	protected void initParticles() {
		int angle, xOffset, yOffset, power;
		double radianAngle;
		TracerMissile missile;

		for (int i = 0; i < explosionCount; i++) {
			power = (int) (Math.abs(random.nextInt() % 500) / 8.0);
			angle = 20 + Math.abs(random.nextInt() % 140);
			radianAngle = (double) angle * Math.PI / 180.00;
			xOffset = (int) (Math.cos(radianAngle) * offset);
			yOffset = (int) (Math.sin(radianAngle) * offset);
			missile = new TracerMissile(
				bitmap,
				new Physics(
					x + xOffset,
					bitmap.getHeight() - y + yOffset,
					angle,
					power
				),
				new ColorStripExplosion(
					bitmap,
					null,
					0,
					0,
					FUNKY_SIZE,
					colors[i % colors.length]
				)
			);
			particles.addElement(missile);
		}

		radius = FUNKY_SIZE;
		aftershockRadius = Math.round(explosionCount * 10);
	}

	public boolean drawNextFrame(boolean update) {
		boolean res;

		if (explosion == null) {
			res = super.drawNextFrame(update);
			if (!res)
				explosion = new SimpleExplosion(bitmap, x, y, aftershockRadius);
			res = true;
		} else
			res = explosion.drawNextFrame(update);

		return res;
	}

	public int calculateDamage(ScorchPlayer player) {
		if (explosion == null)
			return 0;
		else
			return super.calculateDamage(player) + explosion.calculateDamage(player);
	}

	public ExplosionInfo getExplosionInfo() {
		ExplosionInfo info = super.getExplosionInfo();
		info.explosionArea.add(new Rectangle(
			x - aftershockRadius,
			y - aftershockRadius,
			2 * aftershockRadius,
			2 * aftershockRadius
		));
		return info;
	}
}
