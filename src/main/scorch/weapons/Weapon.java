package scorch.weapons;

import scorch.Bitmap;
import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.items.Item;
import scorch.items.ItemControl;

import java.util.Random;

/**
 * The parent class of all the scorch weapons used to store explosions, prices, names, etc.
 * Refer to README for brief description of weapon adding procedure.
 *
 * @author Mikhail Kruk
 */
public abstract class Weapon extends Item {
	private static final String packageName = "scorch.weapons.";

	public static final int Missile = 0,
		BabyNuke = 1, Nuke = 2,
		SandBomb = 3, BabyRoller = 4, Roller = 5, HeavyRoller = 6,
		BabyDigger = 7, Digger = 8, HeavyDigger = 9,
		FunkyBomb = 10, FunkyNuke = 11,
		Napalm = 12, HotNapalm = 13, MIRV = 14, DeathHead = 15;

	private static final String[] names = {
		"Missile", "Baby Nuke", "Nuke",
		"Sand Bomb",
		"Baby Roller", "Roller", "Heavy Roller",
		"Baby Digger", "Digger", "Heavy Digger",
		"Funky Bomb", "Funky Nuke",
		"Napalm", "Hot Napalm",
		"MIRV", "Death Head"
	};

	protected String explosionClass = "weapon";
	protected int argument = -1;

	// get name of this instance of weapon
	public String getName() {
		return names[type];
	}

	// get weapon name by type
	public static String getName(int type) {
		return names[type];
	}

	// get type of the weapons buy its name
	public static int getType(String name) {
		for (int i = 0; i < names.length; i++)
			if (names[i].equals(name))
				return i;
		return -1;
	}

	public ItemControl getControlPanel(ScorchGame scorchGame) {
		controlPanel = new WeaponControl(type, scorchGame);
		return controlPanel;
	}

	public Explosion produceExplosion(Bitmap bitmap, Random random) {
		try {
			// for Netscape again
			Explosion explosion = (Explosion) Class.forName(packageName + explosionClass).newInstance();

			explosion.setBitmap(bitmap);
			//explosion.setPosition(0,0);
			explosion.setRandom(random);
			explosion.setArgument(argument);
			return explosion;
		} catch (Exception error) {
			System.err.println("produceExplosion: " + error);
		}
		return null;
	}

	// produce an array of all available weapons.
	// Relies on the list of class names called names[].
	// For now name of the weapons == name of the class (spaces in names are omitted)
	public static Weapon[] loadWeapons(ScorchPlayer player) {
		Weapon[] result = new Weapon[names.length];

		try {
			for (int i = 0; i < names.length; i++) {
				String name = trimSpaces(packageName, names[i]);
				result[i] = (Weapon) Class.forName(name).newInstance();
				result[i].scorchPlayer = player;
			}
		} catch (Exception error) {
			System.err.println("Failed to load weapons: " + error);
		}
		return result;
	}
}


