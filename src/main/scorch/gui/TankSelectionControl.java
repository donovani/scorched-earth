package scorch.gui;


import scorch.Bitmap;
import scorch.Tanks;
import scorch.backgrounds.PlainBackground;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
class TankSelectionControl extends Panel {
	private static final int tankBorder = 3;

	private Bitmap bitmap = null;
	private int width = -1, height = -1;
	private Image backBuffer;
	private int selectedTank = 0;

	public TankSelectionControl(int width, int height) {
		setLayout(null);
		setSize(width, height);
		this.width = width;
		this.height = height;
	}

	public void paint(Graphics graphics) {
		update(graphics);
	}

	public boolean handleEvent(Event event) {
		int i, x, y;

		if (bitmap == null)
			return false;

		if (event.id == Event.KEY_ACTION &&
			(event.key == Event.LEFT || event.key == Event.RIGHT)) {
			drawBorder(selectedTank, false);
			if (event.key == Event.LEFT)
				selectedTank--;
			else
				selectedTank++;
			if (selectedTank < 0) selectedTank = Tanks.TANK_COUNT - 1;
			if (selectedTank > Tanks.TANK_COUNT - 1) selectedTank = 0;
			drawBorder(selectedTank, true);
		}

		if (event.id == Event.MOUSE_DOWN || event.id == Event.MOUSE_UP ||
			event.id == Event.MOUSE_DRAG) {
			x = event.x;
			y = event.y;

			i = ((x - (width / (Tanks.TANK_COUNT + 1)) / 2) /
				(width / (Tanks.TANK_COUNT + 1)));

			if (i < 0 || i >= Tanks.TANK_COUNT)
				return true;

			if (Math.abs((i + 1) * (width / (Tanks.TANK_COUNT + 1)) - x) <=
				(Tanks.getTankWidth(i) / 2 + tankBorder)
				&& (i != selectedTank)) {
				drawBorder(selectedTank, false);
				selectedTank = i;
				drawBorder(selectedTank, true);
			}
		}

		return super.handleEvent(event);
	}

	public void update(Graphics g) {
		if (bitmap == null) {
			bitmap =
				new Bitmap(width, height, new PlainBackground
					(width, height, Color.darkGray));
			backBuffer = createImage(bitmap.getImageProducer());
			redraw();
		}

		g.drawImage(backBuffer, 0, 0, this);
	}

	private void redraw() {
		bitmap.setColor(null);
		bitmap.fillRect(0, 0, width, height);

		for (int i = 0; i < Tanks.TANK_COUNT; i++) {
			if (i == selectedTank) {
				drawBorder(i, true);
			}

			bitmap.drawSprite(
				width / (Tanks.TANK_COUNT + 1) * (i + 1) - Tanks.getTankWidth(i) / 2,
				(height - Tanks.getTankHeight(i)) / 2,
				Tanks.getTank(i, -1),
				0
			);
		}
		bitmap.newPixels(0, 0, width, height, true);
	}

	private void drawBorder(int tank, boolean show) {
		int tankWidth = Tanks.getTankWidth(tank),
			tankHeight = Tanks.getTankHeight(tank),
			borderX = width / (Tanks.TANK_COUNT + 1) * (tank + 1) - tankWidth / 2 - tankBorder,
			borderY = (height - tankHeight) / 2 - tankBorder;

		if (show)
			bitmap.setColor(Color.lightGray);
		else
			bitmap.setColor(null);

		bitmap.drawRect(borderX, borderY, tankWidth + 2 * tankBorder, tankHeight + 2 * tankBorder);

		if (show)
			bitmap.setColor(Color.gray);
		else
			bitmap.setColor(null);

		bitmap.drawLine(
			borderX,
			borderY + tankHeight + 2 * tankBorder,
			borderX + tankWidth + 2 * tankBorder,
			borderY + tankHeight + 2 * tankBorder
		);
		bitmap.drawLine(
			borderX + tankWidth + 2 * tankBorder,
			borderY,
			borderX + tankWidth + 2 * tankBorder,
			borderY + tankHeight + 2 * tankBorder
		);
		bitmap.newPixels(
			borderX,
			borderY,
			tankWidth + 2 * tankBorder + 1,
			tankHeight + 2 * tankBorder + 1,
			true
		);
	}

	public int getSelected() {
		return selectedTank;
	}

	public void setSelected(int tankId) {
		selectedTank = tankId;
	}

}
