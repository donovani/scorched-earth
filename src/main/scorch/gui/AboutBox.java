package scorch.gui;

import scorch.windows.ScorchPanel;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This dialog shows the team's info
 *
 * @author Mikhail Kruk
 */
public class AboutBox extends ScorchWindow implements ActionListener {
	private final CreditsPanel creditsPanel;

	public AboutBox(Container owner) {
		super(-1, -1, 0, 0, "About Scorched Earth 2000", owner);

		Panel panel = new Panel(new FlowLayout(FlowLayout.CENTER));

		creditsPanel = new CreditsPanel();
		ScorchPanel scorchPanel = new ScorchPanel(-1, -1);
		scorchPanel.add(creditsPanel);

		Button okButton = new Button("OK");
		okButton.addActionListener(this);
		panel.add(okButton);

		Button licenseButton = new Button("License");
		licenseButton.addActionListener(this);
		panel.add(licenseButton);

		setLayout(new BorderLayout());
		add(scorchPanel, BorderLayout.CENTER);
		add(panel, BorderLayout.SOUTH);
	}

	public void display() {
		super.display();
		creditsPanel.startScroll();
	}

	public void close() {
		creditsPanel.stopScroll();
		super.close();
		transferFocus();
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		if (command.equals("OK")) {
			close();
		}
		if (command.equals("License")) {
			close();
			LicensePanel licensePanel = new LicensePanel(owner);
			licensePanel.display();
		}
	}
}
