package scorch.gui;

import scorch.Physics;
import scorch.PlayerProfile;
import scorch.PlayerSettings;
import scorch.ScorchGame;
import scorch.windows.MessageBox;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * The dialog window presented to the master client to
 * select both his player and general game options
 *
 * @author Mikhail Kruk
 */
public class GameOptions extends PlayersList implements ActionListener,
	FocusListener {
	private final TextField gravity;
	private final TextField cash;
	private final TextField rounds;
	private float gravityValue = Physics.EARTH_GRAVITY;
	private long cashValue = 0;
	private int roundsValue = 5;
	private final Choice wind;
	private final TankSelection tankSelection;
	private final Checkbox hazards;
	private final Checkbox sounds;
	private final Checkbox lamerMode;
	private final boolean master;
	private final PlayerProfile profile;


	public GameOptions(String masterName, int maxPlayers, PlayerProfile profile, ScorchGame scorchGame) {
		this(masterName, maxPlayers, true, profile, scorchGame);
	}

	public GameOptions(String masterName, int maxPlayers, boolean master, PlayerProfile profile, ScorchGame scorchGame) {
		super("Game options", scorchGame);

		// init dialog items
		gravity = new TextField(7);
		cash = new TextField(7);
		rounds = new TextField(7);
		hazards = new Checkbox("Nature hazards");
		sounds = new Checkbox("Sound effects");
		lamerMode = new Checkbox("Lamer mode");
		tankSelection = new TankSelection(200, 30);
		wind = new Choice();
		wind.addItem("Constant wind");
		wind.addItem("No wind");
		wind.addItem("Changing wind");
		wind.select(1);

		gravity.addActionListener(this);
		cash.addActionListener(this);
		rounds.addActionListener(this);
		gravity.addFocusListener(this);
		cash.addFocusListener(this);
		rounds.addFocusListener(this);

		// attempt to read settings from profile
		if (profile.getNumberOfRounds() > 0) roundsValue = profile.getNumberOfRounds();
		if (profile.getInitialCash() > 0) cashValue = profile.getInitialCash();
		if (profile.getGravity() > 0) gravityValue = profile.getGravity();
		if (profile.getWind() >= 0) wind.select(profile.getWind());
		if (profile.getTankType() >= 0)
			tankSelection.setSelected(profile.getTankType());
		hazards.setState(profile.getHazards());
		sounds.setState(profile.getSounds());
		lamerMode.setState(false);

		this.master = master;
		this.profile = profile;

		Panel contentPanel, playersPanel, roundsPanel, bottomPanel, gravityPanel, cashPanel, checkboxPanel, infoPanel, buttonPanel, tankPanel, p2d;

		// enable everything that is relevant to masters. Master is always true
		gravity.setEnabled(master);
		cash.setEnabled(master);
		rounds.setEnabled(master);
		hazards.setEnabled(false);
		lamerMode.setEnabled(master);
		sounds.setEnabled(master);

		// init values
		gravity.setText("" + gravityValue);
		cash.setText("" + cashValue);
		rounds.setText("" + roundsValue);

		addPlayer(masterName);

		playersPanel = new Panel();
		playersPanel.setLayout(new BorderLayout());
		playersPanel.add(new Label("Players:"), BorderLayout.NORTH);
		playersPanel.add(players, BorderLayout.CENTER);

		roundsPanel = new Panel();
		roundsPanel.setLayout(new GridLayout(5, 1));
		p2d = new Panel();
		p2d.setLayout(new FlowLayout(FlowLayout.RIGHT));
		p2d.add(new Label("Number of rounds:"));
		p2d.add(rounds);
		roundsPanel.add(p2d);

		gravityPanel = new Panel();
		gravityPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		gravityPanel.add(new Label("Gravity (m/sec^2):"));
		gravityPanel.add(gravity);
		roundsPanel.add(gravityPanel);

		cashPanel = new Panel();
		cashPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		cashPanel.add(new Label("Initial cash ($):"));
		cashPanel.add(cash);
		roundsPanel.add(cashPanel);

		roundsPanel.add(wind);

		checkboxPanel = new Panel();
		checkboxPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		//checkboxPanel.add(hazards);
		checkboxPanel.add(sounds);
		checkboxPanel.add(lamerMode);
		roundsPanel.add(checkboxPanel);

		bottomPanel = new Panel();
		bottomPanel.setLayout(new BorderLayout());

		tankPanel = new Panel();
		tankPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		tankPanel.add(new Label("Select your tank:"));
		tankPanel.add(tankSelection);

		buttonPanel = new Panel();
		if (master) {
			Button startButton = new Button("Start Game");
			startButton.addActionListener(this);
			buttonPanel.add(startButton);

			Button cancelButton = new Button("Cancel Game");
			cancelButton.addActionListener(this);
			buttonPanel.add(cancelButton);
		} else {
			Button okButton = new Button("OK");
			okButton.addActionListener(this);
			buttonPanel.add(okButton);

			Button cancelButton = new Button("Cancel");
			cancelButton.addActionListener(this);
			buttonPanel.add(cancelButton);
		}

		bottomPanel.add(buttonPanel, BorderLayout.SOUTH);
		bottomPanel.add(tankPanel, BorderLayout.CENTER);

		contentPanel = new Panel();
		contentPanel.setLayout(new FlowLayout());
		contentPanel.add(playersPanel);
		contentPanel.add(roundsPanel);

		infoPanel = new Panel();
		infoPanel.setLayout(new BorderLayout());
		if (master) {
			infoPanel.add(
				new Label("You are the master of the game."),
				BorderLayout.NORTH
			);
			infoPanel.add(
				new Label("Maximum number of players in the game is set to " + maxPlayers),
				BorderLayout.CENTER
			);
			infoPanel.add(
				new Label("Please select game options:"),
				BorderLayout.SOUTH
			);
		} else {
			infoPanel.add(
				new Label("You are connected as " + masterName),
				BorderLayout.NORTH
			);
			infoPanel.add(
				new Label("Maximum number of players in the game is set to " + maxPlayers),
				BorderLayout.CENTER
			);
			infoPanel.add(
				new Label("Please select player options:"),
				BorderLayout.SOUTH
			);
		}

		setLayout(new BorderLayout());
		add(infoPanel, BorderLayout.NORTH);
		add(contentPanel, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);
		validate();

		timeLeft = 180;
	}

	public void focusGained(FocusEvent event) {
	}

	public void focusLost(FocusEvent event) {
		Object source = event.getSource();

		if (source == gravity) {
			try {
				gravityValue = Float.parseFloat(gravity.getText());
			} catch (NumberFormatException error) {}
			if (gravityValue < 0) gravityValue = 0;
			gravity.setText("" + gravityValue);
		}
		if (source == cash) {
			try {
				cashValue = Long.parseLong(cash.getText());
			} catch (NumberFormatException error) {}
			if (cashValue < 0) cashValue = 0;
			cash.setText("" + cashValue);
		}
		if (source == rounds) {
			try {
				roundsValue = Integer.parseInt(rounds.getText());
			} catch (NumberFormatException error) {}
			if (roundsValue < 1) roundsValue = 1;
			rounds.setText("" + roundsValue);
		}
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		Object source = event.getSource();

		if (source == rounds) {
			gravity.requestFocus();
			return;
		}
		if (source == gravity) {
			cash.requestFocus();
			return;
		}
		if (source == cash) {
			wind.requestFocus();
			return;
		}
		if (command.equals("OK")) {
			close();
			scorchGame.joinGame(new PlayerSettings(tankSelection.getSelected(), sounds.getState()));
			return;
		}
		if (command.equals("Cancel")) {
			scorchGame.Quit();
			return;
		}
		if (command.equals("Start Game")) {
			if (players.getItemCount() < 2) {
				String[] buttons = {"OK"};
				MessageBox messageBox = new MessageBox(
					"Error",
					"Can not start game without opponents",
					buttons,
					null,
					owner,
					this
				);
				messageBox.display();
				return;
			}

			close();

			profile.setNumberOfRounds(roundsValue);
			profile.setInitialCash(cashValue);
			profile.setGravity(gravityValue);
			profile.setWind(wind.getSelectedIndex());
			profile.setTankType(tankSelection.getSelected());
			profile.setHazards(hazards.getState());
			profile.setSounds(sounds.getState());
			profile.setLamerMode(lamerMode.getState());

			scorchGame.setGameOptions(profile);
			return;
		}
		if (command.equals("Cancel Game")) {
			scorchGame.Quit();
		}
	}
}
