package scorch.gui;

import scorch.windows.ScorchWindow;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
public class ChatPanel extends ScorchWindow {
	private final TextArea chatArea;
	private final TextField chatMessage;

	public ChatPanel(int x, int y, int width, int height, Container owner) {
		super(x, y, width, height, "Scorched Earth 2000 Chat", owner);

		chatArea = new TextArea();
		chatArea.setEditable(false);
		chatMessage = new TextField();

		setLayout(new BorderLayout());
		add(chatArea, BorderLayout.CENTER);
		add(chatMessage, BorderLayout.SOUTH);
	}
}
