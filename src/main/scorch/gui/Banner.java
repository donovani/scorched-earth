package scorch.gui;

import scorch.ScorchGame;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * @author Mikhail Kruk
 */
class Banner extends Canvas implements MouseListener {
	private final String bannerImage; // = "b1.gif";
	private final String url;
	private final Image banner;

	private final BannerWindow owner;

	Banner(BannerWindow owner, String image, String address) {
		super();
		this.owner = owner;
		this.bannerImage = image;
		this.url = address;

		MediaTracker tracker = new MediaTracker(this);
		banner = ScorchGame.getImage(bannerImage);
		tracker.addImage(banner, 0);

		try {
			tracker.waitForAll();
		} catch (InterruptedException error) {
			System.err.println(error);
		}
		setSize(banner.getWidth(this), banner.getHeight(this));
		setCursor(new Cursor(Cursor.HAND_CURSOR));

		addMouseListener(this);
	}

	public void mouseClicked(MouseEvent event) {
		owner.visit(url);
	}

	public void mousePressed(MouseEvent event) {
	}

	public void mouseReleased(MouseEvent event) {
	}

	public void mouseEntered(MouseEvent event) {
	}

	public void mouseExited(MouseEvent event) {
	}

	public void paint(Graphics graphics) {
		graphics.drawImage(banner, 0, 0, this);
	}
}
