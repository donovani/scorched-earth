package scorch.gui;

import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.weapons.Weapon;
import scorch.windows.MessageBox;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.util.Vector;

/**
 * The main GUI class for the interaction with the user.
 * All the controls live here, & this handles all the keyboard
 * input and then forwards it to appropriate destinations.
 *
 * @author Mikhail Kruk
 */
public class MainToolbar extends ScorchWindow {
	private final Label powerLabel;
	private final Label angleLabel;
	private final Label ammoLabel;
	private final PlayersListControl playersListControl;
	private final Button upButton;
	private final Button downButton;
	private final Button leftButton;
	private final Button rightButton;
	private final Button fireButton;
	private final Button inventoryButton;
	private final Button systemMenuButton;
	private final Choice weaponChoice;
	private final ScorchPlayer myPlayer;
	private final SystemMenu systemMenu;
	private Inventory inventoryWindow;
	private final ScorchGame scorchGame;

	public MainToolbar(Vector players, ScorchGame scorchGame) {
		super(
			scorchGame.getGameWidth(),
			0,
			scorchGame.getWidth() - scorchGame.getGameWidth(),
			scorchGame.getHeight(),
			"SCORCH 2000",
			scorchGame.container
		);

		this.scorchGame = scorchGame;
		this.myPlayer = scorchGame.getMyPlayer();

		//addKeyListener(this);

		systemMenu = new SystemMenu(scorchGame);

		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints;

		setLayout(layout);

		playersListControl = new PlayersListControl(players, myPlayer, this);
		add(playersListControl);
		playersListControl.setVisible(true);
		//pl.addKeyListener(this);
		constraints = makeConstraints
			(0, 0, 1, 8, GridBagConstraints.BOTH,
				GridBagConstraints.CENTER, new Insets(0, 5, 0, 5));
		constraints.weightx = 1000;
		constraints.weighty = 1000;
		layout.setConstraints(playersListControl, constraints);

		systemMenuButton = new Button("System");
		//system.addKeyListener(this);
		add(systemMenuButton);
		constraints = makeConstraints(
			0,
			9,
			1,
			1,
			GridBagConstraints.HORIZONTAL,
			GridBagConstraints.CENTER,
			new Insets(2, 5, 2, 5)
		);
		layout.setConstraints(systemMenuButton, constraints);


		inventoryButton = new Button("Inventory");
		//inventory.addKeyListener(this);
		add(inventoryButton);
		constraints = makeConstraints(
			0,
			10,
			1,
			1,
			GridBagConstraints.HORIZONTAL,
			GridBagConstraints.CENTER,
			new Insets(2, 5, 2, 5)
		);
		layout.setConstraints(inventoryButton, constraints);

		weaponChoice = new Choice();
		//weapon.addKeyListener(this);

		Weapon[] weapons = myPlayer.getWeapons();
		Weapon weapon;
		for (int i = 0; i < weapons.length; i++) {
			weapon = weapons[i];
			if (weapon.getQuantity() > 0)
				weaponChoice.addItem(weapon.getName());
		}

		add(weaponChoice);
		constraints = makeConstraints(
			0,
			11,
			1,
			1,
			GridBagConstraints.NONE,
			GridBagConstraints.CENTER,
			new Insets(2, 5, 2, 5)
		);
		layout.setConstraints(weaponChoice, constraints);

		Panel dataPanel = new Panel();
		powerLabel = new Label(myPlayer.getPower() + "", Label.LEFT);
		angleLabel = new Label(myPlayer.getAngle() + "", Label.LEFT);
		ammoLabel = new Label("" + myPlayer.getWeaponAmmo(), Label.LEFT);
		powerLabel.setForeground(Color.red);
		angleLabel.setForeground(Color.red);
		ammoLabel.setForeground(Color.red);
		dataPanel.setLayout(new GridLayout(3, 2, 0, 0));
		dataPanel.add(new Label("Ammo:"));
		dataPanel.add(ammoLabel);
		dataPanel.add(new Label("Power:"));
		dataPanel.add(powerLabel);
		dataPanel.add(new Label("Angle:"));
		dataPanel.add(angleLabel);
		add(dataPanel);
		constraints = makeConstraints(
			0,
			13,
			1,
			2,
			GridBagConstraints.HORIZONTAL,
			GridBagConstraints.WEST,
			new Insets(0, 5, 0, 5)
		);
		layout.setConstraints(dataPanel, constraints);

		Panel buttonPanel = new Panel();
		buttonPanel.setLayout(new GridLayout(2, 3, 5, 5));
		upButton = new Button("+");
		//up.addKeyListener(this);
		downButton = new Button("-");
		//down.addKeyListener(this);
		leftButton = new Button("<");
		//left.addKeyListener(this);
		rightButton = new Button(">");
		//right.addKeyListener(this);
		fireButton = new Button("fire");
		//fire.addKeyListener(this);
		buttonPanel.add(new Label(""));
		buttonPanel.add(upButton);
		buttonPanel.add(new Label(""));
		buttonPanel.add(leftButton);
		buttonPanel.add(downButton);
		buttonPanel.add(rightButton);

		add(buttonPanel);
		constraints = makeConstraints(
			0,
			17,
			1,
			1,
			GridBagConstraints.HORIZONTAL,
			GridBagConstraints.WEST,
			new Insets(2, 5, 2, 5)
		);
		layout.setConstraints(buttonPanel, constraints);

		add(fireButton);
		constraints = makeConstraints(
			0,
			18,
			1,
			1,
			GridBagConstraints.HORIZONTAL,
			GridBagConstraints.WEST,
			new Insets(2, 5, 2, 5)
		);
		layout.setConstraints(fireButton, constraints);

		updateWeapon();

		validate();
	}

	public void updateWeapon() {
		weaponChoice.select(Weapon.getName(myPlayer.getWeapon()));
		ammoLabel.setText("" + myPlayer.getWeaponAmmo());
	}
    /*
    public void keyPressed(KeyEvent event)
    {
	if( !isEnabled() ) return;

	char key = event.getKeyChar();
	int code = event.getKeyCode();

	if( !event.isActionKey() )
	    {
		if( (key >= 'A' && key <= 'Z') || (key >= 'a' && key <= 'z') ||
		    (key >= '!' && key <= '@') )
		    {
			scorchGame.showChat(key);
			return;
		    }
	    }

	int scale = 1;
	if ( event.isControlDown() ) scale = 5;

	if( code == KeyEvent.VK_F1 )
	    {
		scorchGame.displayReference();
		return;
	    }

	if( code == KeyEvent.VK_F2 )
	    {
		StatsWindow sw = new StatsWindow
		    (StatsWindow.IG,scorchGame);
		sw.display();
		return;
	    }

	if( code == KeyEvent.VK_F3 && !(scorchGame).isGuest() )
	    {
		NewUser nu =
		    new NewUser
			scorchGame.getMyPlayer().getName(),
			 scorchGame,
			 scorchGame.getMyPlayer().
			 getProfile());
		nu.display();
		return;
	    }

	if( code == KeyEvent.VK_F4 )
	    {
		MessageBox msg;

		if( scorchGame.isMaster() )
		    {
			String b[] = {"Yes", "Cancel"};
			String c[] = {"massKill", null};
			msg = new MessageBox
			    ("Confirmation",
			     "Are you sure you want to kill everybody and start a new round?",
			     b, c, owner, this);
		    }
		else
		    {
			String b[] = {"OK"};
			String c[] = {null};
			msg = new MessageBox
			    ("Message",
			     "You have to be the master of the game to masskill",
			     b, c, owner, this);
		    }
		msg.display();
		return;
	    }

	if( code == KeyEvent.VK_F5 &&
	    (wndInventory == null || !wndInventory.isVisible()) &&
	    inventory.isEnabled())
	    {
		wndInventory = new Inventory(scorchGame);
		wndInventory.display();
		return;
	    }

	if( code == KeyEvent.VK_F10 && !sMenu.isVisible() )
	    {
		sMenu.display();
		return;
	    }

	// ignore all other events if it is not our turn
	if( !fire.isEnabled() )
	    return;

	if( code == KeyEvent.VK_LEFT)
	    {
		updateAngleLabel(scorchGame.changeAngle(scale));
		return;
	    }
	if( code == KeyEvent.VK_RIGHT )
	    {
		updateAngleLabel(scorchGame.changeAngle(-scale));
		return;
	    }
	if( code == KeyEvent.VK_HOME)
	    {
		updateAngleLabel(scorchGame.changeAngle(10));
		return;
	    }
	if( code == KeyEvent.VK_END )
	    {
		updateAngleLabel(scorchGame.changeAngle(-10));
		return;
	    }
	if( code == KeyEvent.VK_UP || key == '+' )
	    {
		updatePowerLabel(scorchGame.changePower(scale));
		return;
	    }
	if( code == KeyEvent.VK_DOWN || key == '-' )
	    {
		updatePowerLabel(scorchGame.changePower(-scale));
		return;
	    }
	if( code == KeyEvent.VK_PAGE_UP )
	    {
		updatePowerLabel(scorchGame.changePower(10));
		return;
	    }
	if( code == KeyEvent.VK_PAGE_DOWN )
	    {
		updatePowerLabel(scorchGame.changePower(-10));
		return;
	    }
	if( key == ' ' )
	    {
		enableKeys(false);
		scorchGame.fire();
		if(myPlayer.getWeaponAmmo() <= 0)
		    {
			weapon.remove(weapon.getSelectedIndex());
			myPlayer.setWeapon(0);
		    }
		ammo.setText(""+myPlayer.getWeaponAmmo());
		return;
	    }
    }

    public void keyReleased(KeyEvent event) {}
    public void keyTyped(KeyEvent event) {}

    public void actionPerformed(ActionEvent event)
    {
	if( !isEnabled() ) return;

	String command = event.getActionCommand();
    }    */

	public boolean handleEvent(Event event) {
		if (!isEnabled())
			return super.handleEvent(event);

		if (event.id == Event.KEY_PRESS &&
			((event.key >= 'A' && event.key <= 'Z') ||
				(event.key >= 'a' && event.key <= 'z') ||
				(event.key >= '!' && event.key <= '@'))) {
			scorchGame.showChat((char) event.key);
			return true;
		}

		if (event.id == Event.ACTION_EVENT) {
			if (event.target == weaponChoice) {
				scorchGame.setWeapon(Weapon.getType(weaponChoice.getSelectedItem()));
				ammoLabel.setText("" + myPlayer.getWeaponAmmo());
				return true;
			}
			if (event.target == leftButton) {
				updateAngleLabel((scorchGame).changeAngle(3));
				return true;
			}
			if (event.target == rightButton) {
				updateAngleLabel(scorchGame.changeAngle(-3));
				return true;
			}
			if (event.target == upButton) {
				updatePowerLabel(scorchGame.changePower(5));
				return true;
			}
			if (event.target == downButton) {
				updatePowerLabel(scorchGame.changePower(-5));
				return true;
			}
			if (event.target == fireButton && fireButton.isEnabled()) {
				enableKeys(false);
				scorchGame.fire();
				if (myPlayer.getWeaponAmmo() <= 0) {
					weaponChoice.remove(weaponChoice.getSelectedIndex());
					myPlayer.setWeapon(0);
				}
				ammoLabel.setText("" + myPlayer.getWeaponAmmo());
				return true;
			}
			if (event.target == systemMenuButton && !systemMenu.isVisible()) {
				systemMenu.display();
				return true;
			}
			if (event.target == inventoryButton &&
				(inventoryWindow == null || !inventoryWindow.isVisible()) &&
				inventoryButton.isEnabled()) {
				//if( inventoryWindow == null )
				inventoryWindow = new Inventory(scorchGame);
				inventoryWindow.display();
				return true;
			}

		}

		if (event.id == Event.KEY_ACTION || event.id == Event.KEY_PRESS) {
			int scale = 1;
			if (event.controlDown()) scale = 5;

			if (event.key == Event.F1) {
				(scorchGame).displayReference();
				return true;
			}

			if (event.key == Event.F2) {
				StatsWindow statsWindow = new StatsWindow(StatsWindow.IN_GAME, scorchGame);
				statsWindow.display();
				return true;
			}

			if (event.key == Event.F3 && !(scorchGame).isGuest()) {
				NewUser newUser = new NewUser(
					scorchGame.getMyPlayer().getName(),
					scorchGame,
					scorchGame.getMyPlayer().getProfile()
				);
				newUser.display();
				return true;
			}

			if (event.key == Event.F4) {
				MessageBox messageBox;

				if ((scorchGame).isMaster()) {
					String[] buttons = {"Yes", "Cancel"};
					String[] callbacks = {"massKill", null};
					messageBox = new MessageBox(
						"Confirmation",
						"Are you sure you want to kill everybody and start a new round?",
						buttons,
						callbacks,
						owner,
						this
					);
				} else {
					String[] buttons = {"OK"};
					String[] callbacks = {null};
					messageBox = new MessageBox(
						"Message",
						"You have to be the master of the game to masskill",
						buttons,
						callbacks,
						owner,
						this
					);
				}
				messageBox.display();
				return true;
			}

			if (event.key == Event.F5 &&
				(inventoryWindow == null || !inventoryWindow.isVisible()) &&
				inventoryButton.isEnabled()) {
				//if( wndInventory == null )
				inventoryWindow = new Inventory(scorchGame);
				inventoryWindow.display();
				return true;
			}

			if (event.key == Event.F8) {
				if ((scorchGame).isMaster()) {
					BootBox bootBox = new BootBox(scorchGame);
					bootBox.display();
				} else {
					MessageBox msg;
					String[] buttons = {"OK"};
					String[] callbacks = {null};
					msg = new MessageBox
						("Message",
							"You have to be the master of the game to boot players",
							buttons, callbacks, owner, this);
					msg.display();
				}
				return true;
			}

			if (event.key == Event.F10 && !systemMenu.isVisible()) {
				systemMenu.display();
				return true;
			}

			// ignore all other events if it is not our turn
			if (!fireButton.isEnabled())
				return super.handleEvent(event);

			if (event.key == Event.LEFT) {
				updateAngleLabel((scorchGame).changeAngle(scale));
				return true;
			}
			if (event.key == Event.RIGHT) {
				updateAngleLabel((scorchGame).changeAngle(-scale));
				return true;
			}
			if (event.key == Event.HOME) {
				updateAngleLabel((scorchGame).changeAngle(10));
				return true;
			}
			if (event.key == Event.END) {
				updateAngleLabel((scorchGame).changeAngle(-10));
				return true;
			}
			if (event.key == Event.UP || event.key == '+') {
				updatePowerLabel((scorchGame).changePower(scale));
				return true;
			}
			if (event.key == Event.DOWN || event.key == '-') {
				updatePowerLabel((scorchGame).changePower(-scale));
				return true;
			}
			if (event.key == Event.PGUP) {
				updatePowerLabel((scorchGame).changePower(10));
				return true;
			}
			if (event.key == Event.PGDN) {
				updatePowerLabel((scorchGame).changePower(-10));
				return true;
			}
			if (event.key == ' ' && fireButton.isEnabled()) {
				enableKeys(false);
				(scorchGame).fire();
				if (myPlayer.getWeaponAmmo() <= 0) {
					weaponChoice.remove(weaponChoice.getSelectedIndex());
					myPlayer.setWeapon(0);
				}
				ammoLabel.setText("" + myPlayer.getWeaponAmmo());
				return true;
			}
		}

		return super.handleEvent(event);
	}

	public PlayersLister getPlayersList() {
		return playersListControl;
	}

	public void enableKeys(boolean enable) {
		if (!enable)
			transferFocus();

		upButton.setEnabled(enable);
		downButton.setEnabled(enable);
		leftButton.setEnabled(enable);
		rightButton.setEnabled(enable);
		fireButton.setEnabled(enable);
		inventoryButton.setEnabled(enable);
		weaponChoice.setEnabled(enable);

		if (inventoryWindow != null)
			inventoryWindow.close();
	}

	public boolean isBarEnabled() {
		return upButton.isEnabled();
	}

	public void hideMenu() {
		systemMenu.close();
	}

	public void updatePowerLabel(int newValue) {
		powerLabel.setText(newValue + "");
	}

	public void updateAngleLabel(int newValue) {
		angleLabel.setText(newValue + "");
	}
}
