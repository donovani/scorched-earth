package scorch.gui;

import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.items.Item;
import scorch.weapons.Weapon;
import scorch.windows.ScorchScrollPanel;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

/**
 * Shows the list of weapons and items and allows to select and activate them
 *
 * @author Mikhail Kruk
 */
public class Inventory extends ScorchWindow implements ActionListener, KeyListener {
	protected ScorchPlayer myPlayer;
	protected ScorchGame scorchGame;

	public Inventory(ScorchGame scorchGame) {
		this(scorchGame, "Inventory");
	}

	public Inventory(ScorchGame scorchGame, String title) {
		super(-1, -1, 0, 0, title, scorchGame.container);

		this.scorchGame = scorchGame;
		myPlayer = scorchGame.getMyPlayer();

		Vector panels = new Vector();
		int count = 0;

		loadPanels(panels);

		count = panels.size();

		Panel buttonPanel = new Panel(new FlowLayout(FlowLayout.CENTER));
		Button okButton = new Button("OK");
		buttonPanel.add(okButton);

		okButton.addActionListener(this);
		addKeyListener(this);

		// take max number of rows we can display
		// assume that Inventory box should be <= 70% of the applet
		Panel tablePanel = new Panel(new FlowLayout());
		tablePanel.add((Component) panels.elementAt(0));
		tablePanel.setVisible(false);
		scorchGame.container.add(tablePanel);
		tablePanel.validate();

		int rows = (int) (scorchGame.getHeight() * 0.7) / ((Component) panels.elementAt(0)).getSize().height;

		scorchGame.container.remove(tablePanel);
		tablePanel.removeAll();

		ScorchScrollPanel scrollPanel = new ScorchScrollPanel(this, -1, -1, panels, rows);

		setLayout(new BorderLayout(0, 0));
		add(scrollPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	protected void loadPanels(Vector panels) {
		Weapon[] weapons = myPlayer.getWeapons();
		Item[] items = myPlayer.getItems();

		loadPanels(panels, weapons);
		loadPanels(panels, items);
	}

	// build a list of panels which correspond to items vector
	protected void loadPanels(Vector panels, Item[] items) {
		Panel panel;
		for (int i = 0; items != null && i < items.length; i++) {
			panel = preparePanel(items[i]);
			if (panel != null)
				panels.addElement(panel);
		}
	}

	// prepare panel to be inserted into the inventory
	// if item should not be inserted, return null
	protected Panel preparePanel(Item item) {
		Panel panel;

		if (item.getQuantity() == 0)
			return null;

		panel = new Panel(new GridLayout(1, 3, 0, 0));

		panel.add(new Label(item.getName(), Label.CENTER));
		panel.add(item.getQuantityLabel());
		panel.add(item.getControlPanel(scorchGame));

		return panel;
	}

	public void close() {
		super.close();
		transferFocus();
	}

	public void actionPerformed(ActionEvent event) {
		close();
	}

	public void keyPressed(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.VK_ESCAPE)
			close();
	}

	public void keyReleased(KeyEvent event) {
	}

	public void keyTyped(KeyEvent event) {
	}

}
