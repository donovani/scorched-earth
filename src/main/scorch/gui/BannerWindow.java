package scorch.gui;

import scorch.ScorchGame;
import scorch.windows.ScorchPanel;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Mikhail Kruk
 */
public class BannerWindow extends ScorchWindow implements ActionListener {
	private final String address;

	private final ScorchGame scorchGame;

	public BannerWindow(ScorchGame scorchGame, String image, String address) {
		super(-1, -1, 0, 0, "Advertisement", scorchGame.container);
		this.scorchGame = scorchGame;

		this.address = address;

		Panel labelPanel = new Panel(new GridLayout(2, 1));

		Panel buttonPanel = new Panel();
		Button cancelButton = new Button("Maybe later");
		Button buttonVisit = new Button("Open the page");
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(buttonVisit);
		buttonPanel.add(cancelButton);

		setLayout(new BorderLayout(0, 0));

		Panel topPanel = new Panel();
		ScorchPanel bannerPanel = new ScorchPanel(-1, -1);
		bannerPanel.add(new Banner(this, image, address));
		topPanel.add(bannerPanel);
		add(topPanel, BorderLayout.NORTH);
		labelPanel.add(new Label(
			"Please support our sponsors and us by clicking on the banner above",
			Label.CENTER
		));
		labelPanel.add(new Label(
			"Clicking banner will not terminate current game",
			Label.CENTER
		));
		add(labelPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		cancelButton.addActionListener(this);
		buttonVisit.addActionListener(this);

		validate();
	}

	public void visit(String siteUrl) {
		URL bannerUrl = null;
		try {
			bannerUrl = new URL(siteUrl);
		} catch (MalformedURLException e) {
		}

		close();
		scorchGame.banner(bannerUrl);
	}

	public void actionPerformed(ActionEvent event) {
		String cmd = event.getActionCommand();

		if (cmd.equals("Open the page"))
			visit(address);
		else
			scorchGame.banner(null);

		close();
	}
}
