package scorch.gui;

import scorch.PlayerProfile;
import scorch.Protocol;
import scorch.ScorchGame;
import scorch.windows.MessageBox;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The dialog box used for creating new user profiles & editing user profiles
 *
 * @author Mikhail Kruk
 */
public class NewUser extends ScorchWindow implements ActionListener {
	private final TextField username;
	private final TextField password1;
	private final TextField password2;
	private final TextField email;
	private final PlayerProfile profile;
	private final ScorchGame scorchGame;

	public NewUser(String name, ScorchGame owner) {
		this(name, owner, null);
	}

	public NewUser(String name, ScorchGame scorchGame, PlayerProfile profile) {
		super(
			-1, -1, 0, 0,
			profile == null ? "Creating new user profile" : "Edit user profile",
			scorchGame.container
		);

		this.profile = profile;
		this.scorchGame = scorchGame;

		username = new TextField(15);
		password1 = new TextField(15);
		password2 = new TextField(15);
		email = new TextField(15);

		username.setText(name);
		password1.setEchoChar('*');
		password2.setEchoChar('*');
		password1.setEditable(true);
		password2.setEditable(true);

		if (profile != null) {
			username.setEnabled(false);
			email.setText(profile.getEmail());
		}

		Panel credentialsPanel, buttonPanel, usernamePanel, passwordPanel1, passwordPanel2, emailPanel;

		usernamePanel = new Panel();
		usernamePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		usernamePanel.add(new Label("Username:"));
		usernamePanel.add(username);

		passwordPanel1 = new Panel();
		passwordPanel1.setLayout(new FlowLayout(FlowLayout.RIGHT));
		passwordPanel1.add(new Label("Password:"));
		passwordPanel1.add(password1);

		passwordPanel2 = new Panel();
		passwordPanel2.setLayout(new FlowLayout(FlowLayout.RIGHT));
		passwordPanel2.add(new Label("Confirm:"));
		passwordPanel2.add(password2);

		emailPanel = new Panel();
		emailPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		emailPanel.add(new Label("E-mail address:"));
		emailPanel.add(email);

		credentialsPanel = new Panel();
		credentialsPanel.setLayout(new GridLayout(4, 1));
		credentialsPanel.add(usernamePanel);
		credentialsPanel.add(passwordPanel1);
		credentialsPanel.add(passwordPanel2);
		credentialsPanel.add(emailPanel);

		buttonPanel = new Panel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		Button okButton = new Button("OK");
		okButton.addActionListener(this);
		buttonPanel.add(okButton);

		Button cancelButton = new Button("Cancel");
		cancelButton.addActionListener(this);
		buttonPanel.add(cancelButton);

		setLayout(new BorderLayout());
		add(credentialsPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		validate();
	}

	public void display() {
		super.display();
		if (username.getText().equals(""))
			username.requestFocus();
		else
			password1.requestFocus();
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		if (command.equals("Cancel")) {
			close();
			if (profile == null)
				(scorchGame).loginWindow(username.getText());
			return;
		}
		if (username.getText().equals("")) {
			String[] buttons = {"OK"};
			MessageBox messageBox = new MessageBox(
				"Error",
				"You must provide a user name",
				buttons,
				null,
				owner,
				this
			);
			messageBox.display();
			return;
		}
		if (username.getText().equals(Protocol.guest)) {
			String[] buttons = {"OK"};
			MessageBox messageBox = new MessageBox(
				"Error",
				"Username " + Protocol.guest + " is reserved",
				buttons,
				null,
				owner,
				this
			);
			messageBox.display();
			return;
		}
		if (!password1.getText().equals(password2.getText())) {
			String[] buttons = {"OK"};
			MessageBox messageBox = new MessageBox(
				"Error",
				"Passwords do not match. Please reenter",
				buttons,
				null,
				owner,
				this
			);
			password1.setText("");
			password2.setText("");
			messageBox.display();
			return;
		}
		if (password1.getText().equals("") && profile == null) {
			String[] buttons = {"OK"};
			MessageBox messageBox = new MessageBox(
				"Error",
				"You must enter a password",
				buttons,
				null,
				owner,
				this
			);
			messageBox.display();
			return;
		}
		if (command.equals("OK")) {
			if (profile == null)
				(scorchGame).sendNewUser(new PlayerProfile(
					username.getText(),
					password1.getText(),
					email.getText()
				));
			else {
				profile.setEmail(email.getText());
				String p = password1.getText();
				if (!p.equals("")) {
					profile.setPassword(p);
					(scorchGame).updateUser(profile, true);
				} else
					(scorchGame).updateUser(profile, false);
			}

			close();
		}
	}

	public void close() {
		// Linux appletviewer bug fix (almost works)
		password1.setText("");
		password2.setText("");
		password1.setEchoChar((char) 0);
		password2.setEchoChar((char) 0);
		super.close();
		//owner.requestFocus();
	}
}
