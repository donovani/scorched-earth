package scorch.gui;

import scorch.ScorchGame;
import scorch.items.Item;

import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Similar to inventory, but used for auto-defense only
 *
 * @author Mikhail Kruk
 */
public class AutoDefenseWindow extends Inventory {
	public AutoDefenseWindow(ScorchGame owner) {
		super(owner, "Auto Defense System");
	}

	protected Panel preparePanel(Item item) {
		if (item.autoDefense())
			return super.preparePanel(item);
		else
			return null;
	}

	public void actionPerformed(ActionEvent event) {
		(scorchGame).sendEOT("AutoDefense");
		close();
	}
}
