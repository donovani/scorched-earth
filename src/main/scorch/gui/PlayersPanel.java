package scorch.gui;

import scorch.ScorchPlayer;
import scorch.windows.ScorchPanel;

import java.awt.*;
import java.util.Vector;

/**
 * Panel that is part of the PlayersListControl.
 * @author Mikhail Kruk
 */
class PlayersPanel extends Panel {
	private Image backBuffer = null;
	private final Vector players;
	private int fontHeight;
	private Graphics backBufferGraphics;
	private final PlayersListControl owner;

	private ScorchPlayer highlighted;

	public PlayersPanel(Vector players, PlayersListControl owner) {
		this.players = players;
		this.owner = owner;
	}

	public void update() {
		drawNames();
		repaint();
	}

	public void paint(Graphics g) {
		if (backBuffer == null) {
			Dimension size = getSize();

			backBuffer = createImage(size.width, size.height);
			backBufferGraphics = backBuffer.getGraphics();

			fontHeight = owner.getFontHeight();
			drawNames();
		}

		g.drawImage(backBuffer, 0, 0, this);
	}

	void highlight(ScorchPlayer player) {
		highlighted = player;
		update();
	}

	private void drawNames() {
		String t;

		if (backBuffer == null)
			return;

		Dimension size = getSize();

		backBufferGraphics.setColor(Color.gray);
		backBufferGraphics.fillRect(0, 0, size.width, size.height);

		int verticalSize = (size.height - 2 * ScorchPanel.windowBorder) / ScorchPlayer.MAX_PLAYERS;
		if (verticalSize > fontHeight) //+ owner.wndBorder )
			verticalSize = fontHeight; //+ owner.wndBorder;

		for (int i = 0; i < players.size(); i++) {
			ScorchPlayer player = (ScorchPlayer) players.elementAt(i);
			if (!player.isAlive())
				backBufferGraphics.setColor(Color.black);
			else
				backBufferGraphics.setColor(new Color(player.getColor()));

			t = player.getName();
			t = t.substring(0, Math.min(12, t.length()));
			backBufferGraphics.drawString(t, 2 * ScorchPanel.windowBorder, (i + 1) * verticalSize);

			if (player == owner.getMyself()) {
				int lineWidth = (backBufferGraphics.getFontMetrics()).stringWidth(t);

				backBufferGraphics.setColor(Color.white);
				backBufferGraphics.drawLine(
					2 * ScorchPanel.windowBorder,
					(i + 1) * verticalSize + 1,
					2 * ScorchPanel.windowBorder + lineWidth,
					(i + 1) * verticalSize + 1
				);
			}

			if (highlighted == player) {
				backBufferGraphics.setColor(Color.white);
				backBufferGraphics.drawRect(
					2 * ScorchPanel.windowBorder - 2,
					i * verticalSize + 2,
					size.width + 2 - 4 * ScorchPanel.windowBorder,
					verticalSize);
				backBufferGraphics.setColor(Color.lightGray);
				backBufferGraphics.drawLine(
					2 * ScorchPanel.windowBorder - 2,
					i * verticalSize + 2 + verticalSize,
					size.width - 2 * ScorchPanel.windowBorder,
					i * verticalSize + 2 + verticalSize
				);
				backBufferGraphics.drawLine(
					size.width - 2 * ScorchPanel.windowBorder,
					i * verticalSize + 2,
					size.width - 2 * ScorchPanel.windowBorder,
					i * verticalSize + 2 + verticalSize
				);
			}
		}
	}
}
