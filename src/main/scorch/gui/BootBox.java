package scorch.gui;

import scorch.AIPlayer;
import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

/**
 * The window that popups when masters want to boot someone from the game
 *
 * @author Mikhail Kruk
 */
public class BootBox extends ScorchWindow implements ActionListener, KeyListener {
	private final Choice players;

	public BootBox(ScorchGame scorchGame) {
		super(-1, -1, 0, 0, "Boot player", scorchGame.container);

		players = new Choice();
		players.addKeyListener(this);

		Vector playerList = scorchGame.getPlayers();
		ScorchPlayer player;
		for (int i = 0; i < playerList.size(); i++) {
			player = (ScorchPlayer) playerList.elementAt(i);
			if (!(player instanceof AIPlayer))
				players.addItem(player.getName());
		}

		Panel buttonPanel = new Panel();

		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		Button bootButton = new Button("Boot");
		bootButton.addActionListener(this);
		buttonPanel.add(bootButton);

		Button cancelButton = new Button("Cancel");
		cancelButton.addActionListener(this);
		buttonPanel.add(cancelButton);

		addKeyListener(this);

		Panel playerPanel = new Panel(new FlowLayout(FlowLayout.CENTER));
		playerPanel.add(new Label("Boot player: "));
		playerPanel.add(players);

		setLayout(new BorderLayout());
		add(playerPanel, BorderLayout.CENTER);
		//add(new Panel(), BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.SOUTH);

		validate();
	}

	public void keyPressed(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.VK_ESCAPE)
			close();
	}

	public void keyReleased(KeyEvent event) {
	}

	public void keyTyped(KeyEvent event) {
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();

		if ("Boot".equals(command)) {
			//owner.bootPlayer(id?);
			close();
			return;
		}
		if ("Cancel".equals(command)) {
			close();
		}
	}
}
