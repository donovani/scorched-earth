package scorch.gui;

import scorch.windows.ScorchWindow;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
public class UsersPanel extends ScorchWindow {
	private final Button createButton;
	private final Button userInfoButton;
	private final Button helpButton;
	private final Button leaveButton;

	public UsersPanel(int x, int y, int width, int height, Container owner) {
		super(x, y, width, height, "Users", owner);

		setLayout(new BorderLayout());
		add(new List(), BorderLayout.CENTER);

		createButton = new Button("Create Game");
		userInfoButton = new Button("Get user info");
		helpButton = new Button("Help");
		leaveButton = new Button("Leave Scorch");

		Panel buttonPanel = new Panel(new GridLayout(4, 1, 0, 5));
		buttonPanel.add(createButton);
		buttonPanel.add(userInfoButton);
		buttonPanel.add(helpButton);
		buttonPanel.add(leaveButton);
		add(buttonPanel, BorderLayout.SOUTH);
	}
}
