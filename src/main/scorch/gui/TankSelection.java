package scorch.gui;

import scorch.windows.ScorchPanel;

import java.awt.*;

/**
 * TankSelection UI component used to select tank type for each player
 * @author Mikhail Kruk
 */
public class TankSelection extends ScorchPanel {
	TankSelectionControl control;

	public TankSelection(int width, int height) {
		super(width, height);
		control = new TankSelectionControl(width, height);
		add(control);
	}

	public int getSelected() {
		return control.getSelected();
	}

	public void setSelected(int s) {
		control.setSelected(s);
	}

	public void paint(Graphics graphics) {
		width = getSize().width;
		height = getSize().height;
		super.paint(graphics);
	}
}
