package scorch.gui;

import scorch.ScorchGame;
import scorch.windows.MessageBox;
import scorch.windows.ScorchWindow;

import java.awt.*;

/**
 * This class provides the functionality for the System Menu GUI
 * @author Mikhail Kruk
 */
public class SystemMenu extends ScorchWindow {
	private final Button massKill;
	private final Button editProfile;
	private final Button deleteProfile;
	private final Button topTen;

	private final ScorchGame scorchGame;

	public SystemMenu(ScorchGame scorchGame) {
		super(-1, -1, 0, 0, "System Menu", scorchGame.container);

		this.scorchGame = scorchGame;

		Panel panel = new Panel();
		panel.setLayout(new GridLayout(9, 1, 0, 5));
		panel.add(new Button("Statistics"));
		topTen = new Button("Top 10 players");
		topTen.setEnabled(true);
		panel.add(topTen);
		massKill = new Button("Mass kill");
		massKill.setEnabled(scorchGame.isMaster());
		panel.add(massKill);
		editProfile = new Button("Edit profile");
		editProfile.setEnabled(!scorchGame.isGuest());
		panel.add(editProfile);
		deleteProfile = new Button("Delete profile");
		deleteProfile.setEnabled(false);
		panel.add(deleteProfile);
		panel.add(new Button("About Scorch"));
		panel.add(new Button("On-line help"));
		panel.add(new Button("Leave Scorch"));
		panel.add(new Button("Close this menu"));

		setLayout(new FlowLayout(FlowLayout.CENTER));
		add(panel);

		validate();
	}

	public boolean handleEvent(Event event) {
		if (event.id == Event.KEY_PRESS) {
			if (event.key == Event.ESCAPE) {
				close();
				return true;
			} else
				return super.handleEvent(event);
		}

		if (event.id == Event.ACTION_EVENT) {
			if (event.arg.equals("On-line help")) {
				//close();
				(scorchGame).showHelp();
				return true;
			}
			if (event.arg.equals("Edit profile")) {
				//close();
				NewUser newUser = new NewUser(
					(scorchGame).getMyPlayer().getName(),
					scorchGame,
					(scorchGame).getMyPlayer().getProfile()
				);
				newUser.display();
				return true;
			}
			if (event.arg.equals("Statistics")) {
				//close();
				StatsWindow statsWindow =
					new StatsWindow(StatsWindow.IN_GAME, scorchGame);
				statsWindow.display();
				return true;
			}
			if (event.arg.equals("Top 10 players")) {
				//close();
				(scorchGame).requestTopTen();
				return true;
			}
			if (event.arg.equals("About Scorch")) {
				//close();
				AboutBox aboutBox = new AboutBox(scorchGame.container);
				aboutBox.display();
				return true;
			}
			if (event.arg.equals("Leave Scorch")) {
				(scorchGame).Quit();
				return true;
			}
			if (event.arg.equals("Close this menu")) {
				close();
				return true;
			}
			if (event.arg.equals("Mass kill")) {
				close();

				String[] buttons = {"Yes", "Cancel"};
				String[] callbacks = {"massKill", null};
				MessageBox messageBox = new MessageBox(
					"Confirmation",
					"Are you sure you want to kill everybody and start a new round?",
					buttons,
					callbacks,
					owner,
					this
				);
				messageBox.display();
			}
		}
		return super.handleEvent(event);
	}

	public void display() {
		if ((scorchGame).isMassKilled())
			massKill.setEnabled(false);
		super.display();
	}
}
