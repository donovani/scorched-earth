package scorch.gui;

import scorch.PlayerProfile;
import scorch.Protocol;
import scorch.ScorchGame;
import scorch.windows.MessageBox;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The initial login dialog window with the ability to
 * go to the new user creating window or to log in as guest user
 *
 * @author Mikhail Kruk
 */
public class LoginWindow extends ScorchWindow implements ActionListener {
	private final TextField username;
	private final TextField password;
	private final TextField gamePassword;
	private final ScorchGame scorchGame;

	public LoginWindow(String name, ScorchGame scorchGame) {
		super(-1, -1, 200, 200, "Welcome to Scorched Earth 2000!", scorchGame.container);
		this.scorchGame = scorchGame;

		username = new TextField(15);
		password = new TextField(15);
		gamePassword = new TextField(15);

		username.setText(name);
		password.setEchoChar('*');
		password.setEnabled(true);
		gamePassword.setEchoChar('*');
		gamePassword.setEnabled(true);

		Panel credentialsPanel, centerPanel, buttonPanel, usernamePanel, passwordPanel, gamePasswordPanel;

		usernamePanel = new Panel();
		usernamePanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		usernamePanel.add(new Label("Username:"));
		usernamePanel.add(username);

		passwordPanel = new Panel();
		passwordPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		passwordPanel.add(new Label("Password:"));
		passwordPanel.add(password);

		gamePasswordPanel = new Panel();
		gamePasswordPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		gamePasswordPanel.add(new Label("Game Password:"));
		gamePasswordPanel.add(gamePassword);

		credentialsPanel = new Panel();
		credentialsPanel.setLayout(new GridLayout(3, 1));
		credentialsPanel.add(usernamePanel);
		credentialsPanel.add(passwordPanel);
		credentialsPanel.add(gamePasswordPanel);

		centerPanel = new Panel();
		centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		centerPanel.add(credentialsPanel);

		buttonPanel = new Panel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		Button loginButton = new Button("Log in");
		Button newPlayerButton = new Button("New player");
		Button guestButton = new Button("Guest");
		Button leaveButton = new Button("Leave");
		loginButton.addActionListener(this);
		newPlayerButton.addActionListener(this);
		guestButton.addActionListener(this);
		leaveButton.addActionListener(this);
		buttonPanel.add(loginButton);
		buttonPanel.add(newPlayerButton);
		buttonPanel.add(guestButton);
		buttonPanel.add(leaveButton);

		setLayout(new BorderLayout(10, 10));
		add(centerPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		validate();
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();

		if (command.equals("Leave")) {
			scorchGame.Quit();
			return;
		}
		if (command.equals("New player")) {
			close();
			scorchGame.newUser(username.getText());
			return;
		}

		if (username.getText().equals("")) {
			String[] buttons = {"OK"};
			MessageBox messageBox = new MessageBox(
				"Error",
				"You must provide a user name",
				buttons,
				null,
				owner,
				this
			);
			messageBox.display();
			return;
		}

		if (command.equals("Log in")) {
			if (username.getText().equals(Protocol.guest)) {
				String[] buttons = {"OK"};
				MessageBox messageBox = new MessageBox(
					"Error",
					"Username " + Protocol.guest + " is reserved",
					buttons,
					null,
					owner,
					this
				);
				messageBox.display();
				return;
			}

			scorchGame.login(new PlayerProfile(username.getText(), password.getText()), gamePassword.getText());
			close();
			return;
		}

		if (command.equals("Guest")) {
			if (username.getText().equals(Protocol.guest)) {
				String[] buttons = {"OK"};
				MessageBox messageBox = new MessageBox(
					"Error",
					"Username " + Protocol.guest + " is reserved",
					buttons,
					null,
					owner,
					this
				);
				messageBox.display();
				return;
			}

			scorchGame.login(new PlayerProfile(Protocol.guest, username.getText()), gamePassword.getText());
			close();
		}
	}

	public void close() {
		password.setText("");
		password.setEchoChar((char) 0);
		gamePassword.setText("");
		gamePassword.setEchoChar((char) 0);
		super.close();
	}

	public void display() {
		super.display();
		if (username.getText().equals(""))
			username.requestFocus();
		else
			password.requestFocus();
	}
}
