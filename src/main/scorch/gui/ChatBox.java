package scorch.gui;

import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

/**
 * The window that popups when user starts typing some text
 *
 * @author Mikhail Kruk
 */
public class ChatBox extends ScorchWindow implements ActionListener, KeyListener {
	private static final String BROADCAST = "Everybody";

	private final TextField message;
	private final char character;
	private final Choice recipientChoice;
	private final ScorchGame scorchGame;

	public ChatBox(char character, ScorchGame scorchGame) {
		super(-1, -1, 0, 0, "Chat", scorchGame.container);

		this.character = character;

		this.scorchGame = scorchGame;

		recipientChoice = new Choice();
		recipientChoice.addKeyListener(this);
		recipientChoice.add(BROADCAST);
		Vector players = scorchGame.getPlayers();
		for (int i = 0; i < players.size(); i++)
			recipientChoice.addItem(((ScorchPlayer) players.elementAt(i)).getName());

		Panel pb = new Panel();

		pb.setLayout(new FlowLayout(FlowLayout.CENTER));

		Button sendButton = new Button("Say it");
		sendButton.addActionListener(this);
		pb.add(sendButton);

		Button cancelButton = new Button("Cancel");
		cancelButton.addActionListener(this);
		pb.add(cancelButton);

		addKeyListener(this);

		message = new TextField(40);
		message.addKeyListener(this);

		Panel up = new Panel(new FlowLayout(FlowLayout.CENTER));
		up.add(new Label("Say"));
		up.add(message);
		up.add(new Label("to"));
		up.add(recipientChoice);

		setLayout(new BorderLayout());
		add(up, BorderLayout.CENTER);
		add(new Panel(), BorderLayout.NORTH);
		add(pb, BorderLayout.SOUTH);

		validate();
	}

	public void keyPressed(KeyEvent event) {
		if (event.getSource() == recipientChoice)
			message.requestFocus();

		if (event.getKeyCode() == KeyEvent.VK_ESCAPE)
			closeChat(false);
		else if (event.getKeyCode() == KeyEvent.VK_ENTER)
			closeChat(true);
	}

	public void keyReleased(KeyEvent event) {
	}

	public void keyTyped(KeyEvent event) {
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		if ("Say it".equals(command)) {
			closeChat(true);
		} else if ("Cancel".equals(command)) {
			closeChat(false);
		}
	}

	void closeChat(boolean say) {
		close();
		scorchGame.closeChat(say, recipientChoice.getSelectedIndex());
	}

	public void display() {
		super.display();
		message.setText(character + "");
		message.setCaretPosition((message.getText()).length());
		message.requestFocus();
	}

	public String getMessage() {
		return message.getText();
	}
}
