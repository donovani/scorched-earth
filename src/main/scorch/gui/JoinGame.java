package scorch.gui;


import scorch.PlayerProfile;
import scorch.PlayerSettings;
import scorch.ScorchGame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The dialog window presented to a non-master user when they join the game.
 * Allows to select tank type and shows people who are waiting to start.
 *
 * @author Mikhail Kruk
 */
public class JoinGame extends PlayersList implements ActionListener {
	private final TankSelection tankSelection;
	private final Checkbox sounds;

	public JoinGame(PlayerProfile profile, ScorchGame owner) {
		super("Join game", owner);

		Panel buttonPanel, centerPanel, playersPanel, playerListPanel;
		Button okButton = new Button("OK");
		Button cancelButton = new Button("Cancel");

		sounds = new Checkbox("Sound effects");

		sounds.setState(profile.getSounds());

		tankSelection = new TankSelection(200, 30);

		if (profile.getTankType() >= 0)
			tankSelection.setSelected(profile.getTankType());

		buttonPanel = new Panel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(okButton);
		buttonPanel.add(cancelButton);

		Panel soundPanel = new Panel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		soundPanel.add(sounds);

		centerPanel = new Panel();
		centerPanel.setLayout(new BorderLayout());
		centerPanel.add(soundPanel, BorderLayout.NORTH);
		centerPanel.add(new Label("Select your tank:"), BorderLayout.CENTER);
		centerPanel.add(tankSelection, BorderLayout.SOUTH);

		playerListPanel = new Panel();
		playerListPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		playerListPanel.add(players);

		playersPanel = new Panel();
		playersPanel.setLayout(new BorderLayout());
		playersPanel.add(
			new Label("Players in game:", Label.CENTER),
			BorderLayout.NORTH
		);
		playersPanel.add(playerListPanel, BorderLayout.SOUTH);

		setLayout(new BorderLayout(10, 5));
		add(centerPanel, BorderLayout.CENTER);
		add(playersPanel, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.SOUTH);

		okButton.addActionListener(this);
		cancelButton.addActionListener(this);

		validate();

		timeLeft = 60;
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();

		if (command.equals("OK")) {
			close();
			scorchGame.joinGame(new PlayerSettings(tankSelection.getSelected(), sounds.getState()));
			return;
		}
		if (command.equals("Cancel")) {
			scorchGame.Quit();
		}
	}
}
