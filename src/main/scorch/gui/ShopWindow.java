package scorch.gui;

import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.items.Item;
import scorch.weapons.Weapon;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * The ScorchShop window which lets users buy items & weapons for the next rounds
 * @author Mikhail Kruk
 */
public class ShopWindow extends ScorchWindow implements ActionListener, KeyListener {
	private final ScrollPane scrollPane;
	private final Button finishButton;
	private final Button cancelButton;
	private final Button leaveButton;
	private final ScorchPlayer myPlayer;
	private long cash;
	private final Label cashLabel;
	private final ShopItem[] shopItems;
	private final ScorchGame scorchGame;

	public ShopWindow(int width, int height, ScorchGame scorchGame) {
		super(-1, -1, width, height, "Scorched Earth Shop", scorchGame.container);

		this.scorchGame = scorchGame;

		myPlayer = scorchGame.getMyPlayer();
		Weapon[] weapons = myPlayer.getWeapons();
		Item[] items = myPlayer.getItems();
		int count = 0;

		if (weapons != null) count += weapons.length;
		if (items != null) count += items.length;

		cash = myPlayer.getCash();

		Panel itemsPanel = new Panel(new GridLayout(count, 1, 0, 4)),
			mainPanel = new Panel(new BorderLayout(0, 0));

		shopItems = new ShopItem[weapons.length + items.length];

		loadShopItems(itemsPanel, 0, weapons);
		loadShopItems(itemsPanel, weapons.length, items);

		Panel legend = new Panel(new GridLayout(1, 5));

		legend.add(new Label("item", Label.CENTER));
		legend.add(new Label("price", Label.CENTER));
		legend.add(new Label("amount", Label.CENTER));
		legend.add(new Label("order", Label.CENTER));
		legend.add(new Label(""));

		scrollPane = new ScrollPane();

		mainPanel.add(legend, BorderLayout.NORTH);
		mainPanel.add(itemsPanel, BorderLayout.CENTER);

		scrollPane.add(mainPanel);

		finishButton = new Button("Confirm order");
		cancelButton = new Button("Cancel order");
		leaveButton = new Button("Leave the game");

		finishButton.addActionListener(this);
		cancelButton.addActionListener(this);
		leaveButton.addActionListener(this);

		Panel buttonPanel = new Panel();
		buttonPanel.add(finishButton);
		buttonPanel.add(cancelButton);
		buttonPanel.add(leaveButton);

		cashLabel = new Label("", Label.CENTER);
		updateCash(0);

		setLayout(new BorderLayout(0, 0));
		add(cashLabel, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		addKeyListener(this);
	}

	// load shopItems panels corresponding to the items array
	// into to the Panel itemsPanel and also make a copy reference to them
	// from the list ShopItems
	private void loadShopItems(Panel itemsPanel, int offset, Item[] items) {
		for (int i = 0; items != null && i < items.length; i++) {
			ShopItem shopItem = new ShopItem(this, items[i]);
			itemsPanel.add(shopItem);
			shopItems[offset + i] = shopItem;
		}
	}

	// change amount of cash and update the label in the window
	public void updateCash(long amount) {
		cash += amount;
		cashLabel.setText(
			"Please buy weapons and items for the next round. " +
			"You have $" + cash + " left"
		);

		for (ShopItem shopItem : shopItems)
			shopItem.updateButtons();
	}

	public void display() {
		super.display();
		center();
	}

	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		if (source == finishButton) {
			confirm();
		} else if (source == cancelButton) {
			close();
			scorchGame.startGame();
		} else if (source == leaveButton) {
			close();
			scorchGame.Quit();
		}
	}

	private void confirm() {
		close();
		for (ShopItem shopItem : shopItems)
			shopItem.confirmOrder();
		myPlayer.setCash(cash);
		scorchGame.startGame();
	}

	public void keyPressed(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {
			close();
			scorchGame.startGame();
		}

		if (event.getKeyCode() == KeyEvent.VK_ENTER) {
			confirm();
			scorchGame.startGame();
		}
	}

	public void keyReleased(KeyEvent event) {
	}

	public void keyTyped(KeyEvent event) {
	}

	public boolean hasCash(long price) {
		return cash >= price;
	}
}
