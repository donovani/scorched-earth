package scorch.gui;

import scorch.ScorchPlayer;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.util.Vector;

/**
 * The GUI element that provides the toolbar widget,
 * which lists current players of the game, shows their status, etc.
 * @author Mikhail Kruk
 */
public class PlayersListControl extends ScorchWindow implements PlayersLister {
	private final PlayersPanel playersPanel;
	private final ScorchPlayer myself;

	public PlayersListControl(Vector players, ScorchPlayer myself, ScorchWindow owner) {
		super(-1, -1, 0, 0, null, owner);

		this.myself = myself;

		setDoubleBorder(false);

		setLayout(new BorderLayout(0, 0));
		playersPanel = new PlayersPanel(players, this);
		add(playersPanel, BorderLayout.CENTER);
		validate();
		width = height = -1;
	}

	public void paint(Graphics graphics) {
		if (width == -1 && height == -1) {
			Dimension size = getSize();
			width = size.width;
			height = size.height;

			mainPanel.setLocation(2 * windowBorder, 2 * windowBorder);
			mainPanel.setSize
				(width - 4 * windowBorder, height - 4 * windowBorder);
			mainPanel.validate();
		}
		super.paint(graphics);
	}

	public void addPlayer(String player) {
		playersPanel.update();
	}

	public void removePlayer(String player) {
		playersPanel.update();
	}

	public void highlight(ScorchPlayer player) {
		playersPanel.highlight(player);
	}

	public ScorchPlayer getMyself() {
		return myself;
	}
}

