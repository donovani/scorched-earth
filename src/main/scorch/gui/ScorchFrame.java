package scorch.gui;

import scorch.ScorchField;
import scorch.ScorchGame;
import scorch.utility.Debug;
import scorch.windows.ScorchWindow;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Random;

/**
 * The holder for the scorch game field.
 * Provides border and resizes it correctly.
 *
 * @author Mikhail Kruk
 */
public class ScorchFrame extends ScorchWindow implements FocusListener {
	private final ScorchField scorchField;

	public ScorchFrame(int w, int h, Random rand, ScorchGame scorchGame) {
		super(0, 0, w, h, null, scorchGame.container);

		scorchField = new ScorchField(w - 6 * windowBorder, h - 6 * windowBorder, rand, scorchGame);
		Debug.println("Scorch Field created");

		addFocusListener(this);

		setLayout(null);
		scorchField.setLocation(0, 0);
		scorchField.setSize(w - 6 * windowBorder, h - 6 * windowBorder);
		add(scorchField, 0);

		validate();
	}

	public ScorchField getScorchField() {
		return scorchField;
	}

	public void focusGained(FocusEvent event) {
		transferFocus();
	}

	public void focusLost(FocusEvent event) {}

}
