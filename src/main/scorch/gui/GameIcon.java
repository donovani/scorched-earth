package scorch.gui;

import scorch.ScorchGame;

import java.awt.*;
import java.util.Random;

/**
 * @author Mikhail Kruk
 */
class GameIcon extends Canvas {
	private static final String[] iconNames = {"Images/gicon1.gif", "Images/gicon2.gif"};
	private static Image[] icons;

	private final Image icon;

	GameIcon() {
		super();
		if (icons == null)
			loadIcons();

		icon = icons[(int) (Math.random() * Integer.MAX_VALUE) % icons.length];
		setSize(icon.getWidth(this), icon.getHeight(this));
	}

	public void paint(Graphics graphics) {
		graphics.drawImage(icon, 0, 0, this);
	}

	private void loadIcons() {
		icons = new Image[iconNames.length];
		MediaTracker tracker = new MediaTracker(this);

		for (int i = 0; i < iconNames.length; i++) {
			icons[i] = ScorchGame.getImage(iconNames[i]);
			tracker.addImage(icons[i], i);
		}
		try {
			tracker.waitForAll();
		} catch (InterruptedException error) {
			System.err.println(error);
		}
	}
}
