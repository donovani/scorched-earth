package scorch.gui;

import scorch.PlayerProfile;
import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.utility.QSort;
import scorch.windows.ScorchPanel;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.util.Vector;

/**
 * Shows current game stats of all players
 * TODO: make it into 4 different classes with one super class!
 *
 * @author Mikail Kruk
 */
public class StatsWindow extends ScorchWindow {
	public static final int
		END_OF_GAME = 0, // show end of game statistics
		END_OF_ROUND = 1, // end of round
		IN_GAME = 2,  // in game
		TOP_TEN = 3;  // top ten

	private final int type;

	private ScorchGame scorchGame;

	public StatsWindow(int type, ScorchGame scorchGame) {
		this(type, scorchGame.getPlayers(), scorchGame);
	}

	public StatsWindow(int type, Vector players, ScorchGame scorchGame) {
		super(-1, -1, 0, 0, "Players Statistics", scorchGame.container);

		switch (type) {
			case IN_GAME:
			case END_OF_ROUND:
				name = "Players Statistics for Round #" + scorchGame.getRoundCount() + " out of " + scorchGame.getMaxRounds();
				break;
			case TOP_TEN:
				name = "Top Ten Players";
				break;
			case END_OF_GAME:
				name = "Game Over";
				break;
		}

		this.type = type;

		Panel playersPanel = buildPlayersTable(players);

		Panel legendPanel = new Panel(new GridLayout(1, 5));
		legendPanel.add(new Label("Player Name", Label.CENTER));
		if (type != TOP_TEN) {
			legendPanel.add(new Label("Kills", Label.CENTER));
			legendPanel.add(new Label("Gain", Label.CENTER));
		}
		legendPanel.add(new Label("Overall Kills", Label.CENTER));
		legendPanel.add(new Label("Overall Gain", Label.CENTER));

		Panel buttonPanel = new Panel(new FlowLayout(FlowLayout.CENTER));
		if (type != END_OF_ROUND) {
			buttonPanel.add(new Button("OK"));
		} else {
			buttonPanel.add(new Button("Play next round"));
			buttonPanel.add(new Button("Go shopping"));
			buttonPanel.add(new Button("Leave the game"));
		}

		ScorchPanel panel = new ScorchPanel(-1, -1);
		Panel contentPanel = new Panel();
		contentPanel.setBackground(Color.gray);
		contentPanel.setLayout(new BorderLayout(5, 5));
		contentPanel.add(legendPanel, BorderLayout.NORTH);
		contentPanel.add(playersPanel, BorderLayout.CENTER);
		panel.add(contentPanel);

		setLayout(new BorderLayout(0, 0));
		//add(legendPanel, BorderLayout.NORTH);
		//add(playersPanel, BorderLayout.CENTER);
		add(panel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}

	public void close() {
		super.close();
		transferFocus();
	}

	public boolean handleEvent(Event event) {
		if (event.id == Event.ACTION_EVENT) {
			if (event.arg.equals("OK")) {
				close();
				if (type == END_OF_GAME)
					scorchGame.Quit();
				return true;
			}
			if (event.arg.equals("Play next round")) {
				close();
				scorchGame.startGame();
				return true;
			}
			if (event.arg.equals("Go shopping")) {
				close();
				scorchGame.shop();
				return true;
			}
			if (event.arg.equals("Leave the game")) {
				close();
				scorchGame.Quit();
				return true;
			}
		}
		if (event.id == Event.KEY_PRESS && event.key == Event.ESCAPE) {
			close();
			switch (type) {
				case END_OF_GAME:
					scorchGame.Quit();
					break;
				case IN_GAME:
				case TOP_TEN:
					break;
				case END_OF_ROUND:
					scorchGame.startGame();
					break;
				default:
					System.err.println("Internal error in StatsWindow.java, wrong type");
			}

			return true;
		}

		return false;
	}

	private Panel buildPlayersTable(Vector players) {
		String ts;
		PlayerProfile currentProfile;
		ScorchPlayer player;

		Panel playerPanel = new Panel(new GridLayout(1, 5));
		Panel rootPanel = new Panel(new GridLayout(
			Math.max(players.size(), ScorchPlayer.MAX_PLAYERS),
			1,
			0,
			0
		));

		int[] index;
		long[] cash;

		cash = new long[players.size()];

		for (int i = 0; i < players.size(); i++) {
			if (type == TOP_TEN) {
				currentProfile = (PlayerProfile) players.elementAt(i);
				cash[i] = currentProfile.getOverallCashGained();
			} else {
				player = (ScorchPlayer) players.elementAt(i);
				cash[i] = player.getEarnedCash();
			}
		}

		try {
			index = QSort.sort(cash);
		} catch (Exception e) {
			System.err.println("and of course sort failed: " + e);
			return null;
		}

		for (int i = 0; i < players.size(); i++) {
			player = null;
			if (type == TOP_TEN)
				currentProfile = (PlayerProfile) players.elementAt(index[i]);
			else {
				player = (ScorchPlayer) players.elementAt(index[i]);
				currentProfile = player.getProfile();
			}

			ts = currentProfile.getName();
			ts = ts.substring(0, Math.min(20, ts.length()));
			playerPanel.add(new Label(ts, Label.CENTER));
			if (type != TOP_TEN) {
				playerPanel.add(new Label("" + player.getKills(), Label.CENTER));
				playerPanel.add(new Label("" + player.getEarnedCash(), Label.CENTER));
			}
			playerPanel.add(new Label("" + currentProfile.getOverallKills(), Label.CENTER));
			playerPanel.add(new Label("" + currentProfile.getOverallCashGained(), Label.CENTER));
			rootPanel.add(playerPanel);
		}

		return rootPanel;
	}
}
