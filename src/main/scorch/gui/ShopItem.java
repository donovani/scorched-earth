package scorch.gui;

import scorch.items.Item;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Represents the items in the shop window.
 * @author Mikhail Kruk
 */
class ShopItem extends Panel implements ActionListener {
	private final long price;
	private final String name;
	private int order = 0;
	private final int maxOrder;
	private final Item item;
	private final Label priceLabel;
	private final Label quantityLabel;
	private final Label orderLabel;
	private final Button addButton;
	private final Button removeButton;
	private final ShopWindow owner;

	public ShopItem(ShopWindow owner, Item item) {
		super(new GridLayout(1, 5));

		this.item = item;
		this.owner = owner;
		this.name = item.getName();
		this.price = item.getPrice();
		this.maxOrder = item.getMaxOrder();

		add(new Label(name, Label.CENTER));

		Panel buttonPanel = new Panel(new FlowLayout(FlowLayout.CENTER, 4, 0));
		addButton = new Button("Add");
		removeButton = new Button("Remove");
		addButton.addActionListener(this);
		removeButton.addActionListener(this);
		buttonPanel.add(addButton);
		buttonPanel.add(removeButton);

		orderLabel = new Label("0", Label.CENTER);
		priceLabel = new Label(price + "", Label.CENTER);
		quantityLabel = item.getQuantityLabel();
		add(priceLabel);
		add(quantityLabel);
		add(orderLabel);
		add(buttonPanel);
	}

	// make sure that only affordable items are enabled
	// also disable items the max capacity for which is reached
	public void updateButtons() {
		boolean addEnabled = price > 0 && owner.hasCash(price) &&
			(order <= maxOrder - item.getBundle() ||  maxOrder == -1);
		addButton.setEnabled(addEnabled);
		removeButton.setEnabled(price > 0 && order > 0);
	}

	public void confirmOrder() {
		item.incrementQuantity(order);
	}

	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();

		if (source == addButton && addButton.isEnabled()) {
			order += item.getBundle();
			owner.updateCash(-price);
			orderLabel.setText("" + order);
			return;
		}
		if (source == removeButton && removeButton.isEnabled()) {
			order -= item.getBundle();
			owner.updateCash(price);
			orderLabel.setText("" + order);
		}
	}
}

