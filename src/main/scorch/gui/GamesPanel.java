package scorch.gui;

import scorch.windows.ScorchWindow;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
public class GamesPanel extends ScorchWindow {
	Panel gamesList;

	public GamesPanel(int x, int y, int width, int height, Container owner) {
		super(x, y, width, height, "Games", owner);

		ScrollPane scrollPane = new ScrollPane();
		gamesList = new Panel(new FlowLayout(FlowLayout.CENTER, 10, 10));
		scrollPane.add(gamesList);
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.CENTER);
		for (int i = 0; i < 10; i++) {
			Panel controlPanel = new GameControl(i, this);
			gamesList.add(controlPanel);
		}
	}
}
