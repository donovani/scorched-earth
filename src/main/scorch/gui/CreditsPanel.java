package scorch.gui;

import scorch.ScorchGame;
import scorch.windows.ScorchPanel;

import java.awt.*;

/**
 * This panel shows the game credits
 * @author Mikhail Kruk
 */
class CreditsPanel extends ScorchPanel implements Runnable {

	// TODO: Update credits text
	private final String[] credits = {
		"Scorched Earth 2000", ScorchGame.Version, "by KAOS Software Team", "",
		"Project Lead", "Hei C. Ng (xixi)", "",
		"Development Lead", "Alexander Rasin", "",
		"Client Programming", "Mikhail Kruk", "",
		"Weapons Programming", "Nathan Roslavker", "",
		"Physics", "Ramya Ramesh", "",
		"Documentation", "Kapil Mehra", "",
		"Quality Assurance", "John Langton", "",
		"", "Special Thanks", "",
		"Wendell Hicken", "for original Scorched Earth", "",
		"Stanislav Malyshev (Baggins)", "for help with networking", "",
		"Boris Kruk", "for hints on color matching", "",
		"Galsla", "for Galsla mode", "",
		"Roger Waters", "for the name of our team", "",
		"and",
		"dkw for protecting our files", ""
	};

	private final Color[] colors = {
		Color.yellow, Color.white, Color.cyan,
		Color.cyan,
		Color.cyan, Color.lightGray,
		Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.white, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.lightGray, Color.cyan, Color.cyan,
		Color.cyan, Color.white, Color.cyan,
	};

	private int scroller = 0;
	private Thread thread;
	private Image backBuffer;
	private Graphics backBufferGraphics;

	public CreditsPanel() {
		super(0, 0, 0, 0);

		int w = 0;
		for (String credit : credits)
			if (w < fontMetrics.stringWidth(credit))
				w = (int) (1.8 * (double) fontMetrics.stringWidth(credit));

		height = width = w;
		setSize(width, height);

		backgroundColor = Color.lightGray;
		setBackground(Color.gray);
	}

	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}

	public void paint(Graphics graphics) {
		update(graphics);
	}

	public void update(Graphics graphics) {
		if (backBuffer == null) {
			backBuffer = createImage(width, height);
			backBufferGraphics = backBuffer.getGraphics();
		}
		graphics.drawImage(backBuffer, 0, 0, this);
	}

	public void startScroll() {
		thread = new Thread(this, "aboutbox-thread");
		thread.start();
	}

	public void stopScroll() {
		scroller = -1;
	}

	public void run() {
		while (scroller >= 0) {
			if (backBufferGraphics == null)
				continue;

			backBufferGraphics.clearRect(0, 0, width, height);

			for (int i = 0; i < credits.length; i++) {
				if (height + (int) (1.5 * (double) fontHeight) * i - scroller < 0 ||
					height + (int) (1.5 * (double) fontHeight) * i - scroller > height + fontHeight)
					continue;

				backBufferGraphics.setColor(Color.black);
				backBufferGraphics.drawString(
					credits[i],
					2 + (width - 2 * windowBorder - fontMetrics.stringWidth(credits[i])) / 2,
					2 + height + (int) (1.5 * (double) fontHeight) * i - scroller
				);
				backBufferGraphics.setColor(colors[i]);
				backBufferGraphics.drawString(
					credits[i],
					(width - 2 * windowBorder - fontMetrics.stringWidth(credits[i])) / 2,
					height + (int) (1.5 * (double) fontHeight) * i - scroller
				);
			}
			scroller += 2;
			if (scroller >= height + (int) ((double) fontHeight * 1.5) * credits.length)
				scroller = 0;

			repaint();

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
	}
}
