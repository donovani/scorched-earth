package scorch.gui;

import scorch.AIPlayer;
import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * The window used to create a new game.
 * Allows adding AI players and setting the number of rounds.
 *
 * @author Mikhail Kruk
 */
public class StartGame extends ScorchWindow implements ActionListener,
	FocusListener {
	private final List players;
	private final TextField maxPlayersText;
	private int maxPlayers;

	private final ScorchGame scorchGame;

	public StartGame(ScorchGame scorchGame) {
		super(-1, -1, 0, 0, "Create new game", scorchGame.container);

		this.scorchGame = scorchGame;

		players = new List(ScorchPlayer.MAX_PLAYERS);
		maxPlayersText = new TextField(4);
		maxPlayersText.setText("" + ScorchPlayer.MAX_PLAYERS);
		maxPlayersText.addFocusListener(this);
		maxPlayersText.addActionListener(this);
		maxPlayers = ScorchPlayer.MAX_PLAYERS;

		Panel aiPanel, aiButtonPanel, bottomButtonPanel, maxPlayerPanel, topPanel;

		aiPanel = new Panel();
		aiPanel.setLayout(new BorderLayout());
		aiPanel.add(new Label("AI players:"), BorderLayout.NORTH);
		aiPanel.add(players, BorderLayout.CENTER);

		maxPlayerPanel = new Panel();
		maxPlayerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		maxPlayerPanel.add(new Label("Max # of players:"));
		maxPlayerPanel.add(maxPlayersText);

		aiButtonPanel = new Panel();
		aiButtonPanel.setLayout(new GridLayout(2 + AIPlayer.NUMBER_OF_TYPES, 1, 5, 5));
		aiButtonPanel.add(maxPlayerPanel);

		for (int i = 0; i < AIPlayer.NUMBER_OF_TYPES; i++) {
			Button addAiButton = new Button("Add " + AIPlayer.AI_TYPES[i]);
			addAiButton.addActionListener(this);
			addAiButton.setActionCommand(AIPlayer.AI_TYPES[i]);
			aiButtonPanel.add(addAiButton);
		}
		Button removeButton = new Button("Remove");
		removeButton.addActionListener(this);
		aiButtonPanel.add(removeButton);

		bottomButtonPanel = new Panel();
		bottomButtonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		Button createButton = new Button("Create game");
		createButton.addActionListener(this);
		bottomButtonPanel.add(createButton);

		Button cancelButton = new Button("Cancel game");
		cancelButton.addActionListener(this);
		bottomButtonPanel.add(cancelButton);

		topPanel = new Panel();
		topPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		topPanel.add(new Label("Setup general game options:"));

		setLayout(new BorderLayout(5, 5));
		add(topPanel, BorderLayout.NORTH);
		add(aiPanel, BorderLayout.WEST);
		add(aiButtonPanel, BorderLayout.EAST);
		add(bottomButtonPanel, BorderLayout.SOUTH);

		validate();
	}

	public void focusGained(FocusEvent event) {
	}

	public void focusLost(FocusEvent event) {
		Object source = event.getSource();

		if (source == maxPlayersText) {
			try {
				maxPlayers = Integer.parseInt(maxPlayersText.getText());
			}
			catch (NumberFormatException error) {}

			if (maxPlayers < 2)
				maxPlayers = 2;
			if (maxPlayers > ScorchPlayer.MAX_PLAYERS)
				maxPlayers = ScorchPlayer.MAX_PLAYERS;
			maxPlayersText.setText(maxPlayers + "");

			int itemCount = players.getItemCount();

			if (itemCount > maxPlayers) {
				for (int i = 0; i < itemCount - maxPlayers + 1; i++)
					players.remove(itemCount - i - 1);
			}
		}
	}

	public void actionPerformed(ActionEvent event) {
		String cmd = event.getActionCommand();
		Object source = event.getSource();

		if (source == maxPlayersText) {
			transferFocus();
			return;
		}

		if (cmd.equals("Remove")) {
			int i = players.getSelectedIndex();
			if (i >= 0)
				players.remove(i);
			return;
		}

		if (cmd.equals("Cancel game")) {
			scorchGame.Quit();
			return;
		}

		if (cmd.equals("Create game")) {
			scorchGame.createGame(maxPlayers, players.getItems());
			close();
			return;
		}

		// otherwise it's "Add <ai type>"
		if (players.getItemCount() < maxPlayers - 1) {
			players.add(cmd);
		}
	}
}
