package scorch.gui;

import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.items.Fuel;
import scorch.items.Item;
import scorch.windows.MessageBox;
import scorch.windows.ScorchGauge;
import scorch.windows.ScorchWindow;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The window that popups when user uses the fuel item to move tank
 *
 * @author Mikhail Kruk
 */
public class FuelBox extends ScorchWindow implements ActionListener {
	private final ScorchGauge fuelGauge;
	private final Fuel fuel;
	private final ScorchPlayer player;
	private final Dimension dimension;

	private final ScorchGame scorchGame;

	public FuelBox(ScorchGame scorchGame) {
		super(0, 0, 0, 0, "Fuel", scorchGame.container);

		this.scorchGame = scorchGame;

		this.dimension = scorchGame.container.getSize();

		this.player = scorchGame.getMyPlayer();
		this.fuel = (Fuel) player.getItems()[Item.Fuel];

		fuelGauge = new ScorchGauge(fuel.getQuantity(), fuel.getMaxQuantity(), scorchGame.container);

		fuel.setGauge(fuelGauge);

		Button btnL, btnR, btnOK;

		Panel p = new Panel(new FlowLayout());
		btnL = new Button("<");
		btnR = new Button(">");
		btnOK = new Button("OK");
		p.add(btnL);
		p.add(btnOK);
		p.add(btnR);
		btnL.addActionListener(this);
		btnR.addActionListener(this);
		btnOK.addActionListener(this);

		setLayout(new BorderLayout());
		add(new Label("Use fuel to move your tank", Label.CENTER),
			BorderLayout.NORTH);
		add(fuelGauge, BorderLayout.CENTER);
		add(p, BorderLayout.SOUTH);

		validate();
	}

	private void move(int dir) {
		if (fuel.getQuantity() <= 0) {
			//close();
			String[] b = {"OK"};
			String[] c = {null};
			MessageBox msg = new MessageBox
				("Message",
					"You are out of fuel!",
					b, c, owner, this);
			msg.display();
		}

		scorchGame.useItem(Item.Fuel, dir);

		adjustPos();
	}

	public void actionPerformed(ActionEvent evt) {
		String cmd = evt.getActionCommand();
		if ("OK".equals(cmd)) {
			close();
		} else if ("<".equals(cmd)) {
			move(-1);
		} else if (">".equals(cmd)) {
			move(1);
		}
	}

	public boolean handleEvent(Event evt) {
		if (evt.id == Event.KEY_PRESS) {
			if (evt.key == Event.ESCAPE) {
				close();
				return true;
			}
		}

		if (evt.id == Event.KEY_ACTION) {
			if (evt.key == Event.LEFT) {
				move(-1);
				return true;
			}
			if (evt.key == Event.RIGHT) {
				move(1);
				return true;
			}
		}

		return super.handleEvent(evt);
	}

	private void adjustPos() {
		int wx, wy;

		wx = (int) (player.getX() < dimension.width / 2 ?
			dimension.width * 3.0 / 4.0 - width / 2.0 :
			dimension.width / 4.0 - width / 2.0);
		wy = (int) (player.getY() < dimension.height / 2 ?
			dimension.height * 3.0 / 4.0 - height / 2.0 :
			dimension.height / 4.0 - height / 2.0);

		if (wx != x && wy != y) {
			setLocation(wx, wy);
			x = wx;
			y = wy;
		}
	}

	protected void place() {
		super.place();
		adjustPos();
	}

	public void close() {
		super.close();
		fuel.setGauge(null);

		// we want to synchronize on the player, because the animations are
		// run on it, and we don't want to return control to UI before all
		// animations are finished
		synchronized (scorchGame.getMyPlayer()) {
			scorchGame.closeFuelWindow();
		}
	}
}
