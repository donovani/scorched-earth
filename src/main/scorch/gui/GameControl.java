package scorch.gui;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
public class GameControl extends Panel {
	public GameControl(int number, GamesPanel owner) {
		super();
		setBackground(Color.red);
		setLayout(new BorderLayout(0, 0));
		Panel panel = new Panel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		panel.add(new GameIcon());
		add(panel, BorderLayout.CENTER);
		add(
			new Label("Game #" + number, Label.CENTER),
			BorderLayout.SOUTH
		);
	}

	/*
	public void paint(Graphics graphics)
	{
		width = getSize().width;
		height = getSize().height;
		super.paint(graphics);
	}
	*/
}

