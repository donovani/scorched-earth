package scorch.gui;

/**
 * An interface which provides basic functionality of an object containing a list of players
 * @author Mikhail Kruk
 */
public interface PlayersLister {
	void addPlayer(String name);
	void removePlayer(String name);
}
