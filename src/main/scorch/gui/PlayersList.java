package scorch.gui;

import scorch.ScorchGame;
import scorch.ScorchPlayer;
import scorch.windows.ScorchWindow;

import java.awt.*;

/**
 * Extension of the PlayersLister interface that stores players in the List java GUI element
 *
 * @author Mikhail Kruk
 */
public class PlayersList extends ScorchWindow implements PlayersLister, Runnable {
	// max [displayed] length of username
	protected static final int MAX_LENGTH = 20;

	protected int timeLeft = 0;
	protected Thread thread;

	protected List players;

	protected ScorchGame scorchGame;

	public PlayersList(String name, ScorchGame scorchGame) {
		super(-1, -1, 0, 0, name, scorchGame.container);
		players = new List(ScorchPlayer.MAX_PLAYERS);
		this.scorchGame = scorchGame;
	}

	public void addPlayer(String name) {
		players.add(name.substring(0, Math.min(MAX_LENGTH, name.length())));
	}

	public void removePlayer(String name) {
		players.remove(name.substring(0, Math.min(MAX_LENGTH, name.length())));
	}

	public void display() {
		super.display();
		thread = new Thread(this);
		thread.start();
	}

	public void close() {
		super.close();
		thread = null;
	}

	public void run() {
		while (timeLeft > 0 && thread != null) {
			scorchGame.setTimerLabel(timeLeft);
			try {
				Thread.sleep(1000);
			}
			catch (InterruptedException error) {}
			timeLeft--;
		}
		if (timeLeft <= 0)
			scorchGame.showDisconnect("You have timed out");
		else
			scorchGame.setTimerLabel(-1); // hide the label
	}
}

