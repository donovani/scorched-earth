package scorch;

import scorch.items.*;
import scorch.utility.Debug;
import scorch.weapons.Explosion;
import scorch.weapons.ExplosionInfo;
import scorch.weapons.Weapon;

import java.awt.*;
import java.text.DecimalFormat;
import java.util.Random;

/**
 * The class that represents every ScorchPlayer. Contains all
 * the information about every player, its state, weapon, items, tank
 * condition and type; it is able to redraw itself and can explode.
 *
 * @author Mikhail Kruk
 */
public class ScorchPlayer extends PhysicalObject implements Explodable {
	public static final int MAX_PLAYERS = 8;
	public static final int START_ANGLE = 30;
	//public static final int angle_step = 1; // don't change [broken desync]
	public static final int MAX_POWER = 1000;
	public static final int MIN_POWER = (int) (0.2 * MAX_POWER);
	public static final int START_POWER = 300;

	private static final long CASH_BOUNTY = 40000;

	private final int ID;
	private final PlayerProfile profile;
	private int color = -1;
	private final Weapon[] weapons;
	private final Item[] items;
	private int parachutes = 0, tracers = 0;
	private Shield shield = null;

	protected ScorchField scorchField;
	protected int tankType;

	private Random random;

	private long cash, earnedCash = 0;
	private int kills = 0;
	private int angle, power = START_POWER, powerLimit = MAX_POWER;
	private int currentWeapon = Weapon.Missile;

	private int angleChange = 0;

	private boolean alive = true;
	private Explosion explosion, lastExplosion;
	private ExplosionInfo explosionInfo;

	// variable used for tank falling
	private boolean falling = false, exploding = false,
		firstTurn = true, usingFuel = false;
	private static final int fallStep = 3, FALLING_LEFT = 0, FALLING_RIGHT = 1;
	private int
		fallCount,
		//fallHeight,
		ox, oy,  // old position to hide previous frame
		fallDirection = -1, // F_LEFT or F_RIGHT
		leftPos, rightPos; // falling margins for dirt dropping

	protected ScorchGame owner;

	public ScorchPlayer(int id, PlayerProfile profile, ScorchGame owner) {
		super(null, null);
		this.owner = owner;
		this.profile = profile;
		this.ID = id;
		angle = START_ANGLE;

		weapons = Weapon.loadWeapons(this);
		items = Item.loadItems(this);
	}

	/**
	 * Event called by ScorchField when player is put on the field
	 */
	public void onFieldInit(ScorchField scorchField, Bitmap bitmap, int color) {
		this.color = color;
		this.scorchField = scorchField;
		this.bitmap = bitmap;
	}

	/**
	 * Called from ScorchGame if Lamer Mode is enabled
	 */
	public void giveWeapons() {
		int i;
		for (i = 0; i < weapons.length; i++)
			weapons[i].setQuantity(999);
		for (i = 0; i < items.length; i++)
			items[i].setQuantity(999);
	}

	public void setRandom(Random random) {
		this.random = random;
	}

	synchronized public boolean drawNextFrame(boolean update) {
		int south, east;

		if (!alive) return false;

		if (owner.galslaMode) {
			hideFrame(true, false);

			if (Math.random() > 0.5)
				x += (int) (Math.random() * 4.0);
			else
				x -= (int) (Math.random() * 4.0);
			if (Math.random() > 0.51)
				y += (int) (Math.random() * 4.0);
			else
				y -= (int) (Math.random() * 4.0);

			increaseAngle(10);

			if (x < 0) x = 0;
			if (y < 15) y = 15;
			if (x > bitmap.getWidth()) x = bitmap.getWidth();
			if (y > bitmap.getHeight()) y = bitmap.getHeight();

			drawTank(true);

			return false;
		}

		// The tank is falling after explosion
		while (falling)
		{
			fallCount++;
			int width = getWidth(); // tank dimensions
			int height = getHeight();
			// where tank's base actually begins
			// (if it's not whole bottom line of the sprite)
			int leftBase = Tanks.getLeftBase(tankType),
				rightBase = Tanks.getRightBase(tankType);

			south = -1;
			east = -1;
			// Check what is under the tank, s(outh), e(ast)
			for (int i = leftBase; i < width - rightBase; i++)
				if (!bitmap.isBackground(x + i, y + height)) {
					if (south == -1) south = i;
					east = i;
				}

			if (south < 0 && east < 0) // nothing underneath: just fall
			{
				y++;

				if (fallCount % fallStep == 0 || parachuteOpen()) {
					drawFallStep();
					return true;
				}
				continue;
			}

			// Falling is simplified if engine is started (using fuel)
			// This means tanks stop falling as soon as it hits something
			if (usingFuel) {
				setUsingFuel(false);
				if (fallCount > 0)
					fallingDamage();
				else
					setFalling(false);

				return false;
			}

			// slide to the left or to the right if there is no walls
			for (int i = 0; i < height && (south < 0 || east < 0); i++) {
				if (!bitmap.isBackground(x - 1, y + i)) {
					south = 0;
					if (east < 0) east = south;
				}
				if (!bitmap.isBackground(x + width, y + i)) {
					east = width - 1;
					if (south < 0) south = east;
				}
			}

			// decide which way to slide
			if (south > width / 2 && (fallDirection == FALLING_LEFT ||
				fallDirection == -1)) {
				x--;
				fallDirection = FALLING_LEFT;
				if (x < leftPos) leftPos = x;
				if (fallCount % fallStep == 0 || parachuteOpen()) {
					drawFallStep();
					return true;
				}
				continue;
			}
			if (east < width / 2 && (fallDirection == FALLING_RIGHT ||
				fallDirection == -1)) {
				x++;
				fallDirection = FALLING_RIGHT;
				if (x > rightPos) rightPos = x;
				if (fallCount % fallStep == 0 || parachuteOpen()) {
					drawFallStep();
					return true;
				}
				continue;
			}

			// we are stable (end of falling)

			if (fallCount > 0) {
				fallingDamage();

				// The dirt that was on top of this tank has to fall
				new Dropper(bitmap, leftPos - width, rightPos + 2 * width);
				drawTank(false);
				bitmap.newPixels(leftPos - width, 0,
					rightPos - leftPos + 3 * width,
					bitmap.getHeight());
			} else {
				setFalling(false);
			}

			if (explosion != null) {
				explosion.setPosition
					(getX() + getWidth() / 2, getY() + getHeight() / 2);
			}
			return false;
		}

		if (explosion != null) {
			exploding = true;
			boolean t = explosion.drawNextFrame(update);
			if (!t) {
				explosionInfo = explosion.getExplosionInfo();
				setExplosion(null);
				setAlive(false);
				owner.sendTankDead(getID());
			}
			return t;
		}

		drawTank(true);
		return false;
	}

	synchronized private void drawFallStep() {
		if (!alive || bitmap == null) return; // hm..

		if (ox > 0 && oy > 0) {
			int tx, ty;
			tx = x;
			ty = y;
			x = ox;
			y = oy;
			hideFrame(false, false);

			x = tx;
			y = ty;
		}

		drawTank(false);
		int tl = Math.max(getTurretLength(), Parachute.PARACHUTE_ICON.length);
		bitmap.newPixels(Math.min(x, ox) - tl, oy - tl,
			getWidth() + 2 * tl, getHeight() + 2 * tl);
		ox = x;
		oy = y;
	}

	// that's old-style drawShield method which doesn't affect Bitmap.
	public void drawShield(Graphics graphics) {
		//System.err.println("deprecated method ScorchPlayer.drawShield()");
		if (shield == null || !alive)
			return;

		int radius = 2 * Math.max(getWidth(), getHeight());
		int i = Math.max(0, (int) (255 * shield.getStrength() /
			shield.getMaxStrength()));

		graphics.setColor(new Color(i, i, i));
		for (i = 0; i < shield.thickness; i++)
			graphics.drawOval(x + getWidth() / 2 - (radius + i) / 2,
				y + getHeight() / 2 - (radius + i) / 2, radius + i, radius + i);
	}

	public void drawShield(boolean show, boolean update) {
		// if called from ScorchField, or if falling cancel
		if (true)
			return;

		int radius = Math.max(getWidth(), getHeight());
		int i = Math.max(0, (int) (255 * shield.getStrength() / shield.getMaxStrength()));
		int xc = x + getWidth() / 2, yc = y + getHeight() / 2;

		bitmap.setSandMode(true);
		if (show)
			bitmap.setColor(new Color(i, i, i));
		else
			bitmap.setColor(null);

		for (i = 0; i < shield.thickness; i++)
			bitmap.drawEllipse(xc, yc, radius + i, radius + i);

		bitmap.setSandMode(false);

		if (update)
			bitmap.newPixels(
				xc - radius - shield.thickness,
				yc - radius - shield.thickness,
				(radius + shield.thickness) * 2,
				(radius + shield.thickness) * 2
			);
	}

	synchronized public void drawTank(boolean update) {
		if (!alive || bitmap == null)
			return;

		bitmap.drawSprite(x, y, Tanks.getTank(tankType, color), 0);
		drawParachute(true);

		bitmap.setColor(getColor());
		drawTurret(update);

		if (shield != null && !falling)
			drawShield(true, update);
		else if (update) bitmap.newPixels(x, y, getWidth(), getHeight());
	}

	synchronized private void drawParachute(boolean show) {
		int parachuteX, parachuteY;

		if (parachuteOpen()) {
			parachuteX = x + ((getWidth() - Parachute.PARACHUTE_ICON[0].length) / 2);
			parachuteY = getTurretSY() - Parachute.PARACHUTE_ICON.length;
			if (show)
				bitmap.drawSprite(parachuteX, parachuteY, Parachute.PARACHUTE_ICON, 0);
			else
				bitmap.hideSprite(parachuteX, parachuteY, Parachute.PARACHUTE_ICON, 0);
		}
	}

	synchronized private void drawTurret(boolean update) {
		int x1 = x + Tanks.getTurretX(tankType),
			x2 = getTurretX(1), y1 = y + Tanks.getTurretY(tankType),
			y2 = getTurretY(1);

		bitmap.drawLine(x1, y1, x2, y2);
		if (update)
			bitmap.newPixels(
				Math.min(x1, x2),
				Math.min(y1, y2),
				Math.abs(x1 - x2) + 1,
				Math.abs(y1 - y2) + 1
			);
	}

	synchronized public void hideFrame(boolean update, boolean turretOnly) {
		bitmap.setColor(null);
		drawTurret(update);

		if (turretOnly)
			return;

		bitmap.hideSprite(x, y, Tanks.getTank(tankType, color), 0);

		drawParachute(false);
		if (shield != null)
			drawShield(false, update);
		else if (update) bitmap.newPixels(x, y, getWidth(), getHeight());
	}

	synchronized public int getTurretSX() {
		return x + Tanks.getTurretX(tankType);
	}

	synchronized public int getTurretSY() {
		return y + Tanks.getTurretY(tankType);
	}

	synchronized public int getTurretX(double q) {
		int length = getTurretLength();
		double radianAngle = (double) angle * Math.PI / 180.0;
		return x + Tanks.getTurretX(tankType) + (int) ((double) length * q * Math.cos(radianAngle));
	}

	synchronized public int getTurretY(double q) {
		int length = getTurretLength();
		double radianAngle = (double) angle * Math.PI / 180.0;
		return y + Tanks.getTurretY(tankType) - (int) ((double) length * q * Math.sin(radianAngle));
	}

	synchronized public int getTurretLength() {
		return Tanks.getTurretLength(tankType);
	}

	private boolean parachuteOpen() {
		return (fallCount > 5 && parachutes > 0 && !isDying());
	}

	public String getName() {
		return profile.getName();
	}

	public long getCash() {
		return cash;
	}

	public long getBounty() {
		return CASH_BOUNTY;
	}

	public long getEarnedCash() {
		return earnedCash;
	}

	public void setEarnedCash(long earnedCash) {
		this.earnedCash = earnedCash;
	}

	public void setCash(long cash) {
		this.cash = cash;
	}

	public void addCash(long amount) {
		cash += amount;
		earnedCash += amount;
	}

	public int getKills() {
		return kills;
	}

	private void addKills(int amount) {
		kills += amount;
	}

	public int getID() {
		return ID;
	}

	public void setTankType(int tankType) {
		this.tankType = tankType;
	}

	public int getTankType() {
		return tankType;
	}

	/*
	public void setColor(int color)
  {
		this.color = color;
	}
	*/

	public int getAngle() {
		return angle;
	}

	public void setPower(int newPower) {
		power = newPower;

		if (power < 0) power = powerLimit;
		if (power > powerLimit) power = 0;
	}

	public int increasePower(int amount) {
		setPower(power + amount);

		return power;
	}

	/**
	 * Increment turret angle by specified degrees
 	 */
	public int increaseAngle(int amount) {
		angleChange += amount;
		angle += amount;

	/*if(Math.abs(angle_change) > angle_step ||
	   angle < 0 || angle >= 180 || amount == 0)
	   {*/
		angle -= angleChange;
		bitmap.setColor(null);
		drawTurret(true);
		//hideFrame(false, false);
		angle += angleChange;
		//angle_change = angle_step*2;
		/*}*/

		if (angle < 0) angle += 180;
		if (angle >= 180) angle %= 180;

		if (/*Math.abs(angle_change) > angle_step &&*/ !owner.galslaMode) {
			// we need to draw tank, not just turret
			// because while hiding turret we might hide part of tank...
			drawTank(true);
			bitmap.newPixels();
			angleChange = 0;
			owner.sendUpdate(this);
		}

		//int tl = getTurretL();
		//bitmap.newPixels(x-tl, y-tl, getWidth()+2*tl, getHeight()+2*tl, true);

		return angle;
	}

	public void setAngle(int newAngle) {
		hideFrame(false, false);
		angle = newAngle;
		drawTank(false);

		int turretLength = getTurretLength();
		bitmap.newPixels(
			x - turretLength,
			y - turretLength,
			getWidth() + 2 * turretLength,
			getHeight() + 2 * turretLength
		);
		angleChange = 0;
	}

	// calculate damage from the falling
	// for now as simple as 1 point for any fall step...
	// must be changed, probably
	// if the parachute is open, no damage is inflicted
	private void fallingDamage() {
		drawParachute(false); // hide parachute if needed
		// dispose of the used parachute
		if (parachuteOpen()) {
			parachutes--;
			items[Item.Parachute].decrementQuantity();
		} else {
			powerLimit -= fallCount;
			checkPower();
		}

		// reset fall count after drawParachute but before
		// setFalling() since setFalling() redraws tank (?)
		fallCount = -1;
		setFalling(false);
	}

	// check that power is below powerLimit and update label if necessary
	private void checkPower() {
		if (power > powerLimit) {
			power = powerLimit;
			owner.updatePowerLabel(this);
		}
	}

	// Decrease power limit by q * maxPower
	// Return true if player is still alive after that
	// TODO: Determine intent of d & q
	public boolean decreasePowerLimit(int amount) {
		double d;

		if (shield != null) {
			// If there is a shield make the damage two times smaller,
			// then apply part of it to the shield & part of it to the player.
			double q = (shield.damage * amount) / (MAX_POWER);

			if ((d = shield.decreaseStrength(q)) <= 0) {
				amount *= (1 - shield.damage - d);
				deactivateShield(); // no more shield
			} else
				amount *= (1 - shield.damage);
		}

		powerLimit -= amount;

		Debug.println("New power limit for " + getName() + " is " + powerLimit);

		if (powerLimit < MIN_POWER)
			return false;
		else {
			checkPower();
			return true;
		}
	}

	public int getPower() {
		return power;
	}

	public int getPowerLimit() {
		return powerLimit;
	}

	public void increasePowerLimit(int increase) {
		powerLimit += increase;
		if (powerLimit < 0) powerLimit = 0;
		if (powerLimit > MAX_POWER) powerLimit = MAX_POWER;
	}

	public int getWeapon() {
		return currentWeapon;
	}

	public Weapon[] getWeapons() {
		return weapons;
	}

	public Item[] getItems() {
		return items;
	}

	public int getWidth() {
		return Tanks.getTankWidth(tankType);
	}

	public int getHeight() {
		return Tanks.getTankHeight(tankType);
	}

	public int getColor() {
		return Tanks.getTankColor(color);
	}

	public PlayerProfile getProfile() {
		return profile;
	}

	public synchronized void setFalling(boolean fall) {
		if (falling == fall || exploding)
			return; // if already falling or exploding -- ignore

		this.falling = fall;

		if (fall) {
			ox = x;
			oy = y;
			fallCount = -1;
			//fallHeight = 0;
			fallDirection = -1;
			leftPos = x;
			rightPos = x;
			//hideFrame(true, false);
		} else
			drawFallStep();
	}

	public synchronized boolean isUsingFuel() {
		return usingFuel;
	}

	public synchronized void setUsingFuel(boolean usingFuel) {
		this.usingFuel = usingFuel;
	}

	public void resetAll() {
		firstTurn = true;
		setAlive(true);
		//setKills(0);
		//setEarnedCash(0);
		//deactivateShield();
		setExplosion(null);
		lastExplosion = null;

		Parachute parachute = (Parachute) items[Item.Parachute];
		parachute.active = false;
		parachutes = 0;

		Tracer tracer = (Tracer) items[Item.Tracer];
		tracer.active = false;
		tracers = 0;

		powerLimit = MAX_POWER;
		power = START_POWER;
	}

	private void setAlive(boolean alive) {
		this.alive = alive;
		if (alive) {
			setFalling(false);
			exploding = false;
		} else
			hideFrame(true, false);
	}

	public boolean isAlive() {
		return alive;
	}

	// if explosion is already set this tank is about to die.
	public boolean isDying() {
		return explosion != null;
	}

	public void setExplosion(Explosion newExplosion) {
		if (newExplosion != null)
			newExplosion.setPosition(getX() + getWidth() / 2, getY() + getHeight() / 2);
		else
			lastExplosion = explosion;

		explosion = newExplosion;
	}

	/**
	 * Calculate the damage caused by this instance of ScorchPlayer
	 */
	public int calculateDamage(ScorchPlayer player) {
		if (lastExplosion != null)
			return lastExplosion.calculateDamage(player);
		else
			return 0;
	}

	public ExplosionInfo getExplosionInfo() {
		if (explosion == null) {
			// If no explosion, reset explosionInfo to null and return it
			ExplosionInfo info = explosionInfo;
			explosionInfo = null;
			return info;
		}
		return explosion.getExplosionInfo();
	}

	public void useItem(int type, int arg) {
		switch (type) {
			case Item.Shield:
			case Item.MediumShield:
			case Item.HeavyShield:
				activateShield(type);
				break;
			case Item.Parachute:
				activateParachute(arg);
				break;
			case Item.Tracer:
				activateTracer(arg);
				break;
			case Item.Battery:
				activateBattery(arg);
				break;
			case Item.Fuel:
				activateFuel(arg);
				break;
			default:
				System.err.println("ScorchPlayer.useItem(): Illegal item " +
					type);
		}
	}

	private void activateShield(int type) {
		if (shield != null)
			return;

		shield = (Shield) items[type];

		shield.decrementQuantity();

		shield.reset();
		drawTank(true);
		bitmap.newPixels();
	}

	// this should be private, but due to some debug issues...
	public void activateParachute(int quantity) {
		Parachute parachute = ((Parachute) items[Item.Parachute]);

		parachutes = quantity;

		// set local copy of parachute to the current state
		if (quantity > 0 && !parachute.active) {
			parachute.active = true;
			parachute.setQuantity(quantity);
		}
	}

	private void activateTracer(int quantity) {
		Tracer tracer = ((Tracer) items[Item.Tracer]);

		tracers = quantity;

		if (quantity > 0 && !tracer.active) {
			tracer.active = true;
			tracer.setQuantity(quantity);
		}
	}

	// used by ScorchGame to show AutoDefense window when appropriate
	public boolean isFirstTurn() {
		if (!firstTurn) return false;

		firstTurn = false;
		return true;
	}

	// Checks if there is an Auto-Defense available.
	// If so, decreases the number the player has left and return the result of the check.
	public boolean useAutoDefense() {
		AutoDefense autoDefense = (AutoDefense) items[Item.AutoDefense];
		boolean need = false; // AutoDefense needed? (i.e. are there any items)
		Item item;

		if (autoDefense.getQuantity() > 0) {
			for (int i = 0; i < items.length && !need; i++) {
				item = items[i];
				need = (item.getQuantity() > 0) &&
					(item instanceof Parachute || item instanceof Shield);
			}

			if (need) {
				autoDefense.decrementQuantity();
				return true;
			}
		}

		return false;
	}

	private void activateBattery(int power) {
		Battery battery = (Battery) items[Item.Battery];

		increasePowerLimit(power);

		battery.decrementQuantity();
	}

	// direction = -1 for left, 1 for right
	private void activateFuel(final int direction) {
		final Fuel fuel = (Fuel) items[Item.Fuel];

		class fuelMove implements Runnable {
			public fuelMove() {
				Thread thread = new Thread(this, "fuel-thread");
				thread.start();
			}

			public void run() {
				int tankX, tankY;

				synchronized (ScorchPlayer.this) {
					tankY = getY() + getHeight() - 1;
					tankX = getX();
					if (direction == 1)
						tankX += getWidth();
					else
						tankX -= 1;

					// For now control quantity only for the local player
					if (!bitmap.isBackground(tankX + direction, tankY - 1) ||
						(owner.getMyPlayer() == ScorchPlayer.this &&
							fuel.getQuantity() <= 0))
						return;

					fuel.decrementQuantity();

					if (direction == 1)
						tankX -= Tanks.getRightBase(tankType);
					else
						tankX += Tanks.getLeftBase(tankType);

					hideFrame(true, false);
					if (!bitmap.isBackground(tankX, tankY))
						y--;

					x += direction;

					setFalling(true);
					setUsingFuel(true);
					scorchField.runAnimationNow(ScorchPlayer.this);
				}
			}
		}
		new fuelMove();
	}

	private void deactivateShield() {
		drawShield(false, true); // hide it
		shield = null;
		//drawTank(true);
	}

	public boolean checkTracer() {
		if (tracers > 0) {
			tracers--;
			items[Item.Tracer].decrementQuantity();
			return true;
		} else
			return false;
	}

	public void setWeapon(int weapon_n) {
		currentWeapon = weapons[weapon_n].getType();
	}

	public void decrementWeapon() {
		weapons[currentWeapon].decrementQuantity();
	}

	public int getWeaponAmmo() {
		return weapons[currentWeapon].getQuantity();
	}

	public Explosion getWeaponExplosion(int weapon) {
		return weapons[weapon].produceExplosion(bitmap, random);
	}

	public String toString() {
		String res = "name: " + getName() + " angle: " + getAngle() + " power: " +
			getPower() + " powerLimit: " + powerLimit;

		return res;
	}

	// return string to be displayed in tool tip
	public String getToolTip() {
		String res;
		double percent = ((double) powerLimit / MAX_POWER * 100);
		DecimalFormat format = new DecimalFormat();
		format.setMaximumFractionDigits(2);

		res = getName() + " " + format.format(percent) + "%";
		if (shield != null) {
			double sv = shield.getStrength() / shield.getMaxStrength() * 100;
			res += " Shield " + format.format(sv) + "%";
		}

		return res;
	}

	public void updateKills(long earnedCash, int kills) {
		profile.increaseOverallCashGained(earnedCash);
		profile.increaseOverallKills(kills);
		addCash(earnedCash);
		addKills(kills);
	}
}
