package scorch;

import scorch.backgrounds.Background;

import java.awt.*;

/**
 * Class responsible for drawing chat messages over the ScorchField
 * @author Mikhail Kruk
 */
class ScorchChat implements Runnable {
	private static final String SYSTEM_MESSAGE_PREFIX = "*System* ";
	private static final int MARGIN = 5;
	private static final int DELAY = 7500; // ms
	private static final int MAX_LINES = 8;

	private final String[] chat = new String[MAX_LINES];
	private Color chatColor = null;
	private FontMetrics fontMetrics = null;
	private int fontHeight = -1, currentChatLine = 0;

	private Thread thread;
	private final ScorchField owner;

	public ScorchChat(ScorchField owner, Background bk) {
		this.owner = owner;

		int c1 = Math.min(bk.getPixelColor(0, 0), bk.getPixelColor(0, 1));

		// take complementary color to be the color of chat lines
		chatColor = new Color(
			255 - Bitmap.getRed(c1),
			255 - Bitmap.getGreen(c1),
			255 - Bitmap.getBlue(c1)
		);
	}

	public void paint(Graphics g, int width) {
		if (fontMetrics == null) {
			fontMetrics = g.getFontMetrics();
			fontHeight = fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();
		}

		g.setColor(chatColor);

		String windString;
		int wind = Physics.getWind() * 8;
		if (wind > 0)
			windString = wind + " ->";
		else
			windString = "<- " + (-wind);
		if (wind != 0)
			g.drawString(windString, width - 2 * fontMetrics.stringWidth(windString), (int) (fontHeight * 1.2));

		for (int i = currentChatLine - 1; i >= 0; i--) {
			if (chat[i].startsWith(SYSTEM_MESSAGE_PREFIX)) {
				g.drawString(
					chat[i],
					(width - fontMetrics.stringWidth(chat[i])) / 2,
					fontHeight * (i + 1)
				);
			} else {
				g.drawString(chat[i], MARGIN, fontHeight * (i + 1));
			}
		}
	}

	public synchronized void addMessage(String message) {
		boolean done = false;
		String nextMessage;
		int stringWidth, screenWidth, newLength, newWidth;
		int[] fontWidth;

		// get the maximum allowed string length. note that I've used
		// 4 margins instead of 2 for extra safety since we do not
		// calculate the exact widths and rely on characters being of the
		// (approx) same size
		screenWidth = owner.getWidth() - 4 * MARGIN;

		nextMessage = message.replace(Protocol.separator, ' '); // what is this for?;

		do {
			message = nextMessage;
			if (fontMetrics != null &&
				(stringWidth = fontMetrics.stringWidth(message)) > screenWidth) {
				fontWidth = fontMetrics.getWidths();
				newWidth = 0;
				newLength = 0;
				while (newWidth < screenWidth) {
					newWidth +=
						fontWidth[message.charAt(newLength)];
					newLength++;
				}

				if (newLength < message.length()) {
					nextMessage = message.substring(newLength);
					message = message.substring(0, newLength);
				} else {
					// IMHO we can't get here, but we do somehow.
					done = true;
				}
			} else
				done = true;

			if (currentChatLine == MAX_LINES)
				dropLastLine();

			chat[currentChatLine++] = message;
			owner.repaint();
		}
		while (!done);

		if (thread == null) {
			thread = new Thread(this, "chat-thread");
			thread.start();
		}
	}

	public synchronized void addSystemMessage(String message) {
		addMessage(SYSTEM_MESSAGE_PREFIX + message);
	}

	private synchronized void dropLastLine() {
		for (int i = 1; i < MAX_LINES; i++)
			chat[i - 1] = chat[i];
		currentChatLine--;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(DELAY);
			} catch (InterruptedException error) {
				System.err.println(error);
			}

			dropLastLine();
			owner.repaint();

			if (currentChatLine == 0) {
				thread = null;
				return;
			}
		}
	}
}
