package scorch.client;

import scorch.ScorchGame;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * The window for the desktop version of the game, which spins up a ScorchGame instance.
 * Also contains the main function to be called by the executable.
 *
 * @author Ian Donovan
 */
public class ScorchApplication extends Frame implements WindowListener {

	private final ScorchGame game;

	public static void main(String... args) {
		ScorchApplication app = new ScorchApplication();
		app.start();
	}

	public ScorchApplication() {
		// TODO: Improve sizing options
		final int width = 800;
		final int height = 600;

		setTitle("Scorched Earth 2000 Restoration");
		setSize(width * 2, height * 2);
		addWindowListener(this);

		// TODO: Add URLs
		game = new ScorchGame(this, width, height, "", 4242, null, null);
	}

	public void start() {
		game.init();
		game.start();
		setVisible(true);
		repaint();
	}

	public void windowOpened(WindowEvent e) {
		// Implementation not needed
	}

	public void windowClosing(WindowEvent e) {
		//game.destroy();
		System.exit(0);
	}

	public void windowClosed(WindowEvent e) {
		// Implementation not needed
	}

	public void windowIconified(WindowEvent e) {
		// Implementation not needed
	}

	public void windowDeiconified(WindowEvent e) {
		// Implementation not needed
	}

	public void windowActivated(WindowEvent e) {
		// Implementation not needed
	}

	public void windowDeactivated(WindowEvent e) {
		// Implementation not needed
	}
}
