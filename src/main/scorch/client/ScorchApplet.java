package scorch.client;

import scorch.ScorchGame;

import java.applet.Applet;
import java.awt.*;
import java.net.URL;

/**
 * The main applet class that runs the ScorchGame.
 * Note that web server must run on the same machine where ScorchServer runs.
 * Java Applets can connect only to the host which served the HTML files.
 *
 * @author Mikhail Kruk
 * @author Ian Donovan
 */
public final class ScorchApplet extends Applet implements ScorchClient {
	ScorchGame game;

	private void initializeScorchGame() {
		if (game == null) {
			// TODO: Improve sizing options
			final int width = 800;
			final int height = 600;

			setSize(width, height);
			setBackground(Color.black);

			// TODO: Add URLs
			game = new ScorchGame(this, width, height, "", 4242, null, null);
		}
	}

	public void init() {
		initializeScorchGame();
		game.init();
	}

	public void start() {
		initializeScorchGame();
		game.start();
	}

	public void stop() {
		game.stop();
	}

	public void destroy() {
		super.destroy();
		// TODO: Add any cleanup operations needed
	}

	@Override
	public void banner(URL burl) {

	}

	@Override
	public void showHelp() {

	}

	@Override
	public void quit() {

	}

	public String getAppletInfo() {
		return "Scorched Earth 2000 " + ScorchGame.Version + "\nby the KAOS Software team";
	}

	public String[][] getParameterInfo() {
		String[][] info =
			{
				// {paramWidth, "int", "game field width"},
				// {paramHeight, "int", "game field height"},
				// {paramPort, "int", "port on which to connect to the server"},
				// {paramLeaveURL, "String", "url of the Scorch start page"},
				// {paramHelpURL, "String", "url of the Scorch on-line help"}
			};

		return info;
	}
}
