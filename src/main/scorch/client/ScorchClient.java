package scorch.client;

import java.net.URL;

/**
 * An interface layer over game clients, covering common platform-level behaviors.
 * This allows the game to define custom behaviors for each type of client; for example,
 * applets may rely on loading web resources, while applications may need to read a file.
 * <br>
 * This interface should primarily be scoped to divergent behavior in clients.
 * If something can be done in a platform-agnostic way, consider breaking it into a service,
 * or making it part of the ScorchGame class - which is shared by clients.
 * @author Ian Donovan
 */
public interface ScorchClient {
	// getAppletContext().showDocument(leaveURL);
	public void quit();

	// getAppletContext().showDocument(burl,"_blank");
	// paid = true;
	public void banner(URL burl);

	// getAppletContext().showDocument(helpURL,"Scorched Earth 2000 Help");
	public void showHelp();
}
