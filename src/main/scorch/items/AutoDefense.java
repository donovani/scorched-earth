package scorch.items;

import scorch.ScorchGame;

/**
 * Auto Defense gives player a chance to activate shield etc. before the round begins
 *
 * @author Mikhail Kruk
 */
public class AutoDefense extends Item {
	public AutoDefense() {
		type = AutoDefense;
		price = 5000;
	}

	public ItemControl getControlPanel(ScorchGame scorchGame) {
		controlPanel = new AutoDefenseControl(this, scorchGame);
		return controlPanel;
	}
}


