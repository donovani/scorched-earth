package scorch.items;

import scorch.ScorchGame;
import scorch.windows.ScorchGauge;

/**
 * Fuel is an item which lets players move their tank to get a better shot
 *
 * @author Mikhail Kruk
 */
public class Fuel extends Item {
	private static int tankVolume; // how much fuel per a tank bought

	private ScorchGauge gauge = null;

	public Fuel() {
		type = Fuel;
		price = 10000;
		quantity = 0;
		maxQuantity = 1000;
		bundle = 100;
	}

	public void setGauge(ScorchGauge gauge) {
		this.gauge = gauge;
	}

	public void setQuantity(int newQuantity) {
		super.setQuantity(newQuantity);
		if (gauge != null)
			gauge.updateValue(quantity);
	}

	public ItemControl getControlPanel(ScorchGame scorchGame) {
		controlPanel = new FuelControl(this, scorchGame);
		return controlPanel;
	}
}


