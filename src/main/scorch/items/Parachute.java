package scorch.items;

import java.awt.*;

/**
 * Implements item Parachute, which can saves tanks from falling damage
 * @author Mikhail Kruk
 */
public class Parachute extends Tracer {
	private final static int w = Color.white.getRGB();
	public final static int[][] PARACHUTE_ICON = {
		{0, 0, 0, 0, 0, w, w, w, w, 0, 0, 0, 0, 0},
		{0, 0, 0, w, w, w, w, w, w, w, w, 0, 0, 0},
		{0, 0, w, w, w, w, w, w, w, w, w, w, 0, 0},
		{0, w, w, w, w, w, w, w, w, w, w, w, w, 0},
		{w, w, w, w, w, w, w, w, w, w, w, w, w, w},
		{0, w, 0, 0, w, 0, 0, 0, 0, w, 0, 0, w, 0},
		{0, 0, w, 0, 0, w, 0, 0, w, 0, 0, w, 0, 0},
		{0, 0, 0, w, 0, w, 0, 0, w, 0, w, 0, 0, 0},
		{0, 0, 0, 0, w, 0, w, w, 0, w, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, w, w, w, w, 0, 0, 0, 0, 0},
	};

	public Parachute() {
		type = Parachute;
		price = 2000;
		autoDefenseAv = true;
	}
}
