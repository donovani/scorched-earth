package scorch.items;

import scorch.ScorchGame;
import scorch.utility.Debug;

/**
 * @author Mikhail Kruk
 */
public class Shield extends Item {
	public double damage; // fraction of damage absorbed
	public int thickness;
	protected double maxStrength; // initial (max) strength of the shield
	protected double strength; // current strength of the shield

	public Shield() {
		type = Shield;
		price = 20000;
		maxStrength = 1;
		damage = 0.9;
		thickness = 1;
		autoDefenseAv = true;
	}

	public ItemControl getControlPanel(ScorchGame owner) {
		controlPanel = new ShieldControl(this, owner);
		return controlPanel;
	}

	public double getStrength() {
		return strength;
	}

	public double getMaxStrength() {
		return maxStrength;
	}

	public void reset() {
		strength = maxStrength;
	}

	public double decreaseStrength(double amount) {
		Debug.println("Shield: " + type + " " + strength + " -> " + (strength - amount));
		strength -= amount;

		if (strength < .2 && strength > 0)
			strength = 0;
		return strength;
	}
}
