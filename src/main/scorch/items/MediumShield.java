package scorch.items;

/**
 * @author Mikhail Kruk
 */
public class MediumShield extends Shield {
	public MediumShield() {
		type = MediumShield;
		maxStrength = 2;
		price = 27000;
		damage = 0.95;
		thickness = 3;
	}

}
