package scorch.items;

import scorch.ScorchGame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Mikhail Kruk
 */
class ShieldControl extends ItemControl implements ActionListener {
	public ShieldControl(Shield shield, ScorchGame owner) {
		super(shield, owner);

		control = new Button("Activate");
		((Button) control).addActionListener(this);

		setLayout(new FlowLayout());
		add(control);
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals("Activate") && item.getQuantity() > 0)
			owner.useItem(item.getType(), -1);
	}
}
