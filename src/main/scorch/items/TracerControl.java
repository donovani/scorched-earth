package scorch.items;

import scorch.ScorchGame;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author Mikhail Kruk
 */
class TracerControl extends ItemControl implements ItemListener {
	public TracerControl(Tracer tracer, ScorchGame owner) {
		super(tracer, owner);

		control = new Checkbox("Use", ((Tracer) item).active);
		((Checkbox) control).addItemListener(this);

		setLayout(new FlowLayout(FlowLayout.CENTER));
		add(control);
	}

	public void itemStateChanged(ItemEvent event) {
		int t = item.getQuantity();
		if (t > 0 && event.getStateChange() == ItemEvent.SELECTED) {
			owner.useItem(item.getType(), t);
			((Tracer) item).active = true;
		} else {
			owner.useItem(item.getType(), 0);
			((Tracer) item).active = false;
		}
	}
}
