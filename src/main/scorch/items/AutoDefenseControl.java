package scorch.items;

import scorch.ScorchGame;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
class AutoDefenseControl extends ItemControl {
	public AutoDefenseControl(Item autoDefenseItem, ScorchGame scorchGame) {
		super(autoDefenseItem, scorchGame);
		setLayout(new FlowLayout());
		control = new Label("active");
		add(control);
	}
}
