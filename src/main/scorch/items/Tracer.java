package scorch.items;

import scorch.ScorchGame;

/**
 * @author Mikhail Kruk
 */
public class Tracer extends Item {
	public boolean active = false;

	public Tracer() {
		type = Tracer;
		price = 100;
	}

	public void decrementQuantity() {
		super.decrementQuantity();
		if (quantity == 0)
			active = false;
	}

	public ItemControl getControlPanel(ScorchGame owner) {
		controlPanel = new TracerControl(this, owner);
		return controlPanel;
	}
}
