package scorch.items;

import scorch.ScorchGame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Batteries are items which restore player's power
 * @author Mikhail Kruk
 */
public class Battery extends Item {
	public static int power = 100;

	public Battery() {
		type = Battery;
		price = 4500;
	}

	public ItemControl getControlPanel(ScorchGame scorchGame) {
		controlPanel = new BatteryControl(this, scorchGame);
		return controlPanel;
	}
}

