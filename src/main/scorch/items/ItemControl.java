package scorch.items;

import scorch.ScorchGame;

import java.awt.*;

/**
 * @author Mikhail Kruk
 */
public abstract class ItemControl extends Container {
	protected ScorchGame owner;
	protected Item item;
	protected Component control;

	public ItemControl(Item item, ScorchGame owner) {
		super();

		this.item = item;
		this.owner = owner;
	}

	public void setEnabled(boolean v) {
		if (control != null)
			control.setEnabled(v);
		else
			System.err.println("ItemControl.setEnabled(): Internal error");
	}
}
