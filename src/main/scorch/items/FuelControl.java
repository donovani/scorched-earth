package scorch.items;

import scorch.ScorchGame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Mikhail Kruk
 */
class FuelControl extends ItemControl implements ActionListener {
	public FuelControl(Fuel fuel, ScorchGame scorchGame) {
		super(fuel, scorchGame);

		control = new Button("Activate");
		((Button) control).addActionListener(this);

		setLayout(new FlowLayout());
		add(control);
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals("Activate") && item.getQuantity() > 0)
			owner.showFuelWindow((Fuel) item);
	}
}
