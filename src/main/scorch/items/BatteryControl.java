package scorch.items;

import scorch.ScorchGame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Mikhail Kruk
 */
class BatteryControl extends ItemControl implements ActionListener {
	public BatteryControl(Battery battery, ScorchGame scorchGame) {
		super(battery, scorchGame);

		control = new Button("Install");
		((Button) control).addActionListener(this);

		setLayout(new FlowLayout());
		add(control);
	}

	public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand().equals("Install")) {
			if (item.getQuantity() > 0)
				owner.useItem(Item.Battery, Battery.power);
		}

	}
}
